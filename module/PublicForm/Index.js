const ApiBusiness = require("@library/Business/Api");
const FormBusiness = require("@library/Business/Form");


// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/PublicForm/Index.js";

const get = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        let result;
        result = await FormBusiness.getPublishedFormDetail(params);
        if (!result) {
            return ApiBusiness.response(res, {
                code: 400,
                message: "Cannot get published form",
            });
        }
        return ApiBusiness.response(res, result);


    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }

};

module.exports = {
    get
};
