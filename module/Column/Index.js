// Business
const ApiBusiness = require('@library/Business/Api');
const ColumnBusiness = require('@library/Business/Column');
const MetricBusiness = require('@library/Business/Metric');

// Model Constants
const {FORMAT_TYPE} = require("@library/Model/Metric")

// Utils
const {handleError, formatRequestParams} = require('@library/Utils');

const PATH = '@module/Column/Index.js';

const getList = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params.type) {
            switch (params.type) {
                case 'get-latest-modify-column': {
                    const result = await ColumnBusiness.getLatestModifyColumn(params);

                    return ApiBusiness.response(res, result);
                }
                case 'get-list-modify-column': {
                    const result = await ColumnBusiness.getListingModifyColumn(params);

                    return ApiBusiness.response(res, result);
                }
                case 'get-columns': { // get list columns metrics
                    const result = await MetricBusiness.getDetailMetric({
                        ...params,
                        type: FORMAT_TYPE.GRID,
                    });

                    return ApiBusiness.response(res, result);
                }
                case "get-list-chart-metrics": {
                    const result = await MetricBusiness.getDetailMetric({
                        ...params,
                        type: FORMAT_TYPE.CHART,
                    });

                    return ApiBusiness.response(res, result);
                }
                case 'validate-name': {
                    let result = await ColumnBusiness.isModifyColumnNameValid(params);

                    return ApiBusiness.response(res, result);
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const get = async (req, res) => {
    //
};

const create = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params.type) {
            switch (params.type) {
                case 'add-modify-column': {
                    const result = await ColumnBusiness.addModifyColumn(params);

                    return ApiBusiness.response(res, result);
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const update = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params.type) {
            switch (params.type) {
                case 'update-modify-column': {
                    const result = await ColumnBusiness.updateModifyColumn(params);

                    return ApiBusiness.response(res, result);
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const _delete = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        const result = await ColumnBusiness.deleteModifyColumn(params);

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

module.exports = {
    getList,
    create,
    get,
    update,
    delete: _delete
};
