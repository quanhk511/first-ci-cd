const ApiBusiness = require("@library/Business/Api");
const ExportDataBusiness = require("@library/Business/ExportData");

// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/ExportData/Index.js";

const exportData = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params && params.type) {
            switch (params.type) {
                case "export": {
                    const {error, fullPath, fileName} = await ExportDataBusiness.exportData(params) || {};

                    if (error) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: error
                        });
                    }

                    if (fileName && fullPath) {
                        return res.download(fullPath, fileName);
                    }

                    break;
                }
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

module.exports = {
    exportData,
}