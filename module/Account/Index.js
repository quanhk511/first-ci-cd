const ApiBusiness = require("@library/Business/Api");
const AccountBusiness = require("@library/Business/Account");


// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/Form/Index.js";

const get = async (req, res) => {
    try {
        const params = formatRequestParams(req);
        // console.log(params);
        const result = await AccountBusiness.getUserInformation(params);
        // console.log(result);
        // console.log(JSON.stringify(result));
        // console.log(result);
        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }

};


module.exports = {
    get
};
