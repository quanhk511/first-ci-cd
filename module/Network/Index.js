const ApiBusiness = require("@library/Business/Api");

// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/Network/Index.js";

const get = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        // Hard code
        let result;
        result = {
            name: "This is a network name",
            image: "//c0-vcdn.anthill.vn/logo/ants.png"
        }
        // if (!result) {
        //     return ApiBusiness.response(res, {
        //         code: 400,
        //         message: "Cannot get published form",
        //     });
        // }
        return ApiBusiness.response(res, {data: result});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }

};

module.exports = {
    get
};
