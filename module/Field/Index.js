// Business
const ApiBusiness = require('@library/Business/Api');
const FieldBusiness = require('@library/Business/Field');

// Utils
const {handleError, formatRequestParams} = require('@library/Utils');

const PATH = '@module/Field/Index.js';

const create = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        const result = await FieldBusiness.createField(params);

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const getList = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params.type) {
            switch (params.type) {
                case 'get-all-field-type': {
                    let result = await FieldBusiness.getFieldTypes(params);

                    return ApiBusiness.response(res, result);
                }
                case "get-all-default-fields": {
                    let result = await FieldBusiness.getAllDefaultFields(params);

                    return ApiBusiness.response(res, result);
                }
                case 'get-all-your-fields': {
                    let result = await FieldBusiness.getAllYourFields(params);

                    return ApiBusiness.response(res, result);
                }
                case 'test-index-es': {
                    let result = await FieldBusiness.testEs(params);

                    if (result) {
                        return ApiBusiness.response(res, result);
                    }

                    return ApiBusiness.response(res, result);
                }
                case 'test-get-es': {
                    let result = await FieldBusiness.testGetEs(params);

                    if (result) {
                        return ApiBusiness.response(res, result);
                    }

                    return ApiBusiness.response(res, result);
                }
                case 'validate-name': {
                    let result = await FieldBusiness.isFieldNameValid(params);

                    return ApiBusiness.response(res, result);
                }
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }

        // if (req.params && req.params.id && type) {
        //     let result = {
        //         code: 200,
        //         message: 'Success',
        //         data: params
        //     };
        //     return ApiBusiness.response(res, result);
        // }
        return ApiBusiness.response(res, {code: 400, message: 'Request invalid'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const get = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        const result = await FieldBusiness.getField(params);

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const update = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        const result = await FieldBusiness.updateField(params);

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

module.exports = {
    create,
    getList,
    get,
    update,
};