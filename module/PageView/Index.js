// Business
const ApiBusiness = require("@library/Business/Api");

// Utils
const {formatRequestParams, handleError, writeLog, distributeRabbitMqMessage} = require("@library/Utils");

const PATH = "@module/PageView/Index.js";

const create = async (req, res) => {
    // Log file name
    const fileSuccess = "Module_Create_Page_View_Success";
    const fileError = "Module_Create_Page_View_Error";

    // Log params
    let arrLog = {};

    try {
        const params = formatRequestParams(req);
        params.ip = req.ip;

        const rabbitMqArgs = {
            instanceName: "info_form",
            exchangeName: "page_view",
            queueName: "page_view",
            routingKey: "page_view.*",
            numberWorker: 6,
        };
        arrLog.rabbit_mq_message = {
            ...rabbitMqArgs,
            className: "PageView",
            functionName: "create",
            msg: params
        };

        const result = await distributeRabbitMqMessage(rabbitMqArgs, "PageView", "create", params);

        arrLog.result = result;
        writeLog(fileSuccess, arrLog);

        return ApiBusiness.response(res, {data: {result}});
    } catch (error) {
        arrLog.error = error;
        writeLog(fileError, arrLog);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

module.exports = {
    create,
}