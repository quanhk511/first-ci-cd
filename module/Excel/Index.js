const Excel = require("@library/Excel");

// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/Excel/Index.js";

const exportData = async (res, req) => {
    try {
        const params = formatRequestParams(res)
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}