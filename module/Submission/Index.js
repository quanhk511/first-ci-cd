// Business
const ApiBusiness = require("@library/Business/Api");
const FieldBusiness = require("@library/Business/Field");

// Model
const FieldModel = require("@library/Model/Field");

// Library
const SizeOf = require("image-size");
const Path = require("path");

// Utils
const {formatRequestParams, handleError, writeLog, distributeRabbitMqMessage} = require("@library/Utils");

const PATH = "@module/Submission/Index.js";

const create = async (req, res) => {
    // Log file name
    const fileSuccess = "Module_Create_Submission_Success";
    const fileError = "Module_Create_Submission_Error";

    // Log params
    let arrLog = {};

    try {
        const params = formatRequestParams(req);

        const rabbitMqArgs = {
            instanceName: "info_form",
            exchangeName: "submission",
            queueName: "submission",
            routingKey: "submission.*",
            numberWorker: 6,
        };
        arrLog.rabbit_mq_message = [
            {
                ...rabbitMqArgs,
                className: "Submission",
                functionName: "create",
                msg: params
            }
        ];

        const result = await distributeRabbitMqMessage(rabbitMqArgs, "Submission", "create", params);

        arrLog.result = result;
        writeLog(fileSuccess, result);

        return ApiBusiness.response(res, {data: {result}});
    } catch (error) {
        arrLog.error = error;
        writeLog(fileError, arrLog);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });

        return ApiBusiness.response(res, {code: 500, message: "Internal server error"});
    }
};

const upload = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        params.id = params.field_id;
        params.networkId = params.network_id;

        const {data} = await FieldBusiness.getField(params); // TODO ES

        if (!data || !data.field) {
            return ApiBusiness.response(res, {
                code: 404,
                message: `Cannot find field with ID: ${params.field_id}`,
            });
        }

        const {field} = data;
        const {allow_multiple_files} = field;

        const maxFileAmount = allow_multiple_files || 1;

        const result = req.files.slice(0, maxFileAmount).map(file => {
            const res = {
                url: file.path.substring(file.path.indexOf("/upload/")),
                extension: Path.extname(file.filename).slice(1),
                size: file.size,
                file_name: file.filename,
            };

            if (file.mimetype.indexOf("image/") >= 0) {
                const dimensions = SizeOf(file.path);

                res.dimensions = {
                    width: dimensions.width,
                    height: dimensions.height
                };
            }

            return res;
        });

        return ApiBusiness.response(res, {data: result});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

module.exports = {
    create,
    upload
}