const ApiBusiness = require("@library/Business/Api");
const FormBusiness = require("@library/Business/Form");


// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/Form/Index.js";

const getList = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params && params.type) {
            switch (params.type) {
                case "get-all-forms": {
                    let result = await FormBusiness.getAllForm(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get list of forms",
                        });
                    }

                    return ApiBusiness.response(res, result);
                }
                case "get-all-form-status": {
                    let result = await FormBusiness.getAllFormStatus(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get list of form statuses",
                        });
                    }

                    return ApiBusiness.response(res, result);
                }
                case "get-gdpr": {
                    let result = await FormBusiness.getAllGdprOptions(params);


                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get list of GDPR options",
                        });
                    }
                    return ApiBusiness.response(res, result);
                }
                case "get-es-form-version": {
                    let result = await FormBusiness.getAllESFormVersion(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get ES Form Version",
                        });
                    }
                    return ApiBusiness.response(res, result);
                }
                case "search-form": {
                    let result = await FormBusiness.searchForm(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot Search Form",
                        });
                    }
                    return ApiBusiness.response(res, result);
                }
                case "validate-form-name": {
                    const result = await FormBusiness.isFormNameExisted(params);

                    return ApiBusiness.response(res, result);
                }
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }

        return ApiBusiness.response(res, {
            code: 400,
            message: "Type not found",
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

// const createDraftFormES = async (req, res) => {
//     try {
//         const params = {
//             ...req.body
//         };
//         const result = await FormBusiness.insertEs(params);
//         return ApiBusiness.response(res, result);
//     } catch (error) {
//         handleError(error, {
//             path: PATH,
//             action: new Error().stack,
//         });
//     }
// }

const create = async (req, res) => {
    try {
        const params = formatRequestParams(req);
        const result = await FormBusiness.insertNewFormToDBandES(params);

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

const update = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params && params.type) {
            let result;
            switch (params.type) {
                case "revert-form-version":
                    result = await FormBusiness.revertFormVersion(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot revert form version",
                        });
                    }
                    return ApiBusiness.response(res, result);
                case "update":
                    result = await FormBusiness.updateDraftForm(params);

                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot update draft form",
                        });
                    }
                    return ApiBusiness.response(res, result);
                case "publish":
                    result = await FormBusiness.publishForm(params);
                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot publish form",
                        });
                    }
                    return ApiBusiness.response(res, result);
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }

        return ApiBusiness.response(res, {
            code: 400,
            message: "Type not found",
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

const get = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params && params.type) {
            let result;
            switch (params.type) {
                case "get-form-version-history":
                    result = await FormBusiness.getAllESFormVersion(params);
                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get form version history",
                        });
                    }
                    return ApiBusiness.response(res, result);
                case "get-form-detail":
                    result = await FormBusiness.getFormDetail(params);
                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get published form",
                        });
                    }
                    return ApiBusiness.response(res, result);
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }

};

const del = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        if (params && params.type) {
            let result;
            switch (params.type) {
                case "remove-form":
                    result = await FormBusiness.removeForm(params);
                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot get remove form",
                        });
                    }
                    return ApiBusiness.response(res, result);
                case "remove-unpublished-changes":
                    result = await FormBusiness.removeUnpublishedChanges(params);
                    if (!result) {
                        return ApiBusiness.response(res, {
                            code: 400,
                            message: "Cannot remove unpublished changes",
                        });
                    }
                    return ApiBusiness.response(res, result);
                default:
                    return ApiBusiness.response(res, {
                        code: 400,
                        message: "Type not found",
                    });
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
}

module.exports = {
    getList,
    create,
    update,
    get,
    del,
};
