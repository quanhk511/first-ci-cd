// Business
const ApiBusiness = require('@library/Business/Api');

// Utils
const {handleError, formatRequestParams} = require('@library/Utils');

const PATH = '@module/Example/Index.js';

const getList = async (req, res) => {
    try {
        const params = formatRequestParams(req);
        let type = req.query && req.query.type ? req.query.type : '';
        console.log("ssss");
        if (params.type) {
            switch (params.type) {
                case 'get-column': {
                    // const result = await MetricBusiness.getLatestModifyColumn(params);
                    //
                    // if (!result) {
                    //     return ApiBusiness.response(res, {code: 400, message: 'Cannot get latest modify column'});
                    // }

                    return ApiBusiness.response(res, result);
                }
            }
        }

        if (req.params && req.params.id && type) {
            let result = {
                code: 200,
                message: 'Success',
                data: params
            };
            return ApiBusiness.response(res, result);
        }
        return ApiBusiness.response(res, {code: 400, message: 'Request invalidddd'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

module.exports = {
    getList
};