// Business
const ApiBusiness = require('@library/Business/Api');

// Utils
const {handleError} = require('@library/Utils');

const PATH = '@module/App/Info.js';

const getList = async (req, res) => {

    const params = {
        ...req.query
    };

    console.log(params)

    let result = {
        code: 200,
        message: 'Success',
        data: params
    };


    return ApiBusiness.response(res, result);
}

const get = async (req, res) => {

    const params = {
        ...req.query
    };

    console.log(params)

    let result = {
        code: 200,
        message: 'Success',
        data: params
    };


    return ApiBusiness.response(res, result);
}

const update = async (req, res) => {
    try {
        let type = req.query && req.query.type ? req.query.type : '';

        const params = {
            ...req.body
        };

        if (req.params && req.params.id && type) {
            let response = null;

            switch (type) {
                case 'frontend-version':
                    // Change version when frontend
                    response = await AppInfoBusiness.changeFrontendVersion({
                        version: req.params.id
                    });
                    break;
            }

            if (response && response.error) {
                return ApiBusiness.error(res, 400, response.error);
            } else {
                let result = {
                    code: 200,
                    message: 'Success',
                    data: response
                };

                return ApiBusiness.response(res, result);
            }
        }

        return ApiBusiness.response(res, { code: 400, message: 'Request invalid' });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

module.exports = {
    update,
    getList,
    get
};
