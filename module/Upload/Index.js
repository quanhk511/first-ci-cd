const ApiBusiness = require("@library/Business/Api");

// Library
const Axios = require("axios");
const FS = require("fs");
const Joi = require("@hapi/joi");
const Logger = require("@library/Logger");
const MD5 = require("md5");
const Moment = require("moment");
const Sharp = require("sharp");
const SizeOf = require("image-size");
const Path = require("path");
const Url = require("url");

// Utils
const util = require("util");
const {formatRequestParams, handleError, getObjectPropSafely} = require("@library/Utils");

// Constants
const Constant = require("@config/common/constant");
const PATH = "@module/Upload/Index.js";

const uploadImage = async (req, res) => {
    let result = {};

    try {
        const {file} = req;

        if (!file) {
            return ApiBusiness.response(res, {
                code: 413,
                message: "Upload file not found",
            })
        }

        if (file.mimetype.indexOf(("image/")) < 0) {
            return ApiBusiness.response(res, {
                code: 413,
                message: "Invalid image file",
            });
        }

        const imageData = {
            image: {
                domain: Constant.get("STATIC_DOMAIN"),
                path: file.path.substring(file.path.indexOf("/upload/")),
            },
            extension: Path.extname(file.filename).slice(1),
            size: file.size,
            file_name: file.filename,
        };

        const dimensions = SizeOf(file.path);
        imageData.dimensions = {
            width: dimensions.width,
            height: dimensions.height
        };

        result = {
            code: 200,
            message: "Upload successfully",
            data: imageData
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });

        result = {
            code: 500,
            message: "Cannot upload image",
        }
    }

    return ApiBusiness.response(res, result);
}

const cropImage = async (req, res) => {
    let result = {};

    try {
        let params = formatRequestParams(req);
        params.file = req.file;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * source_image is a string url, and required if there is no file uploaded
        // * image is a uploaded file and required if there is no source_image
        // * ad_image_x is a non-zero number and required
        // * ad_image_y is a non-zero number and required
        // * ad_image_w is a non-zero number and required
        // * ad_image_h is a non-zero number and required
        const validator = Joi.object({
            source_image: Joi.string().uri({allowRelative: true}).optional(),
            ad_image_x: Joi.number().min(0).required(),
            ad_image_y: Joi.number().min(0).required(),
            ad_image_w: Joi.number().min(0).required(),
            ad_image_h: Joi.number().min(0).required(),
        }).unknown();

        if (validator.validate(params).error) {
            result = {
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid"
                ),
            };

            return ApiBusiness.response(res, result);
        }

        if (!params.source_image && !params.file) {
            result = {code: 400, message: `"source_image" or upload file is required`};

            return ApiBusiness.response(res, result);
        }

        //endregion

        let file, srcFilePath;
        let imageBuffer;
        let croppedFileName, croppedFilePath;

        // source_image is not passed, crop file from upload instead
        if (!params.source_image) {
            file = params.file;

            if (!file) {
                return ApiBusiness.response(res, {code: 413, message: "Upload file not found"})
            }

            if (file.mimetype.indexOf(("image/")) < 0) {
                return ApiBusiness.response(res, {code: 413, message: "Forbidden file type"});
            }

            croppedFileName = MD5(`${file.filename}:${Moment()}`) + Path.extname(file.originalname);

            srcFilePath = file.path.toString();
            croppedFilePath = srcFilePath.replace(file.filename, croppedFileName);
        }
        // otherwise, crop file from URL
        else {
            // Validate URL content-type
            const headResponse = await Axios.head("https:" + params.source_image);

            Logger.trace("Axios.head() response.headers: ", util.inspect(headResponse.headers, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

            const contentType = headResponse.headers["content-type"] || "";
            if (contentType.indexOf("image") === -1) {
                return ApiBusiness.response(res, {code: 413, message: "Forbidden file type"});
            }

            // Get resource image as buffer
            const getResponse = await Axios.get("https:" + params.source_image, {responseType: "arraybuffer"});

            Logger.trace("Axios.get() response.data: ", util.inspect(getResponse.data, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

            imageBuffer = Buffer.from(getResponse.data || "", "utf-8");

            const parsedUrl = Url.parse("https:" + params.source_image);

            croppedFileName = Path.basename(parsedUrl.pathname).slice(Math.max(parsedUrl.pathname.lastIndexOf("/"), 0));
            croppedFilePath = Constant.get("ROOT_PATH") + Constant.get("STATIC_PATH") + "/upload/"
                + Moment().format("YYYY/MM/DD");

            if (!FS.existsSync(croppedFilePath)) {
                try {
                    FS.mkdirSync(croppedFilePath, {recursive: true})
                } catch (err) {
                    Logger.error("Error when creating directory: ", err);

                    throw err;

                }
            }

            croppedFilePath += croppedFileName;
        }


        const croppedFileInfo = await Sharp(srcFilePath || imageBuffer, {sequentialRead: true})
            .extract({
                left: parseInt(+params.ad_image_x),
                top: parseInt(+params.ad_image_y),
                width: parseInt(+params.ad_image_w),
                height: parseInt(+params.ad_image_h),
            })
            .toFile(croppedFilePath);

        const imageData = {
            image: {
                domain: Constant.get("STATIC_DOMAIN"),
                path: croppedFilePath.substring(croppedFilePath.indexOf("/upload/")),
            },
            extension: Path.extname(croppedFileName).slice(1),
            size: croppedFileInfo.size,
            file_name: croppedFileName,
        };

        imageData.dimensions = {
            width: croppedFileInfo.width,
            height: croppedFileInfo.height
        };

        result = {
            code: 200,
            message: "Upload and crop successfully",
            data: imageData
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });

        return ApiBusiness.response(res, {
            code: 500,
            message: "Internal server error",
        });
    }

    return ApiBusiness.response(res, result);
}


module.exports = {
    cropImage,
    uploadImage
};
