// Business
const ApiBusiness = require('@library/Business/Api');
const FilterBusiness = require('@library/Business/Filter');

// Utils
const {handleError, formatRequestParams} = require('@library/Utils');

const PATH = '@module/Filter/Index.js';

const getList = async (req, res) => {
    const params = formatRequestParams(req);

    try {
        if (params.type) {
            switch (params.type) {
                case 'get-list-filter': { // get list filter metrics
                    let result = await FilterBusiness.getFilter(params);

                    return ApiBusiness.response(res, result);
                }
                case 'get-saved-filter': { // get list of saved filters
                    let result = await FilterBusiness.getSavedFilter(params);

                    return ApiBusiness.response(res, result);
                }
                case 'check-name': {
                    let result = await FilterBusiness.checkFilterName(params);

                    if (result) {
                        return ApiBusiness.response(res, result);
                    }

                    return ApiBusiness.response(res, {code: 400, message: 'Cannot check filter name'});
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const get = () => {};

const create = async (req, res) => {
    const params = formatRequestParams(req);

    try {
        if (params.type) {
            switch (params.type) {
                case 'save-filter': {
                    let result = await FilterBusiness.createFilter(params);

                    return ApiBusiness.response(res, result);
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const update = async (req, res) => {
    const params = {
        ...req.body,
        ...req.params,
        ...req.query
    };

    try {
        if (params.type) {
            switch (params.type) {
                case 'update-filter': {
                    let result = await FilterBusiness.updateFilter(params);

                    if (result) {
                        return ApiBusiness.response(res, result);
                    }

                    return ApiBusiness.response(res, {code: 400, message: 'Cannot update filter'});
                }
                default: {
                    return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
                }
            }
        }

        return ApiBusiness.response(res, {code: 400, message: 'Type not found'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const del = async (req, res) => {
    const params = {
        ...req.body,
        ...req.params,
        ...req.query
    };

    try {
        let result = await FilterBusiness.removeFilter(params);

        if (result) {
            return ApiBusiness.response(res, result);
        }

        return ApiBusiness.response(res, {code: 400, message: 'Cannot delete filter'});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

module.exports = {
    getList,
    get,
    create,
    update,
    del
};
