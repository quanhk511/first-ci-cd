const ApiBusiness = require("@library/Business/Api");
const MenuBusiness = require("@library/Business/Menu");

// Utils
const {formatRequestParams, handleError} = require("@library/Utils");

const PATH = "@module/Menu/Index.js";

const getList = async (req, res) => {
    try {
        const params = formatRequestParams(req);

        const result = await MenuBusiness.getListMenu(params);

        if (!result) {
            return ApiBusiness.response(res, {
                code: 400,
                message: "Cannot get list of Menu",
            });
        }

        return ApiBusiness.response(res, result);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
        });
    }
};


module.exports = {
    getList,
};
