require('dotenv').config();

// development: pm2 start|stop|delete|restart ecosystem.config.js
// sandbox/production: PM2_HOME=/sandbox/data/www/public_html/antalyser.ants.vn/v3.1 start|stop|delete|restart ecosystem.config.js

const DEPLOY_USER = process.env.DEPLOY_USER || 'phuocnc';
const DEBUG_PORT = process.env.DEBUG_PORT || 9230;
const APPLICATION_ENV = process.env.APPLICATION_ENV || 'development';
const PORT = process.env.PORT || 8001;
const WATCH_FOLDER = ['library', 'locale', 'module', 'routes', 'utils', 'views', 'config'];
const WATCH_WORKER = process.env.APPLICATION_ENV !== 'production' ? ['worker/*.js', 'worker/Task/*.js'] : false;
const WATCH = process.env.APPLICATION_ENV !== 'production' ? WATCH_FOLDER : false;
const IGNORE_WATCH = ['static', '*.pdf', 'myUserDataDir', 'node_modules', 'worker/Task/myUserDataDir'];

// eslint-disable-next-line no-console
console.log('APPLICATION_ENV', process.env.APPLICATION_ENV);
// eslint-disable-next-line no-console
console.log('DEBUG_PORT', DEBUG_PORT);
// eslint-disable-next-line no-console
console.log('DEPLOY_USER', DEPLOY_USER);

const ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV
};

const WORKER_ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV
};

module.exports = {
    apps: [
        {
            name: `antalyser-v3.1-debug.${APPLICATION_ENV}`,
            script: 'public/index.js',
            env: ENV_PARAMS,
            watch: WATCH,
            ignore_watch: IGNORE_WATCH,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '500M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `worker-admin-debug.${APPLICATION_ENV}`,
            script: 'worker/worker-admin.js',
            env: WORKER_ENV_PARAMS,
            watch: WATCH_WORKER,
            ignore_watch: IGNORE_WATCH,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `worker-export-debug.${APPLICATION_ENV}`,
            script: 'worker/worker-export.js',
            env: WORKER_ENV_PARAMS,
            watch: WATCH_WORKER,
            ignore_watch: IGNORE_WATCH,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        }
    ]
};
