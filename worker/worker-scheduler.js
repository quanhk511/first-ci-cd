/* eslint-disable no-console */
require('module-alias/register');
require('dotenv').config();

// Library
const Moment = require("moment");
const Schedule = require("node-schedule");

// Task
const FileHandlerTask = require("@worker/Task/FileHandler");
const PageViewTask = require("@worker/Task/PageView");

Schedule.scheduleJob("0 0 0 * * *", function () {
    console.log("Worker Scheduler is running FileHandlerTask.remove() at ", Moment().format("YYYY-MM-DD HH:mm:ss"));

    FileHandlerTask.remove({path: Moment().subtract(3, "days").format("YYYY/MM/DD")});
});


const rule = new Schedule.RecurrenceRule();
rule.minute = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

Schedule.scheduleJob(rule, function () {
    console.log("Worker Scheduler is running PageViewTask.bulkToDb() at ", Moment().format("YYYY-MM-DD HH:mm:ss"));

    PageViewTask.bulkToDb();
})
