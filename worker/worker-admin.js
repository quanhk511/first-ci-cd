/* eslint-disable no-console */
require('module-alias/register');
require('dotenv').config();

const WorkerAdmin = require('@library/RabbitMQ/worker-admin');

// Utils
const monitor = require('@antscorp/monitor-nodejs');
const Constant = require('@config/common/constant');

WorkerAdmin.startConsumer().then(() => {
    // Init monitor
    monitor.init({
        pid: Constant.get('MONITOR_PID') || 1583495368760,
        env: process.env.APPLICATION_ENV,
        host: Constant.get('API_MONITOR')
    });

    console.log('Worker consumer admin is running');
});
