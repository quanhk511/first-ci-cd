/* eslint-disable no-console */
require('module-alias/register');
require('dotenv').config();

// Library
const RabbitMQ = require('@library/RabbitMQ/Index');

// Utils
const monitor = require('@antscorp/monitor-nodejs');
const Constant = require('@config/common/constant');

const WorkerSubmission = new RabbitMQ({
    instanceName: "info_form",
    exchangeName: "submission",
    queueName: "submission",
    routingKey: "submission.*",
    numberWorker: 6,
});

WorkerSubmission.startConsumer().then(() => {
    // Init monitor
    monitor.init({
        pid: Constant.get('MONITOR_PID') || 1583495368760,
        env: process.env.APPLICATION_ENV,
        host: Constant.get('API_MONITOR')
    });

    console.log('Worker Submission Consumer is running');
});
