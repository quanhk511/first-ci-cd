require('module-alias/register');
require('dotenv').config();
// Redis
const Redis = require("@library/Redis")
const {promisify} = require('util');

// Business
const FormElasticSearch = require('@library/ElasticSearch/Form');
const CommonElasticSearch = require('@library/ElasticSearch/Common');

const MailSender = require('@worker/Task/MailSender');

const FormBusiness = require('@library/Business/Form');

const run = async () => {
    // let params = {};
    // const {
    //     networkId = 431999,
    //     formName = "dummy data",
    //     properties = {},
    //     emailNotification = [],
    //     shareLink = "dummy data",
    //     status = 22,
    //     userId = 1,
    //     ctime = '2020-09-09 00:00:00'
    // } = params;
    //
    //
    // let data = await FormElasticSearch.index({
    //     "formId": +123123,
    //     "networkId": +networkId,
    //     "properties": JSON.stringify(properties),
    //     "listChanged": '{}',
    //     "status": status,
    //     "userId": userId,
    //     "ctime": ctime
    // }) || [];
    //
    //
    // console.log(data);

    // let key = 'form:1057';
    // const redis = Redis.getInstances('caching');
    // const hset = promisify(redis.hset).bind(redis);
    // const hget = promisify(redis.hget).bind(redis);
    // const hgetall = promisify(redis.hgetall).bind(redis);
    //
    // await hset(key, 'properties', '{test}');
    // await hset(key, 'properties1', '{test1}');
    // await hset(key, 'properties2', '{test2}');
    //
    // console.log(await hget(key, 'properties'));
    // console.log(await hgetall(key));

    // let key = 'notexisted';
    // const redis = Redis.getInstances('caching');
    // const hgetall = promisify(redis.hgetall).bind(redis);
    // const data = await hgetall(key);
    //
    // console.log(data);

    // await CommonElasticSearch.bulkData('form', {
    //     formId: [],
    //     networkId: 10507,
    //     isMapping: true
    // })
    // //
    // await CommonElasticSearch.bulkData('field', {
    //     fieldId: [],
    //     networkId: 10507,
    //     isMapping: true
    // })

    // const result = await CommonElasticSearch.bulkData('field', {
    //     fieldId: [],
    //     networkId: 10507,
    //     // isMapping: true
    //     // limit: 3,
    //     // page: 2,
    // });

    // await PageViewBusiness.indexES({
    //     formId: 1057,
    //     networkId: 10507
    // })

    // const PageViewBusiness = require("@library/Business/PageView")
    // PageViewBusiness.bulkToDb();

    // await MailSender.sendMail({
    //     form_id: 1057,
    //     network_id: 10507,
    //     submission_data: {
    //         field1: 'testing',
    //         field2: 'testing 2',
    //         field3: 'testing 3'
    //     }
    // })

    // return CommonElasticSearch.mapping('form_version');

    // const FileHandlerTask = require("@worker/Task/FileHandler");
    // FileHandlerTask.move({path: "/upload/2020/10/21/cfb5b5bd46dd276d518d129f4a2d1468.png"});


    // const result = await FormBusiness.recoverFormVersion([72810]);
    // console.log(result);

    const {moveFile} = require("@library/Utils");
    const moveRes = await moveFile(
        `${process.env.PWD}/static/upload/2020/11/10/23ffdaf00b6b8256e6833808c5ab13b5.jpg`,
        `${process.env.PWD}/data/cloud/ads`)

    console.log(moveRes)

};

run();


