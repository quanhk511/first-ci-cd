// Libraries
const Joi = require('@hapi/joi');
const Moment = require("moment");
const SlackChannel = require("@library/Slack");
const util = require("util");
const Constant = require('@config/common/constant');

// Model
const FieldModel = require("@library/Model/Field");
const FormModel = require("@library/Model/Form");
const SubmissionModel = require("@library/Model/Submission");

// Utils
const {handleError, getObjectPropSafely, writeLog, moveFile} = require('@library/Utils');

// Task
const MailSender = require('@worker/Task/MailSender');

// Constants
const constant = require('@config/common/constant');
const PATH = "@worker/Task/Submission.js";

const MAX_RETRY_ATTEMPTS = 3;
const MAX_RETRY_TIMEOUT = 3000;

const create = async params => {
    try {
        const result = await recordSubmission(params);

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    create,
}

/* ================= Private Methods ================= */

const recordSubmission = async params => {
    // Logs file name
    const fileSuccess = "Task_Record_Submission_Success";
    const fileError = "Task_Record_Submission_Error";

    // Logs params
    let arrLog = {params: params};

    try {
        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * form_id is number and required
        // * network_id is number and required
        const validator = Joi.object({
            form_id: Joi.number().required(),
            network_id: Joi.number().required(),
        }).unknown();

        if (validator.validate(params).error) {
            arrLog.error = getObjectPropSafely(
                () => validator.validate(params).error.details[0].message,
                "Request invalid",
            );

            writeLog(fileError, arrLog);

            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid",
                )
            };
        }
        //endregion

        params.formId = params.form_id;
        params.networkId = params.network_id;

        // Validate form existence
        const formDetail = await FormModel.getFormDetail(params);
        if (!Array.isArray(formDetail) || !formDetail.length) {
            arrLog.error = `Form ${params.form_id} is not existed`;
            writeLog(fileError, arrLog);

            return {
                code: 500,
                message: `Form ${params.form_id} is not existed`,
            };
        }

        const listFields = await FieldModel.getListFieldsByFormId(params) || [];

        if (!listFields.length) {
            arrLog.error = `Error when getting list Fields of Form ID: ${params.form_id}`;
            writeLog(fileError, arrLog);

            return {
                code: 500,
                message: `Error when getting list Fields of Form ID: ${params.form_id}`,
            };
        }

        // Format field value in params based on the list fields belonged to that form
        const listFieldEntries = await Promise
            .all(listFields.map(async field => {
                // Move files from static directory to cdn directory (if any)
                if (+field.field_type_id === FieldModel.FIELD_TYPE.FILE) {
                    if (params[field.field_id] && Array.isArray(params[field.field_id])) {
                        const paths = await Promise.all(params[field.field_id].map(async path => {
                                const fullSrcPath = constant.get("ROOT_PATH") + constant.get("STATIC_PATH") + path;
                                const fullDesPath = constant.get("UPLOAD_PATH");

                                let moveResult = await moveFile(fullSrcPath, fullDesPath);
                                return moveResult.replace(constant.get("UPLOAD_PATH"), "");
                            })
                        );

                        return [field.field_id, paths];
                    }
                }

                // Otherwise just return the field itself
                return [field.field_id, params[field.field_id]];
            }))
            .then(list => list.filter(([key, val]) => Boolean(val))); // Filter to get fields with data only

        // Convert the formatted field params entries back to the submission_data params and email_data
        params.submission_data = {};
        params.email_data = {};

        for (let [key, val] of listFieldEntries) {
            let field = listFields.find(x => +x.field_id === +key);
            if (field) {
                params.submission_data[+key] = val;

                // Format email data, only file name is needed if that field is file, GDPR is not being sent within email
                if (+field.field_type_id !== FieldModel.FIELD_TYPE.GDPR) {
                    // Since file type field is submitted as an array of file path, join method is needed
                    params.email_data[field.field_label] = (+field.field_type_id === FieldModel.FIELD_TYPE.FILE)
                        ? Array.isArray(val)
                            ? val.map(v => `
                                <a href="${Constant.get('C0_DOMAIN')}${v.toString()}">
                                    ${v.toString().substring(v.toString().lastIndexOf("/")).slice(1)}
                                </a>`)
                            : []
                        : (val || "");
                }
            }
        }

        params.submission_data.SUBMITTED_DATE = Moment().format("YYYY-MM-DD HH:mm:ss");

        // region Create new Submission data and retry 3 times when error occurs
        const result = await new Promise(((resolve, reject) => {
            let retryAttempts = 1;

            const createAndRetry = (n => {
                    return SubmissionModel.create(params)
                        .then(res => {
                            if (1 !== +res) {
                                if (n === 1) {
                                    return reject();
                                }

                                setTimeout(() => {
                                    retryAttempts++;

                                    return createAndRetry(n - 1);
                                }, MAX_RETRY_TIMEOUT);

                            }

                            arrLog.create_result = +res;

                            return resolve(+res);
                        })
                        .catch(error => {
                            if (n === 1) {
                                return reject(error);
                            }

                            setTimeout(() => {
                                retryAttempts++;

                                return createAndRetry(n - 1);
                            }, MAX_RETRY_TIMEOUT);
                        });
                }
            );

            const createResult = createAndRetry(MAX_RETRY_ATTEMPTS);

            arrLog.retry_times = retryAttempts;

            return createResult;
        }));
        // endregion

        if (!result) {
            throw new Error("Error when creating new Submission");
        }

        writeLog(fileSuccess, arrLog);

        await MailSender.sendMail(params);

        return result;
    } catch (error) {
        arrLog.error = error.message;
        writeLog(fileError, arrLog);

        // Send message to Slack
        const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
        await slack.sendMessage(
            `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when recording Submission
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(params,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                })}`);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};