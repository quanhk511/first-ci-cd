// Libraries
const CommonElasticSearch = require('@library/ElasticSearch/Common');
const Joi = require('@hapi/joi');
const Moment = require("moment");
const Redis = require("@library/Redis");
const SlackChannel = require("@library/Slack");
const util = require("util");

// Model
const PageViewModel = require("@library/Model/PageView");

// Utils
const {handleError, getObjectPropSafely, writeLog} = require('@library/Utils');

const PATH = "@worker/Task/PageView.js";

const MAX_RETRY_ATTEMPTS = 3;
const MAX_RETRY_TIMEOUT = 3000;

const REDIS_INSTANCE = "caching";

const bulkToDb = async () => {
    // Log file name
    const fileSuccess = "Task_Bulk_Page_View_To_DB_Success";
    const fileError = "Task_Bulk_Page_View_To_DB_Error";

    // Log params
    let arrLog = {
        create_result: [],
        retry_times: []
    };
    let errLog = {};

    const redisClient = await Redis.getInstances(REDIS_INSTANCE);
    const redisKey = `pv:${Moment().format("YYYYMMDD")}`;

    // Get all network ids from DB
    const matchKey = "*:*";
    let cursor = 0; // hscan current cursor

    // Iterate through all cursor, a.k.a. Full iteration
    do {
        const form_ids = [];
        const network_ids = [];
        const pageview_data_list = [];

        try {
            // Find all record match the pattern: PageView record within one hour
            const redisResult = await redisClient.hscan(redisKey, cursor, "MATCH", matchKey);

            if (redisResult && redisResult.length) {
                cursor = +redisResult[0]; // Current cursor index
                const entries = redisResult[1]; // List of [key, value] in Redis

                // Iterate through all records
                for (let i = 0, j = i + 1; i < entries.length - 1; i += 2, j += 2) {
                    const key = entries[i];
                    const value = entries[j];

                    const _ = key.split(":"); // Key example: form_id:network_id
                    const dateName = Moment().format("YYYY-MM-DD")

                    const form_id = _[0];
                    const network_id = _[1];
                    const pageview_data = {[dateName]: [{DATE_NAME: dateName, PAGEVIEW: +value}]};

                    form_ids.push(form_id);
                    network_ids.push(network_id);
                    pageview_data_list.push(pageview_data);
                }

                // Create PageView record and retry 3 times if any error occurs
                await new Promise(((resolve, reject) => {
                    let retryAttempts = 1;

                    const createAndRetry = (n => {
                        if (form_ids.length && network_ids.length && pageview_data_list.length) {
                            return PageViewModel.create({
                                form_ids,
                                network_ids,
                                pageview_data: pageview_data_list
                            })
                                .then(res => {
                                    if (+res !== 1) {
                                        if (n === 1) {
                                            return reject();
                                        }

                                        setTimeout(() => {
                                            retryAttempts++;

                                            return createAndRetry(n - 1);
                                        }, MAX_RETRY_TIMEOUT);
                                    }

                                    arrLog.create_result.push(+res);

                                    return resolve(+res);
                                })
                                .catch(error => {
                                    if (n === 1) {
                                        return reject(error);
                                    }

                                    setTimeout(() => {
                                        retryAttempts++;

                                        return createAndRetry(n - 1);
                                    }, MAX_RETRY_TIMEOUT);
                                });
                        }
                    });

                    const createResult = createAndRetry(MAX_RETRY_ATTEMPTS);

                    arrLog.retry_times.push(retryAttempts);

                    return createResult;
                }));
            }
        } catch (error) {
            errLog.error = error;
            writeLog(fileError, errLog);

            // Send message to Slack
            const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
            await slack.sendMessage(
                `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when recording PageView to DB
                    Environment: ${process.env.APPLICATION_ENV}
                    Error: ${error.message}
                    Params: ${util.inspect(params,
                    {
                        breakLength: Infinity,
                        compact: true,
                        depth: Infinity,
                        showHidden: true
                    })}`);
            handleError(error, {
                path: PATH,
                action: new Error().stack,
                args: {
                    params
                }
            });
        }
    } while (cursor > 0);

    writeLog(fileSuccess, arrLog);

}

const create = async params => {
    try {
        const result = await recordPageView(params);

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    bulkToDb,
    create,
}

/* ================= Private Methods ================= */

const indexES = params => {
    try {
        const {
            formId = null,
            networkId = null,
            date = Moment().format("YYYY-MM-DD HH:mm:ss"),
            pageView = 1,
            ip,
            userAgent,
            referer,
            resolution
        } = params;

        const data = {
            form_id: +formId,
            network_id: +networkId,
            date: date,
            page_view: pageView,
            ip: ip,
            user_agent: userAgent,
            referer: referer,
            resolution: resolution
        };

        return CommonElasticSearch.index('page_view_' + Moment().format("YYYYMMDD"), data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const recordPageView = async params => {
    // Log file name
    const fileSuccess = "Task_Record_Page_View_Success";
    const fileError = "Task_Record_Page_View_Error";

    // Log params
    let arrLog = {params};

    try {
        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * form_id is number and required
        // * network_id is number and required
        const validator = Joi.object({
            form_id: Joi.number().required(),
            network_id: Joi.number().required(),
        }).unknown();

        if (validator.validate(params).error) {
            arrLog.error = getObjectPropSafely(
                () => validator.validate(params).error.details[0].message,
                "Request invalid",
            );
            writeLog(fileError, arrLog);

            return {
                code: 200,
                data: {},
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid",
                )
            };
        }
        //endregion

        params.formId = params.form_id;
        params.networkId = params.network_id;
        params.userAgent = params.user_agent;

        // Call Redis
        const redisInstance = await Redis.getInstances(REDIS_INSTANCE);
        const redisKey = `pv:${Moment().format("YYYYMMDD")}`;
        const redisField = `${params.formId}:${params.networkId}`;

        arrLog.redis_record = {
            instance: REDIS_INSTANCE,
            [redisKey]: [redisField]
        };

        const redisResult = await redisInstance.hincrby(redisKey, redisField, 1);

        // Call ES
        if (redisResult) {
            const esResult = await indexES(params);

            arrLog.redis_record[redisKey][redisField] = redisResult;
            arrLog.es_result = esResult;
            writeLog(fileSuccess, arrLog);

            return esResult;
        }

        throw new Error("Error when increasing record value in Redis");
    } catch (error) {
        arrLog.error = error;
        writeLog(fileError, arrLog);

        // Send message to Slack
        const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
        await slack.sendMessage(
            `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when indexing PageView to Redis and ElasticSearch
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(params,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                })}`);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};