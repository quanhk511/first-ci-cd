// Libraries
const CommonElasticSearch = require('@library/ElasticSearch/Common');

// Utils
const {handleError} = require('@library/Utils');

const PATH = "@worker/Task/DataSync.js";

const bulkToES = async (params) => {
    try {
        return await CommonElasticSearch.bulkData(params.objectName, params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

module.exports = {
    bulkToES,
};