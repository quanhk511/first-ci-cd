const PATH = "@worker/Task/FileHandler.js";

// Constants
const constant = require('@config/common/constant');

// Utils
const {removeDir} = require("@library/Utils");

const remove = async params => {
    const {path} = params;

    const fullSrcPath = constant.get("ROOT_PATH") + constant.get("STATIC_PATH") + '/upload/' + path;

    removeDir(fullSrcPath);
}
module.exports = {
    remove
}