// Library
const EmailClient = require('@library/Email')
const Logger = require("@library/Logger");

// Business
const FormBusiness = require('@library/Business/Form');
const NetworkBusiness = require("@library/Business/Network");

// Utils
const util = require("util");
const {handleError, toCapital} = require('@library/Utils');

// Constants
const Constant = require('@config/common/constant');
const PATH = 'worker/Task/MailSender.js';

const getFormData = async (params) => {
    try {
        const {
            formId = null,
            networkId = null
        } = params;

        const result = await FormBusiness.getFormDetail({
            id: +formId,
            networkId: +networkId
        });

        if (result.data) {
            return result.data;
        }

        return null;

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

const sendMail = async (params) => {
    try {
        const {
            form_id = null,
            network_id = null,
            email_data = []
        } = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const formData = await getFormData({
            formId: +form_id,
            networkId: +network_id
        });

        Logger.trace("getFormData() result: ", util.inspect(formData, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!formData) {
            return null;
        }

        // Default language
        const {language = Constant.get("DEFAULT_LANGUAGE")} = formData;

        Logger.trace("EmailClient.getEmailTemplate() params: ", util.inspect(
            {
                network_id: +network_id,
                language,
                payload: {
                    // my dynamic data
                    formName: formData.form_name,
                    networkId: network_id,
                    url: Constant.get('SITE_URL') + '/' + network_id + '/#/' + formData.user_id + '/form/' + form_id,
                    fields: email_data,
                }
            },
            {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }
        ));

        // Get Network Info
        const networkInfo = await NetworkBusiness.getNetworkInfo({network_id})
            .then(res => {
                const {data: {infoNetwork = null} = {}} = res;

                return infoNetwork;
            });

        Logger.trace("NetworkBusiness.getNetworkInfo() result: ", util.inspect(networkInfo, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!networkInfo) {
            return null;
        }

        // Get Email Template
        //language: vie or eng
        const emailTemplate = EmailClient.getEmailTemplate(
            +network_id,
            language,
            // Dynamic data to inject to template
            {
                formName: formData.form_name,
                network: networkInfo.networkName,
                url: Constant.get('SITE_URL') + '/' + network_id + '/#/' + formData.user_id + '/form/' + form_id,
                fields: email_data,
            }
        );

        Logger.trace("EmailClient.getEmailTemplate() result: ", util.inspect(emailTemplate, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        let {emailNotification: emailsStr = []} = formData;
        emailsStr = emailsStr.join(',');

        if (emailsStr) {
            const sender = EmailClient.getDefaultSender(network_id);
            const emailConfig = {
                from: sender,
                to: emailsStr,
                subject: `New submission on Form ${formData.form_name}`,
                template: emailTemplate
                // template: "aaaa"
            };

            await EmailClient.sendEmail(emailConfig);
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

module.exports = {
    sendMail
};
