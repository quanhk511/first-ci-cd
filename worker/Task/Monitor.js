const {handleError} = require('@library/Utils');

const PATH = '@worker/Task/Monitor.js';

const monitorError = async (args) => {
    handleError(new Error('This is test worker error'), {
        path: PATH,
        actionName: 'monitorError',
        args: {
            args
        }
    });
};

module.exports = {
    monitorError
};
