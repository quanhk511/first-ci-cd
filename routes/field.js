const Field = require('@module/Field/Index');

const routes = {
    index: '/api/field/index'
};

module.exports = (app) => {
    // Index
    app.route(routes.index)
        .get((req, res) => {
            Field.getList(req, res);
        })
        .post((req, res) => {
            Field.create(req, res);
        });

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            Field.get(req, res);
        })
        .put((req, res) => {
           Field.update(req, res);
        });

};
