const PublicForm = require("@module/PublicForm/Index");

const routes = {
    index: "/api/published/form"
};

module.exports = (app) => {

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            PublicForm.get(req, res);
        })
};
