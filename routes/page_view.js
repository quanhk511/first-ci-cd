// Modules
const PageView = require("@module/PageView/Index");

const routes = {
    index: "/page-view"
}

module.exports = (app) => {
    app.get('/', (req, res) => {
        res.send('Page View');
    });

    app.get('/ping', (req, res) => {
        console.log("ping");
        return res.send({
            'code': 200,
            'message': 'Success'
        });
    });

    app.route(routes.index)
        .post((req, res) => {
            PageView.create(req, res);
        });
};