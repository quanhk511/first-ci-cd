const Column = require('@module/Column/Index');

const routes = {
    index: '/api/column/index'
};

module.exports = app => {
    // Column index
    app.route(routes.index)
        .get((req, res) => {
            Column.getList(req, res);
        })
        .post((req, res) => {
            Column.create(req, res);
        });
    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            Column.get(req, res);
        })
        .put((req, res) => {
            Column.update(req, res);
        })
        .delete((req, res) => {
            Column.delete(req, res);
        });
};
