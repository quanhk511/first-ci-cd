const Form = require("@module/Form/Index");

const routes = {
    index: "/api/form/index"
};

module.exports = (app) => {
    // Index
    app.route(routes.index)
        .get((req, res) => {
            Form.getList(req, res);
        })
        .post((req, res) => {
            Form.create(req, res);
        });

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            Form.get(req, res);
        })
        .put((req, res) => {
            Form.update(req, res);
        })
        .delete((req, res) => {
            Form.del(req, res);
        });
};
