const Form = require("@module/Menu/Index");

const routes = {
    index: "/api/menu/index"
};

module.exports = (app) => {
    // Index
    app.route(routes.index)
        .get((req, res) => {
            Form.getList(req, res);
        });
};
