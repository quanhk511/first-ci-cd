const Filter = require('@module/Filter/Index');

const routes = {
    index: '/api/filter/index'
};

module.exports = (app) => {
    // Report Index
    app.route(routes.index)
        .get((req, res) => {
            Filter.getList(req, res);
        })
        .post((req, res) => {
            Filter.create(req, res);
        });

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            Filter.get(req, res);
        })
        .put((req, res) => {
            Filter.update(req, res);
        })
        .delete((req, res) => {
            Filter.del(req, res);
        });
};
