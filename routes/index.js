
// Routes
const middleware = require('./middleware');
const application = require('./app');
const example = require('./example');
const column = require('./column');
const filter = require('./filter');
const form = require('./form');
const menu = require('./menu');
const field = require('./field');
const uploadImage = require('./upload');
const exportData = require("./export_data");
const network = require("./network");
const publicForm = require('./public_form');
const account = require('./account');

const path = require('path');
const {promisify} = require('util');

const Constant = require('@config/common/constant');

module.exports = (app, db) => {

    app.use(middleware);

    app.set('views', path.join(__dirname, '../views'));
    app.set("view engine","pug");

    app.get('/', async (req, res) => {
        let version = req.query.version || '';

        if (!version) {
            const Redis = require('@library/Redis');
            const redis = await Redis.getInstances('caching');
            let key = `form:frontend-version:networkId:${Constant.get('NETWORK_ID')}`;
            version = await redis.get(key);
        }

        const PORTAL_ID = req.get('PORTAL_ID');
        const VERSION = Constant.get('VERSION');
        const SITE_URL = PORTAL_ID ? Constant.get('SITE_URL') + '/' + PORTAL_ID : Constant.get('SITE_URL');
        const API_HOST = Constant.get('API_HOST');
        const API_LOGGING = Constant.get('API_LOGGING');
        const API_LOGGING_ERROR = Constant.get('API_LOGGING_ERROR');
        const API_ID = PORTAL_ID || Constant.get('NETWORK_ID');
        const PROJECT_ID = Constant.get('PROJECT_ID');
        const U_OGS = `${Constant.get('U_OGS')}_${API_ID}`;
        const AUTH_ADX_DOMAIN = Constant.get('AUTH_ADX_DOMAIN');
        const ST_OGS = Constant.get('ST_OGS');
        const UPLOAD_URL = Constant.get('UPLOAD_PATH');
        const LOGO_MAIN = Constant.get('LOGO_MAIN');
        const LOGO_SUB = Constant.get('LOGO_SUB');
        let ASSETS_URL = Constant.get('ASSETS_URL');
        const APPLICATION_ENV = process.env.APPLICATION_ENV || 'development';
        const SUBMISSION_API = Constant.get("SUBMISSION_API");
        const PAGEVIEW_API = Constant.get("PAGEVIEW_API");
        const PERMISSION_DOMAIN = Constant.get("PERMISSION_DOMAIN");
        const APP_ID = Constant.get("APP_ID");

        const STATIC_DOMAIN = Constant.get("STATIC_DOMAIN");
        const C0_DOMAIN = Constant.get("C0_DOMAIN");

        ASSETS_URL = ASSETS_URL + '/' + version;

        res.render('index', {
            VERSION,
            SITE_URL,
            API_HOST,
            API_LOGGING,
            API_LOGGING_ERROR,
            API_ID,
            PROJECT_ID,
            U_OGS,
            AUTH_ADX_DOMAIN,
            ST_OGS,
            UPLOAD_URL,
            LOGO_MAIN,
            LOGO_SUB,
            ASSETS_URL,
            APPLICATION_ENV,
            SUBMISSION_API,
            PAGEVIEW_API,
            PERMISSION_DOMAIN,
            STATIC_DOMAIN,
            C0_DOMAIN,
            APP_ID
        });
    });

    app.get('/ping', (req, res) => {
        return res.send({
            'code': 200,
            'message': 'Success'
        });
    });

    application(app, db);
    example(app, db);
    column(app, db);
    filter(app, db);
    form(app, db);
    menu(app, db);
    field(app, db);
    uploadImage(app, db);
    exportData(app, db);
    network(app);
    publicForm(app);
    account(app);

};
