const ExportData = require("@module/ExportData/Index");

const routes = {
    index: "/api/export"
};

module.exports = (app) => {
    app.route(routes.index)
        .get((req, res) => {
            ExportData.exportData(req, res);
        });
};
