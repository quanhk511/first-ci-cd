const Network = require("@module/Network/Index");


const routes = {
    index: '/api/network/index'
};

module.exports = (app) => {

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            Network.get(req, res);
        });
};
