const AccountIndex = require('@module/Account/Index');
// const AccountAuth = require('@module/Account/Auth');

const routes = {
    index: '/api/account/index',
    // auth: '/api/account/auth'
};

module.exports = (app) => {
    // Index
    // app.route(routes.index)
    //     .get((req, res) => {
    //         AccountIndex.getList(req, res);
    //     })
    //     .post((req, res) => {
    //         AccountIndex.create(req, res);
    //     })
    //     .put((req, res) => {
    //         AccountIndex.update(req, res);
    //     })
    //     .delete((req, res) => {
    //         AccountIndex.delete(req, res);
    //     });

    app.route(`${routes.index}/:id`)
        .get((req, res) => {
            AccountIndex.get(req, res);
        });

    // Auth
    // app.route(routes.auth)
    //     .post((req, res) => {
    //         AccountAuth.create(req, res);
    //     });
    //
    // app.route(`${routes.auth}/:connectorType`)
    //     .get((req, res) => {
    //         AccountAuth.getList(req, res);
    //     });
};
