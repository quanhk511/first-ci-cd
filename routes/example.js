const ExampleIndex = require('@module/Example/Index');

const routes = {
    index: '/api/example/index',
};

module.exports = (app) => {
    // Index
    app.route(routes.index)
        .get((req, res) => {
            ExampleIndex.getList(req, res);
        })
};
