// Library
const Multer = require("multer");
const {DynamicFileSizeStorage} = require("@library/Multer/Index");

// Module
const UploadModule = require("@module/Upload/Index")

const routes = {
    index: "/api/uploadImage/index",
};

// const multer = require('multer');
// const upload = multer({dest: __dirname + '/uploads/images'});

// const uploadMulter = require('@library/Model/ModelMulter');

// const upload = Uploader.upload.single('image');

module.exports = (app) => {
    // Index
    app.route(routes.index)
        .post(
            (req, res, next) => {
                Multer({
                    fileFilter: function (req, file, cb) {
                        let isFileValid = file && (file.mimetype === "image/png" || file.mimetype === "image/jpeg");

                        return cb(isFileValid ? null : new Error("Invalid image file"), isFileValid);
                    },
                    storage: new DynamicFileSizeStorage({max_file_size: 3})
                }).single('image')(req, res, error => {
                    if (error) {
                        return res.send({
                            code: 413,
                            message: error.message
                        });
                    }

                    next();
                })
            },
            (req, res) => UploadModule.uploadImage(req, res)
        )
    ;

    // Crop
    app.route(`${routes.index}/crop`)
        .post((req, res, next) => {
                Multer({
                    fileFilter: function (req, file, cb) {
                        let isFileValid = (file.mimetype === "image/png" || file.mimetype === "image/jpeg");

                        return cb(isFileValid ? null : new Error("Invalid image file"), isFileValid);
                    },
                    storage: new DynamicFileSizeStorage({max_file_size: 3})
                }).single('image')(req, res, error => {
                    if (error) {
                        return res.send({
                            code: 413,
                            message: error.message
                        });
                    }

                    next();
                })
            },
            (req, res) => UploadModule.cropImage(req, res)
        )
};
