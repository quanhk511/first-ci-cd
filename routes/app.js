const AppInfoModule = require('@module/App/Info');

const routes = {
    info: '/api/app/info'
};

module.exports = (app) => {
    // App info APIs
    app.route(routes.info + '/:id')
        .put((req, res) => {
            AppInfoModule.update(req, res);
        })
        .get((req, res) => {
            AppInfoModule.get(req, res);
        });

    app.route(routes.info)
        .get((req, res) => {
            AppInfoModule.getList(req, res);
        });
};
