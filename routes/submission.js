// Library
const Multer = require("multer");
const {DynamicFileSizeStorage, DynamicFileFilter} = require("@library/Multer/Index");

// Modules
const Submission = require("@module/Submission/Index");

const routes = {
    index: "/submission",
    upload_files: "/submission/upload"
}

module.exports = (app) => {
    app.get('/', (req, res) => {
        res.send('Submission');
    });

    app.get('/ping', (req, res) => {
        console.log("ping");
        return res.send({
            'code': 200,
            'message': 'Success'
        });
    });

    app.route(routes.index)
        .post((req, res) => {
            Submission.create(req, res);
        });

    app.route(routes.upload_files)
        .post(
            (req, res, next) => {
                Multer({fileFilter: DynamicFileFilter, storage: new DynamicFileSizeStorage()})
                    .any()(req, res, error => {
                        if (error) {
                            return res.send({
                                code: 413,
                                message: error.message
                            });
                        }

                        next();
                    })
            },
            (req, res) => Submission.upload(req, res)
        );
};