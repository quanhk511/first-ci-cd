const TokenBusiness = require('@library/Business/Token');

const EXCLUDE_ROUTE = [
    '/api/ads/',
    '/ping',
    '^/$',
    '/api/published/form/[0-9]*',
    '/api/language/index',
    '/visualizations',
    '/api/account/authenticate',
    '/api/token/validate',
    '/permission',
];

module.exports = async (req, res, next) => {
    try {

        // Set global req params
        global.hostname = req.hostname;
        global.originalUrl = req.originalUrl;
        global.params = {
            ...req.body,
            ...req.params,
            ...req.query
        };

        // Send debug
        global.debug = (data) => {
            res.send(data);
        };

        // Build SP statement
        // global.buildSPLog = buildSPLog;

        if (req.path.includes('debug')) {
            next();
            return;
        }

        if (EXCLUDE_ROUTE.findIndex(pattern => new RegExp(pattern).test(req.path)) !== -1) {
            next();
        } else {
            // Check token
            if (req.method && req.query && req.query._token) {
                let params = req.query;

                const checkToken = await TokenBusiness.validate({
                    _token: req.query._token
                });

                const tokenInfo = (checkToken.data && checkToken.data.tokenInfo) || {};

                if (tokenInfo['valid']) {
                    if (!tokenInfo['allow_access']) {
                        // Response access denied
                        res.status(403).json({error: 'You are not authorized for this request'});
                    } else if (!+params['_user_id']) {
                        // Response error
                        res.status(403).json({error: 'You are not authorized for this request'});
                    } else {
                        // Validate success
                        req.userId = +params._user_id;
                        req.networkId = +tokenInfo.network_id;
                        req.managerId = +tokenInfo.manager_id;
                        req.token = params._token;

                        req.query = {
                            ...req.query,
                            _network_id: +tokenInfo.network_id,
                            _manager_id: +tokenInfo.manager_id
                        };

                        if (req.query.debug) {
                            // Log out cURL command when use axios request
                            const curlirize = require('@library/AxiosCurlirize');
                            const axios = require('axios');
                            const fixedAxios = require('axios-https-proxy-fix');

                            curlirize(axios);
                            curlirize(fixedAxios);
                        }

                        // Enable debug mode on global
                        global.debugMode = !!req.query.debug;
                        global.currentTime = Date.now();
                        global.params._network_id = +tokenInfo.network_id;
                        global.params._manager_id = +tokenInfo._manager_id;

                        next();
                    }
                } else {
                    res.status(401).json({error: 'Token expired'});
                }
            } else {
                res.status(403).json({error: 'You are not authorized for this request'});
            }
        }
    } catch (error) {
        // Error

    }
};
