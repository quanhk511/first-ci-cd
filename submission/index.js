require('module-alias/register');
require('dotenv').config();

// Library
const express = require('express');
const cors = require('cors');
const Logger = require("@library/Logger");

const app = express();
const port = process.env.SUBMISSION_PORT || 8000;

app.use(function (req, res, next) {
    global.hostname = (
        req.get('origin') &&
        req.get('origin').replace('https://', '').replace('http://', '')
    ) || req.hostname;
    next();
});

app.use(express.json({limit: "50mb"}));
app.use(express.urlencoded({limit: "50mb", extended: true}));
app.use(cors({origin: true}));

// Routes
require("../routes/submission")(app);

app.use(function onError(error, req, res, next) {
    Logger.error('error', error);
});

app.listen(port, () => {
    Logger.info('We are live on ' + port);
});

