require('dotenv').config();

const APPLICATION_ENV = process.env.APPLICATION_ENV || 'staging';
const PORT = process.env.PORT || 8001;
const UV_THREADPOOL_SIZE = process.env.UV_THREADPOOL_SIZE || 20;

const ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV,
    UV_THREADPOOL_SIZE: UV_THREADPOOL_SIZE
};

const WORKER_ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV
};

module.exports = {
    apps: [
        {
            name: `3rd_nodejs.${APPLICATION_ENV}`,
            script: 'public/index.js',
            env: ENV_PARAMS,
            watch: false,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '500M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `3rd_nodejs.worker-admin.${APPLICATION_ENV}`,
            script: 'worker/worker-admin.js',
            env: WORKER_ENV_PARAMS,
            watch: false,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `3rd_nodejs.worker-export.${APPLICATION_ENV}`,
            script: 'worker/worker-export.js',
            env: WORKER_ENV_PARAMS,
            watch: false,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        }
    ]
};
