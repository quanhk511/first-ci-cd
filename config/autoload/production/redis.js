module.exports = Object.freeze({
    redis: {
        caching: {
            host: '10.203.0.100',
            port: '6379',
            timeout: '0'
        }
    },
    redis_slave: {
        caching: {
            host: '10.203.0.100',
            port: '6380',
            timeout: '0'
        }
    }
});
