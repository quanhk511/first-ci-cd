module.exports = Object.freeze({
    elastic: {
        info_master: {
            host: '10.197.0.6',
            transportPort: '9304',
            httpPort: '9204',
            prefix: 'form_'
        }
    }
});
