module.exports = Object.freeze({
    elastic: {
        info_master: {
            host: '10.203.0.4',
            transportPort: '9304',
            httpPort: '9204',
            prefix: 'form_'
        }
    }
});
