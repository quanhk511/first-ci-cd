const fs = require('fs');

let constants = {};

const get = key => {
    try {
        let defaultConstants = {};
        const defaultConstantsPath = process.env.PWD + '/config/domain/' + process.env.APPLICATION_ENV + '/default.js';

        if (fs.existsSync(defaultConstantsPath)) {
            defaultConstants = require(defaultConstantsPath);
        }

        if (global.hostname) {
            let path = process.env.PWD + '/config/domain/' + process.env.APPLICATION_ENV + '/' + global.hostname + '.js';

            if (fs.existsSync(path)) {
                constants = require(path);
            }
        }

        let uploadPath;
        switch (process.env.APPLICATION_ENV) {
            case "development":
                uploadPath = `${process.env.PWD}/data/cloud/ads`;
                break;
            case "sandbox":
                uploadPath = "/sandbox/data/cloud/ads";
                break;
            case "production":
                uploadPath = "/data/cloud/ads";
                break;
            default:
                throw new Error("Invalid application environment");
        }

        let constant = {
            DEFAULT_LANGUAGE: "eng",
            NETWORK_ID: 10507,
            TOKEN_ROLE: 1,
            TOKEN_ROLE_ADMIN: 2,
            ROOT_PATH: process.env.PWD,
            STATIC_PATH: "/static",
            UPLOAD_PATH: uploadPath,
            ...defaultConstants,
            ...constants
        };

        if (key) {
            return constant[key] || null;
        } else {
            return constant;
        }
    } catch (e) {
        // Error
    }
};

// Constant object not changed
module.exports = {
    get
};
