module.exports = Object.freeze({
    API_HOST: '//form.ants.vn/api/',
    ASSETS_URL: '//st-form.anthill.vn/assets',
    NETWORK_ID: 10507,
    AUTH_ADX_DOMAIN: '//iam.ants.vn/',
    U_OGS: 'uogs',
    MONITOR_PID: '1583495368760',
    SUBMISSION_API: "//submission-form.ants.vn/",
    PAGEVIEW_API: "//pageview-form.ants.vn/"
});
