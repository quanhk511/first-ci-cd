module.exports = Object.freeze({
    API_HOST: '//sandbox-form.ants.vn/api/',
    ASSETS_URL: '//sandbox-st-form.anthill.vn/sandbox',
    NETWORK_ID: 10507,
    AUTH_ADX_DOMAIN: '//sandbox-aacm.ants.vn/',
    U_OGS: 'uogs_sand',
    MONITOR_PID: '1583495368760',
    SUBMISSION_API: "//sandbox-submission-form.ants.vn/",
    PAGEVIEW_API: "//sandbox-pageview-form.ants.vn/"
});
