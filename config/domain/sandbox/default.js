module.exports = Object.freeze({
    SITE_URL: 'sandbox-form.ants.vn',
    NETWORK_ID: 10507,
    NETWORK_ID_A1: 28992,
    U_OGS: 'uogs_sand',
    PERMISSION_DOMAIN: '//sandbox-permission.ants.vn',
    APP_ID: 218,
    MONITOR_PID: '1583495368760',
    AUTH_DOMAIN: '//sandbox-platform.ants.vn',
    STATIC_DOMAIN: "//sandbox-st-form.anthill.vn",
    C0_DOMAIN: "//sandbox-c0-form.anthill.vn",
    EMAIL_CONFIG: {
        10507: {
            FOLDER: 'NewSubmission',
            SENDER: {
                name: 'ANTS Platform',
                email: 'noreply@antsprogrammatic.com',
                password: '^Nv&fpU2Sikhfl9Jk*w#'
            }
        },
        28992: {
            FOLDER: 'NewSubmission',
            SENDER: {
                name: 'A1 Digihub',
                email: 'noreply@a1digihub.com',
                password: '-=X4aZ2F@v4Cqxj*'
            }
        }
    }
});
