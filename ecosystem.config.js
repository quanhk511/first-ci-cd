require('dotenv').config();

const DEBUG_PORT = process.env.DEBUG_PORT || 9230;
const DEBUG_WORKER_PORT = process.env.DEBUG_WORKER_PORT || 9231;
const APPLICATION_ENV = process.env.APPLICATION_ENV || 'development';
const PORT = process.env.PORT || 8001;
const UV_THREADPOOL_SIZE = process.env.UV_THREADPOOL_SIZE || 20;
const NODE_OPTIONS = [`--inspect=${DEBUG_PORT}`];
const NODE_WORKER_OPTIONS = [`--inspect=${DEBUG_WORKER_PORT}`];
const WATCH_FOLDER = ['library', 'locale', 'module', 'routes', 'utils', 'views', 'config'];
const WATCH_WORKER = process.env.APPLICATION_ENV !== 'production' ? ['worker/*.js', 'worker/Task/*.js'] : false;
const WATCH = process.env.APPLICATION_ENV !== 'production' ? WATCH_FOLDER : false;
const IGNORE_WATCH = ['static', '*.pdf', 'myUserDataDir', 'node_modules', 'worker/Task/myUserDataDir'];
const LOG_LEVEL = process.env.LOG_LEVEL || "OFF";

const ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV,
    UV_THREADPOOL_SIZE: UV_THREADPOOL_SIZE,
    LOG_LEVEL
};

const WORKER_ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV,
    LOG_LEVEL
};

module.exports = {
    apps: [
        {
            name: `form_nodejs.${APPLICATION_ENV}`,
            script: 'public/index.js',
            env: ENV_PARAMS,
            watch: WATCH,
            ignore_watch: IGNORE_WATCH,
            // node_args: NODE_OPTIONS,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '500M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        // {
        //     name: `form_nodejs.worker-admin.${APPLICATION_ENV}`,
        //     script: 'worker/worker-admin.js',
        //     env: WORKER_ENV_PARAMS,
        //     watch: WATCH_WORKER,
        //     ignore_watch: IGNORE_WATCH,
        //     node_args: NODE_WORKER_OPTIONS,
        //     watch_options: {
        //         'followSymlinks': true
        //     },
        //     max_memory_restart: '200M',
        //     log_date_format: 'YYYY-MM-DD HH:mm:ss'
        // },
        {
            name: `form_nodejs.worker-data-sync.${APPLICATION_ENV}`,
            script: 'worker/worker-data-sync.js',
            env: WORKER_ENV_PARAMS,
            watch: WATCH_WORKER,
            ignore_watch: IGNORE_WATCH,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `form_nodejs.worker-scheduler.${APPLICATION_ENV}`,
            script: 'worker/worker-scheduler.js',
            env: WORKER_ENV_PARAMS,
            watch: WATCH_WORKER,
            ignore_watch: IGNORE_WATCH,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        }
    ]
};
