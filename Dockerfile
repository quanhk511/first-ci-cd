FROM node:10.16-alpine

RUN npm install -g pm2

WORKDIR /app

COPY . .

RUN npm install

EXPOSE 8211

CMD pm2-runtime start ecosystem.sandbox.config.js
