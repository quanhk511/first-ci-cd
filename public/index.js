require('module-alias/register');
require('dotenv').config();

// Library
const express = require('express');
const cors = require('cors');
const Logger = require("@library/Logger");

const app = express();
const port = process.env.PORT_NODE || process.env.PORT || 8000;

// Init monitor

app.use(function (req, res, next) {
    global.hostname = (
        req.get('origin') &&
        req.get('origin').replace('https://', '').replace('http://', '')
    ) || req.hostname;
    next();
});

app.use(express.json());
app.use(cors({origin: true}));


// Routes
require('../routes')(app, {});

app.use(function onError(error, req, res, next) {
    Logger.error('error', error);
});

app.listen(port, () => {
    Logger.info('We are live on ' + port);
});

