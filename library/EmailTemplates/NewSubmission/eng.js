const getTemplate = (params) => {

    let htmlFields = '';
    for (const [key, value] of Object.entries(params.fields)) {
        htmlFields +=
            `<span style="padding-bottom: 10px;line-height: 1.4;color: #666;font-family: sans-serif;font-size: 13px;
                font-weight: normal;margin: 0;">
                    <strong>${key}:</strong> `;

        if (Array.isArray(value)) {
            htmlFields += `<ul style="margin:0px">`;

            htmlFields += value
                .map(val =>
                    `<li>
                        <span style="padding-bottom: 10px;line-height: 1.4;color: #666;font-family: sans-serif;font-size: 13px;
                        font-weight: normal;margin: 0;">
                            ${val}
                        </span>
                    </li>`)
                .join("");

            htmlFields += "</ul>";
        } else  {
            htmlFields += `${value}`;
        }

        htmlFields += `</span><p></p>`;
    }

    return `<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Template Email</title>
    <style>
        .btn-green:focus {
            outline: none;
        }
    </style>
</head>

<body class=""
    style="background-color: #e9f4f7;font-family: sans-serif;-webkit-font-smoothing: antialiased;font-size: 13px;line-height: 1.4;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body"
        style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;background-color: #e9f4f7;">
        <tr>
            <td class="container"
                style="font-family: sans-serif;font-size: 13px;vertical-align: middle;display: block;max-width: 600px;padding: 10px;width: 600px;margin: 0 auto!important;">
                <div class="content"
                    style="box-sizing: border-box;display: block;margin: 0 auto;max-width: 600px;padding: 10px;">
                    <table class="main"
                        style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;background: #fff;border-radius: 5px;">
                        <tr>
                            <td class="wrapper"
                                style="font-family: sans-serif;font-size: 13px;vertical-align: middle;box-sizing: border-box;padding: 5px 20px;">
                                <table border="0" cellpadding="0" cellspacing="0"
                                    style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                    <tr>
                                        <td style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                            <table border="0" cellpadding="0" cellspacing="0"
                                                style="border-bottom: 1px solid #cfeef7;padding-bottom: 10px;border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td valign="middle"
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <img src="http://v3-st-vcdn.anthill.vn/email/logo-ants.png"
                                                                width="124" height="54" alt=""
                                                                style="border: none;-ms-interpolation-mode: bicubic;max-width: 100%;">
                                                        </td>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="right" valign="middle"
                                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                                            <ul
                                                                                style="clear: both;list-style: none;margin: 0;padding: 0;">
                                                                                <li
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="https://www.facebook.com/ANTSCorporation"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-01.png"
                                                                                            alt=""></a></li>
                                                                                <li
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="https://plus.google.com/111160531271552806810"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-02.png"
                                                                                            alt=""></a></li>
                                                                                <li
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="http://twitter.com/antscorporation"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-03.png"
                                                                                            alt=""></a></li>
                                                                                <li
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="https://www.youtube.com/antscorporation"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-04.png"
                                                                                            alt=""></a></li>
                                                                                <li
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="https://www.linkedin.com/company/ants-jsc"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-05.png"
                                                                                            alt=""></a></li>
                                                                                <li class="end"
                                                                                    style="margin:0; display: inline-block;line-height: 1;">
                                                                                    <a href="http://www.slideshare.net/antscorp"
                                                                                        target="_blank"
                                                                                        style="display: block;padding: 0 2px 0 8px;"><img
                                                                                            src="http://v3-st-vcdn.anthill.vn/email/icon-06.png"
                                                                                            alt=""></a></li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td
                                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                                            <table width="0" border="0"
                                                                                style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="right"
                                                                                            valign="middle"
                                                                                            style="font-family: sans-serif;font-size: 14px;vertical-align: middle;">
                                                                                            <img src="http://v3-st-vcdn.anthill.vn/email/icon-mobile.png"
                                                                                                width="12" height="15"
                                                                                                alt=""
                                                                                                style="vertical-align: middle;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;"><span
                                                                                                style="font-size:11px; color:#858d99; padding-left:10px;">+
                                                                                                84 28 7309 9199 </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                            <table border="0" cellpadding="0" cellspacing="0"
                                                style="border-bottom: 1px solid #cfeef7;padding-top: 22px;padding-bottom: 22px;border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <p
                                                                style="padding-bottom: 10px;line-height: 1.4;color: #666;font-family: sans-serif;font-size: 13px;font-weight: normal;margin: 0;">
                                                                <center>New submission on form <strong>${params.formName}</strong></center>
                                                            </p> 
                                                                ${htmlFields} 
                                                            <p
                                                                style="padding-bottom: 10px;line-height: 1.4;color: #666;font-family: sans-serif;font-size: 13px;font-weight: normal;margin: 0;">
                                                                <center>
                                                                <a href="${params.url}">
                                                                <button class="btn-green" type="button"
                                                                        style="background-color: #9cce24;border: 0 none;border-bottom: 2px solid #90bf21;border-radius: 2px;color: #fff;cursor: pointer;font-weight: bold;padding: 0 10px;line-height: 30px;height: 30px;font-size: 12px">OPEN</button></a>
                                                                </center>
                                                            </p>
                                                            <p
                                                                style="padding-bottom: 30px;line-height: 1.4;color: #666;font-family: sans-serif;font-size: 13px;font-weight: normal;margin: 0;">
                                                                If the button above doesn't work, please open at <strong>${params.url}</strong></p>                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <p
                                                                style="padding-bottom: 15px;color: #666;font-family: sans-serif;font-size: 13px;font-weight: normal;margin: 0;">
                                                                Sincerely,<br>${params.network} Support Team</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <div
                                                                style="background-color:#0eabd8; border-radius:5px; display:block; padding:10px 20px 5px 0;">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                    style="border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                                                <img src="http://v3-st-vcdn.anthill.vn/email/img-light.png"
                                                                                    width="45" height="61" alt=""
                                                                                    style="display: block;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;">
                                                                            </td>
                                                                            <td align="right"
                                                                                style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                                                <p
                                                                                    style="color: #fff;font-size: 13px;font-weight: bold;padding-bottom: 3px;font-family: sans-serif;margin: 0;">
                                                                                    ANTS – Empowering the Digital Media,
                                                                                    Ad Exchange, Ad Server, Analytics
                                                                                </p>
                                                                                <p
                                                                                    style="color: #fff;font-size: 12px;font-weight: normal;font-family: sans-serif;margin: 0;">
                                                                                    Unleash the full economic potential
                                                                                    of #Digital Business!</p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <p
                                                                style="color: #999;font-size: 12px;padding-top: 10px;font-family: sans-serif;font-weight: normal;margin: 0;">
                                                                This message was sent from a notification-only email
                                                                address that does not accept incoming email. Please do
                                                                not reply to this message. If you have any questions,
                                                                please feel free to contact us through the <a href=""
                                                                    style="color: #0eabd8;text-decoration: none;">Help
                                                                    Center</a></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                            <table border="0" cellpadding="0" cellspacing="0"
                                                style="padding-top: 10px;padding-bottom: 5px;border-collapse: separate;mso-table-lspace: 0px;mso-table-rspace: 0px;width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <p
                                                                style="font-size: 12px;color: #666;font-family: sans-serif;font-weight: normal;margin: 0;">
                                                                © 2016 Ants</p>
                                                        </td>
                                                        <td align="right"
                                                            style="font-family: sans-serif;font-size: 13px;vertical-align: middle;">
                                                            <nav>
                                                                <ul
                                                                    style="clear: both;list-style: none;margin: 0;padding: 0;">
                                                                    <li
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;">Hone</a>
                                                                    </li>
                                                                    <li
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;">Ad
                                                                            Exchange</a></li>
                                                                    <li
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;">Ad
                                                                            Server</a></li>
                                                                    <li
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;">Insight
                                                                            Analytics</a></li>
                                                                    <li
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;">Blog</a>
                                                                    </li>
                                                                    <li class="end"
                                                                        style="display: inline-block;border-right: 1px solid #0eabd8;line-height: 1;border: 0 none;">
                                                                        <a href=""
                                                                            style="color: #0eabd8;text-decoration: none;font-size: 12px;display: block;padding: 0 8px;margin: 0;padding-right: 0;">Wiki</a>
                                                                    </li>
                                                                </ul>
                                                            </nav>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>`;
};

module.exports = getTemplate;


