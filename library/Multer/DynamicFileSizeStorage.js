// Model
const FieldModel = require("@library/Model/Field");

// Library
const FS = require("fs");
const Joi = require("@hapi/joi");
const Logger = require("@library/Logger")
const MD5 = require("md5");
const Moment = require("moment");
const Path = require('path');

// Utils
const {formatRequestParams, getObjectPropSafely} = require("@library/Utils");

// Constants
const constant = require('@config/common/constant');

const B = 1;
const KB = 1024 * B;
const MB = 1024 * KB;
const GB = 1024 * MB;

/**
 * Multer File object argument
 * @typedef {Object} MulterFileArg
 * @property {string} fieldname - Field name specified in the form
 * @property {string} originalname - Name of the file on the user's computer
 * @property {string} encoding - Encoding type of the file
 * @property {string} mimetype - Mime type of the file
 * @property {number} size - Size of the file in bytes
 * @property {string} destination - The folder to which the file has been saved
 * @property {string} filename - The name of the file within the destination
 * @property {string} path - The full path to the uploaded file
 */

/**
 * @callback getDestination
 * @param {Request} req - Request object
 * @param {MulterFileArg} file - Multer File argument
 * @param {function} cb - Callback function
 */

/**
 * DynamicFileSize Storage option
 * @typedef {Object} DFS_StorageOption
 * @property {number=} max_file_size - Maximum File Size in MB (Default 3 MB)
 * @property {getDestination=} destination - Get Destination callback
 */

/**
 * @param {DFS_StorageOption=} option - Option
 * @constructor Construct a Dynamic File Size Storage with option
 */
function DynamicFileSizeStorage(option) {
    this.maxFileSize = option && option.max_file_size;
    this.getDestination = (option && option.destination) ||
        ((req, file, cb) => {
            cb(null, constant.get('ROOT_PATH') +
                constant.get('STATIC_PATH') +
                '/upload/' +
                Moment().format("YYYY/MM/DD"));
        });
}

DynamicFileSizeStorage.prototype._handleFile = function _handleFile(req, file, cb) {
    const _this = this;

    this.getDestination(req, file, async (error, path) => {
        if (error) {
            return cb(error);
        }

        if (!file) {
            return cb(new Error("Upload file not found"));
        }

        const fileName = MD5(`${file.originalname}:${Moment()}`) + Path.extname(file.originalname);
        const fullPath = path + "/" + fileName;

        if (!FS.existsSync(path)) {
            try {
                FS.mkdirSync(path, {recursive: true});
            } catch (err) {
                Logger.error("Error when creating directory: ", err);

                throw err;
            }
        }

        let maxFileSize = _this.maxFileSize || 3 * MB;
        if (!_this.maxFileSize) {
            const params = formatRequestParams(req);

            //region Validate params
            // Create a Joi validator object
            // Validate rule:
            // * field_id is a number, and required
            // * network_id is a number, and required
            const validator = Joi.object({
                field_id: Joi.number().required(),
                network_id: Joi.number().required(),
            }).unknown();

            if (validator.validate(params).error) {
                return cb(new Error(getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Invalid request params"
                )));
            }
            //endregion

            params.id = params.field_id;
            params.networkId = params.network_id;

            const field = await FieldModel.getField(params) || []; // TODO ES

            if (!field && field.length) {
                return cb(new Error(`Field ${params.field_id} is not existed`));
            }

            if (+field[0].field_type_id !== +FieldModel.FIELD_TYPE.FILE
                && +field[0].field_type_id !== +FieldModel.FIELD_TYPE.IMAGE) {
                return cb(new Error(`Field ${params.field_id} is not a File or Image type`));
            }

            const {properties = null} = field[0];
            const {max_file_size = 3 * MB} = properties;
            maxFileSize = max_file_size;
        }

        const readStream = file.stream;

        let fileSize = 0;
        readStream.on("data", chunk => {
            fileSize += chunk.length;

            Logger.info(`Total chunk size: ${parseFloat(fileSize / MB).toFixed(2)} MB`);

            if (fileSize > maxFileSize * MB) {
                readStream.destroy(new Error("File too large"));
                readStream.emit("close");
            }
        })
        readStream.on("error", err => {
            Logger.error("Error at readable stream: ", err);

            return cb(err);
        });

        if (readStream) {
            const writeStream = FS.createWriteStream(fullPath);

            readStream.pipe(writeStream);

            writeStream.on("error", err => {
                Logger.error("Error at writable stream: ", err);

                return cb(error);
            });
            writeStream.on("finish", () => {
                Logger.info("Finish");

                return cb(null, {
                    destination: path,
                    filename: fileName,
                    path: fullPath,
                    size: writeStream.bytesWritten
                })
            });
        }
    });
}

DynamicFileSizeStorage.prototype._removeFile = function _removeFile(req, file, cb) {
    delete file.destination
    delete file.filename
    delete file.path

    FS.unlink(file.destination, cb);
}

module.exports = DynamicFileSizeStorage;