const DynamicFileSizeStorage = require("./DynamicFileSizeStorage")
const DynamicFileFilter = require("./DynamicFileFilter")

const PATH = "@library/Multer/Index.js";

module.exports = {
    DynamicFileSizeStorage,
    DynamicFileFilter
};


