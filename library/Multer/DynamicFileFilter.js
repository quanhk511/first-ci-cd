// Model
const FieldModel = require("@library/Model/Field");

// Library
const Joi = require("@hapi/joi");
const Path = require('path');

// Utils
const {formatRequestParams, getObjectPropSafely} = require("@library/Utils");

const DynamicFileFilter = async (req, file, cb) => {
    const params = formatRequestParams(req);

    //region Validate params
    // Create a Joi validator object
    // Validate rule:
    // * field_id is a number, and required
    // * network_id is a number, and required
    const validator = Joi.object({
        field_id: Joi.number().required(),
        network_id: Joi.number().required(),
    }).unknown();

    if (validator.validate(params).error) {
        return cb(new Error(getObjectPropSafely(
            () => validator.validate(params).error.details[0].message,
            "Invalid request params"
        )), false);
    }
    //endregion

    params.id = params.field_id;
    params.networkId = params.network_id;

    const field = await FieldModel.getField(params); // TODO ES

    if (!field) {
        return cb(new Error(`Field ${params.field_id} is not existed`), false);
    }

    const {properties} = field[0];
    const {file_types} = properties;

    if (!file || !file_types || !file_types.includes(Path.extname(file.originalname).slice(1))) {
        return cb(new Error("Invalid file type"), false);
    }

    return cb(null, true);
}

module.exports = DynamicFileFilter;