/* eslint-disable no-console */
// Libraries
const amqp = require('amqplib');
const path = require('path');
const util = require('util');
const moment = require('moment');

let connections = {};
let channels = {};
let intervalRestart = {
    intervalTime: 5000
};

class RabbitMQ {
    /**
     * RabbitMQ instance argument
     * @typedef {Object} RabbitMqArgs
     * @property {string} instanceName - Name of the Rabbit MQ instance
     * @property {string} [exchangeName] - Exchange name (optional)
     * @property {string} queueName - Queue name (shouldn't let the RabbitMQ implicit creates this by itself)
     * @property {string} [routingKey] - Routing Key mapping (optional)
     * @property {number} numberWorker - Number of messages this queue will hold at a time
     */
    /**
     * Construct a RabbitMQ instance
     *
     * @param {RabbitMqArgs} args - Argument to create a RabbitMQ instance
     *
     * @see {@link RabbitMqArgs}
     */
    constructor(args) {
        /** @type {string} */
        this.instanceName = args.instanceName

        /** @type {string} */
        this.exchangeName = args.exchangeName || "";

        /** @type {string} */
        this.queueName = args.queueName;

        /** @type {string} */
        this.routingKey = args.routingKey || "*";

        /** @type {number} */
        this.numberWorker = args.numberWorker;

        /** @type {Object} */
        this.config = getConfig(this.instanceName);

        if (this.config && this.config.prefix) {
            this.queueName = `${this.config.prefix}:job:${this.queueName}`;

            if (this.exchangeName) {
                this.exchangeName = `${this.config.prefix}:exchange:${this.exchangeName}`;
            }
        }
    }

    async startConsumer() {
        try {
            const connection = await getConnection(this.config, this.instanceName);

            if (!connection) {
                return;
            }

            connection.on('close', () => {
                intervalRestart[this.instanceName] = setInterval(() => {
                    return this.startConsumer();
                }, intervalRestart.intervalTime);
            });

            const channel = await getChannel(connection, this);

            if (!channel) {
                return;
            }

            channel.consume(this.queueName, async msg => {
                try {
                    const messages = JSON.parse(msg.content.toString());

                    // eslint-disable-next-line no-console
                    console.log('Received on consumer: ' + util.inspect(messages, {showHidden: false, depth: null}));

                    if (messages.className && messages.functionName) {
                        const task = require(`@worker/Task/${messages.className}`);

                        if (task && task[messages.functionName]) {
                            await task[messages.functionName](messages.msg);
                        }
                    }

                    channel.ack(msg);
                } catch (error) {
                    console.log('startConsumer | Error consume | Close channel');
                    console.log(error)

                    await channel.close();

                    return this.startConsumer();
                }
            }, {
                noAck: false
            });
        } catch (error) {
            console.log('Error startConsumer');
        }
    }

    async sendMessage(className, functionName, msg) {
        try {
            const connection = await getConnection(this.config, this.instanceName);

            const channel = await getChannel(connection, this);

            const startJobTime = moment(moment.now());

            const message = {
                ...msg,
                startJobTime
            };

            if (!message.href && global && global.originalUrl && global.hostname) {
                message.href = `${global.hostname}${global.originalUrl}`;
            }

            return await channel.sendToQueue(this.queueName, Buffer.from(JSON.stringify({
                className,
                functionName,
                msg: message
            })));
        } catch (error) {
            console.log('Error sendMessage: ', error);
        }
    }

    async publishMessage(className, functionName, msg) {
        try {
            const connection = await getConnection(this.config, this.instanceName);

            const channel = await getChannel(connection, this);

            const startJobTime = moment(moment.now());

            const message = {
                ...msg,
                startJobTime
            };

            if (!message.href && global && global.originalUrl && global.hostname) {
                message.href = `${global.hostname}${global.originalUrl}`;
            }

            return await channel.publish(this.exchangeName, this.routingKey, Buffer.from(JSON.stringify({
                className,
                functionName,
                msg: message
            })));
        } catch (error) {
            console.log("Error publishMessage: ", error);
        }
    }
}

const getConfig = (instanceName) => {
    let config;

    try {
        const Config = require('@library/Config');

        config = Config.get('worker');

        if (instanceName && config && config.rabbitMQ && config.rabbitMQ[instanceName]) {
            config = config.rabbitMQ[instanceName];
        }
    } catch (error) {
        console.log('Error getConfig', error);
    }

    return config;
};

const getConnection = async (config, instanceName) => {
    let connection;

    try {
        connection = connections[instanceName];

        if (!connection) {
            console.log('Create new connection');

            const uri = `amqp://${config.userName}:${config.password}@${config.host}:${config.port}/?heartbeat=30`;

            connection = await amqp.connect(uri).catch(() => {
            });

            if (!connection) {
                console.log('Cannot get connection');
                return;
            }

            clearInterval(intervalRestart[instanceName]);

            connection.on('close', () => {
                console.log('getConnection | Listen "close"');

                delete connections[instanceName];
            });

            connections[instanceName] = connection;
        }
    } catch (error) {
        console.log('Error getConnection', error);
    }

    return connection;
};

/**
 *
 * @param connection
 * @param {RabbitMQ} rabbitMq
 * @return {Promise<*|T>}
 */
const getChannel = async (connection, rabbitMq) => {
    let channel;

    if (!connection) {
        console.log('getChannel | No connection');
        return;
    }

    try {
        channel = channels[rabbitMq.instanceName];

        if (!channel) {
            console.log('Create new channel');

            channel = await connection.createConfirmChannel();

            if (!channel) {
                console.log('Cannot get channel');
                return;
            }

            const queue = await channel.assertQueue(rabbitMq.queueName, {
                exclusive: false,
                durable: true,
                autoDelete: false
            });

            if (!queue) {
                console.log("Cannot get queue: ", rabbitMq.queueName)

                return ;
            }

            if (rabbitMq.exchangeName) {
                const exchange = await channel.assertExchange(rabbitMq.exchangeName, "topic", {
                    durable: true,
                    autoDelete: false,
                });

                if (!exchange) {
                    console.log("Cannot get exchange: ", rabbitMq.exchangeName);

                    return ;
                }

                if (rabbitMq.routingKey) {
                    await channel.bindQueue(queue.queue, exchange.exchange, rabbitMq.routingKey);
                }
            }

            await channel.prefetch(rabbitMq.numberWorker);

            channel.on('close', () => {
                console.log('getChannel | Listen "close"');

                delete channels[rabbitMq.instanceName];
            });

            channels[rabbitMq.instanceName] = channel;
        }
    } catch (error) {
        console.log('Error getChannel', error);
    }

    return channel;
};

module.exports = RabbitMQ;
