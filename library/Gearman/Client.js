const Gearman = require('gearman');
const Config = require('@library/Config');
const {handleError} = require('@library/Utils');

const PATH = '@library/Gearman/Client.js';
const CLOSE_TIMEOUT = 60000;

function debounce (func, wait, immediate) {
    let timeout;
    return function () {
        let context = this;
        let args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

class Client {
    constructor () {
        this.count = 0;
        this.client = null;
        this.isForceClose = false;
        this.forceCloseConnection = debounce(this.forceCloseConnection, CLOSE_TIMEOUT);
    }

    init () {
        try {
            if (!this.client || this.count === 0) {
                console.log('\x1b[46mINIT_CONNECTION: %s\x1b[0m', this.count);
                let config = Config.get('worker');

                const {host, port} = config.gearman['info_antalyser'];
                if (host && port) {
                    this.client = new Gearman(host, port);
                    this.client.connect(() => {
                        console.log('\x1b[32mCLIENT CONNECTED\x1b[0m');
                    });

                    this.client.on('WORK_COMPLETE', job => {
                        this.count--;

                        console.log('\x1b[44mJOB COMPLETE, result: %s\x1b[0m', job.payload.toString());
                    });
                }
            }

            return this;
        } catch (e) {
            handleError(e, {
                action: 'checkDataSourceName -> closeConnection',
                path: '@library/Gearman/Client.js'
            });
            return null;
        }
    }

    submitJob (payload, jobName) {
        try {
            if (this.client && jobName && payload) {
                this.count++;
                this.client.submitJob(jobName, payload);
            }
        } catch (error) {
            handleError(error, {
                action: new Error().stack,
                path: PATH,
                args: {
                    payload,
                    jobName
                }
            });
        }
    }
    close () {
        const CHECK_CLOSE_INTERVAL = 500;

        if (this.client) {
            const interval = setInterval(() => {
                if (this.count === 0) {
                    this.client.close();
                    clearInterval(interval);
                    console.log('\x1b[33mCLIENT CONNECTION CLOSED\x1b[0m');
                    this.isForceClose = false;
                }
                this.forceCloseConnection();
            }, CHECK_CLOSE_INTERVAL);
        }
    }

    forceCloseConnection (interval) {
        if (this.isForceClose) {
            this.isForceClose = false;
            this.client.close();
            clearInterval(interval);
            console.log('\x1b[33mCLIENT CONNECTION CLOSED DUE TO TIMEOUT\x1b[0m');
        }
    }
}

const connect = () => {
    try {
        let client = null;

        let config = Config.get('worker');

        if (client) {
            return client;
        }

        const {host, port} = config.gearman['info_antalyser'];
        if (host && port) {
            client = new Gearman(host, port);

            if (client) {
                client.connect(() => {
                    // eslint-disable-next-line no-console
                    console.log('\x1b[32mCLIENT CONNECTED\x1b[0m');
                });

                client.on('WORK_COMPLETE', job => {
                    // eslint-disable-next-line no-console
                    console.log('\x1b[44mJOB COMPLETE, result: %s\x1b[0m', job.payload.toString());
                });
                return client;
            }
        }
        return null;
    } catch (e) {
        // eslint-disable-next-line no-console
        handleError(e, {
            action: 'connect',
            path: PATH
        });
    }
};

const submitJob = (payload, jobName) => {
    try {
        const client = connect();

        if (client && payload && jobName) {
            client.submitJob(jobName, payload);
        }
    } catch (error) {
        // eslint-disable-next-line no-console
        handleError(error, {
            action: new Error().stack,
            path: PATH,
            args: {
                payload,
                jobName
            }
        });
    }
};

module.exports = {
    submitJob
};

module.exports = new Client();

