const Config = require('@library/Config');
const oracledb = require('oracledb');

// Assets
const {handleError, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/Database';

const getInstance = async instance => {
    try {
        let connection = null;
        let config = Config.get('database');

        if (instance && config && config.oracle && config.oracle[instance]) {
            let instanceInfo = config.oracle[instance];

            if (instanceInfo.username && instanceInfo.password && instanceInfo.connection_string) {
                const user = instanceInfo.username;
                const password = instanceInfo.password;
                const connectString = instanceInfo.connection_string;

                connection = await oracledb.getConnection({
                    user,
                    password,
                    connectString
                });

                if (getObjectPropSafely(() => global.debugMode === true)) {
                    // Redefine connection.execute to print SP execution params
                    connection.execute = applyDebugMode(connection.execute);
                }

                return connection;
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });

        return null;
    }
};

const getInstanceFromPool = async instance => {
    try {
        let pool = null;
        let connection = null;

        // Get pool by aliasName
        try {
            pool = oracledb.getPool(instance);
        } catch (err) {
            pool = await createInstance(instance);
        }

        if (pool && pool.getConnection) {
            // Get connection from pool
            connection = await pool.getConnection();

            if (getObjectPropSafely(() => global.debugMode === true)) {
                // Redefine connection.execute to print SP execution params
                connection.execute = applyDebugMode(connection.execute);
            }
        }

        return connection;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });

        return null;
    }
};

const getCustomConnection = async instanceInfo => {
    try {
        let connection = null;

        if (instanceInfo.host && instanceInfo.port && instanceInfo.serviceName && instanceInfo.user && instanceInfo.password) {
            const {host, port, serviceName, user, password} = instanceInfo;

            const connectString = `//${host}:${port}/${serviceName}`;

            const info = {
                user,
                password,
                connectString
            };

            connection = await oracledb.getConnection(info);

            if (getObjectPropSafely(() => global.params.debug === true)) {
                // Redefine connection.execute to print SP execution params
                connection.execute = applyDebugMode(connection.execute);
            }

            return connection;
        }
    } catch (error) {
        console.error('Cannot connect to oracle db', error);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                instanceInfo
            }
        });
    }
};

const createInstance = async instance => {
    try {
        let pool = null;

        let config = Config.get('database');

        if (instance && config && config.oracle && config.oracle[instance]) {
            let instanceInfo = config.oracle[instance];

            if (instanceInfo.username && instanceInfo.password && instanceInfo.connection_string) {
                const user = instanceInfo.username;
                const password = instanceInfo.password;
                const connectString = instanceInfo.connection_string;
                const poolMin = instanceInfo.pool_min || 1; /* Minimum connection of pool */
                const poolMax = instanceInfo.pool_max || 10; /* Maximum connection of pool */
                const poolIncrement = instanceInfo.pool_increment || 1;

                pool = await oracledb.createPool({
                    user,
                    password,
                    connectString,
                    poolMin,
                    poolMax,
                    poolIncrement,
                    _enableStats: true,
                    poolAlias: instance /* Alias name to use later with pool.getConnection(poolAlias) */
                });
            }
        }

        return pool;
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {
                instance
            }
        });
    }
};

const init = () => {
    try {
        let config = Config.get('database');

        // Init oracle databases
        if (config.oracle && Object.keys(config.oracle).length) {
            Object.entries(config.oracle).forEach(([instance, instanceInfo]) => {
                if (!instanceInfo.init_on_demand) {
                    createInstance(instance);
                }
            });
        }

        // Close pool
        process
            .once('SIGTERM', closePoolAndExit)
            .once('SIGINT', closePoolAndExit);
    } catch (err) {
        handleError(err, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const closePoolAndExit = async () => {
    try {
        let config = Config.get('database');

        // Init oracle databases
        if (config.oracle && Object.keys(config.oracle).length) {
            for (const instance in config.oracle) {
                // Get the pool from the pool cache and close it when no
                // connections are in use, or force it closed after 10 seconds
                await oracledb.getPool(instance).close(10);
            }
        }
    } catch (err) {
        handleError(err, {
            path: PATH,
            action: new Error().stack,
        });
    }
};

const applyDebugMode = (func, logFunc = console.log) => {
    return (function () {
        const _func = func;

        return function () {
            logFunc(global.buildSPLog(...arguments));
            return _func.apply(this, arguments);
        };
    })();
};

// Avoid two requests create a pool in the same time
init();

module.exports = {
    getInstanceDirectly: getInstance,
    getInstance: getInstanceFromPool,
    getCustomConnection
};
