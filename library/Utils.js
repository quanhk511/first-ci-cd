const EventEmitter = require('events').EventEmitter;
const fs = require("fs");
const jwt = require('jsonwebtoken');
const md5 = require("md5");
const monitor = require('@antscorp/monitor-nodejs');
const moment = require('moment');
const os = require('os');
const path = require("path");
const {promisify, inspect} = require('util');

// Utils
const Logger = require("@library/Logger");
const constant = require('@config/common/constant');

const PATH = '@library/Utils.js';

const writeLog = (fileName, params) => {
    try {
        if (fileName && params) {
            const fs = require('fs');
            const moment = require('moment');
            const day = moment().format('DD');
            const month = moment().format('MM');
            const year = moment().format('YYYY');

            let path = process.env.PWD + '/logs/' + year;

            // Year
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path, {recursive: true});
            }

            // Month
            path = path + '/' + month;
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path, {recursive: true});
            }

            // Day
            path = path + '/' + day;
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path, {recursive: true});
            }

            fileName = path + '/' + fileName.replace(' ', '_') + '_' + day + '_' + month + '_' + year + '.txt';

            fs.appendFile(fileName, JSON.stringify({
                params: params,
                time: moment().format('HH:mm:ss')
            }) + os.EOL,
                err => {
                    if (err) throw err;
                    // eslint-disable-next-line no-console
                    console.log('Log was appended to file!');
                });
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                fileName,
                params
            }
        });
    }
};

const checkJSON = _str => {
    let result = true;

    try {
        JSON.parse(_str);
    } catch (e) {
        result = false;
    }

    return result;
};

const random = number => {
    try {
        let text = '';
        let possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < number; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });
    }
};

const handleError = (error, payload) => {
    // Slack Channel
    const SlackChannel = require("@library/Slack");
    const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});

    // Utils
    const util = require("util");
    const Logger = require("@library/Logger")

    payload.action = payload.action.split("\n")[1].split(" ")[5].split(".")[1];

    Logger.error("Error Message: ", error.message);
    Logger.error("Payload: ", util.inspect(payload, {
        breakLength: Infinity,
        colors: true,
        depth: Infinity,
        showHidden: true
    }));

    return payload.path.indexOf("DAO") >= 0
        ? slack.sendMessage(
            `${moment().format("DD-MM-YYYY HH:mm:ss")} Error in ${payload.path} -> ${payload.action}
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(
                payload.args,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                }
            )}`)
        : Promise.resolve();
    try {
        if (process.env.APPLICATION_ENV === 'development') {
            console.error(error);
        }

        // Send error log
        monitor.handleError(error, {
            traceId: '',
            ...payload
        });
    } catch (e) {
        // Error
    }
};

/**
 * Look up property @param key of provided object.
 * If @param key is string, it will look for first value found or look down to @param obj's sub objects.
 * If @param key is array, it will return an array of values
 */

const snakeToCamel = str =>
    str.replace(/([-_][a-z])/g, group =>
        group
            .toUpperCase()
            .replace('-', '')
            .replace('_', '')
    );

function camelToSnake(str) {
    return str.split('').map((letter, i) => {
        if (i === 0) {
            return letter.toLowerCase();
        } else if (letter === letter.toUpperCase()) {
            return `_${letter.toLowerCase()}`;
        }
        return letter;
    }).join('');
}

/**
 * Clone an object/array with camelized keys
 * @param {*} obj
 * @param {Boolean} keepOriginalKey
 * @param {Boolean} recursive
 */
const camelKeysClone = (obj, keepOriginalKey = true, recursive = false) => {
    if (typeof obj !== 'object' || obj === null || obj === undefined) {
        return obj;
    }

    const result = {};

    for (const key in obj) {
        keepOriginalKey && (result[key] = obj[key]);

        if (recursive) {
            if (Array.isArray(obj[key])) {
                result[snakeToCamel(key)] = obj[key].map(sub => camelKeysClone(sub, keepOriginalKey, recursive));
            } else {
                result[snakeToCamel(key)] = camelKeysClone(obj[key], keepOriginalKey, recursive);
            }
        } else {
            result[snakeToCamel(key)] = obj[key];
        }
    }

    return result;
};

const convertNumberToColumnLetter = number => {
    try {
        let result = '';
        let columnLetter = '';
        let codeFirstLetter = 1;
        let codeLastLetter = 26;

        for (let index = number; (index -= codeFirstLetter) >= 0; codeFirstLetter = codeLastLetter, codeLastLetter *= 26) {
            columnLetter = String.fromCharCode(
                parseInt((index % codeLastLetter) / codeFirstLetter) + 65
            );
            result = columnLetter + result;
        }

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });
    }
};

const convertColumnLetterToNumber = letter => {
    try {
        let column = 0;
        const length = letter.length;

        for (let index = 0; index < length; index++) {
            column +=
                (letter.charCodeAt(index) - 64) * Math.pow(26, length - index - 1);
        }

        return column;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });
    }
};

const replaceDoubleToSingleQuote = message => {
    return message.replace(/"/g, "'");
};

const removeAccent = str => {
    str = str.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a');
    str = str.replace(/[èéẹẻẽêềếệểễ]/g, 'e');
    str = str.replace(/[ìíịỉĩ]/g, 'i');
    str = str.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o');
    str = str.replace(/[ùúụủũưừứựửữ]/g, 'u');
    str = str.replace(/[ỳýỵỷỹ]/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/[ÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴ]/g, 'A');
    str = str.replace(/[ÈÉẸẺẼÊỀẾỆỂỄ]/g, 'E');
    str = str.replace(/[ÌÍỊỈĨ]/g, 'I');
    str = str.replace(/[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]/g, 'O');
    str = str.replace(/[ÙÚỤỦŨƯỪỨỰỬỮ]/g, 'U');
    str = str.replace(/[ỲÝỴỶỸ]/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    return str;
};

const removeSpecialCharacter = str => {
    str = str.replace(/[^a-zA-Z0-9]/g, ' ');
    str = removeMultiWhiteSpace(str);
    return str;
};

function replaceNewLineCharacterWithSpace(str) {
    // eslint-disable-next-line no-control-regex
    return str.replace(/[\r\n\x0B\x0C\u0085\u2028\u2029]+/g, ' ').replace(/'/g, '\'\'');
}

const removeMultiWhiteSpace = str => {
    return str.replace(/[ ]+/g, ' ');
};

const convertValueToArray = value => {
    if (Array.isArray(value)) {
        return value;
    }

    let newValue = [];

    if (typeof value === 'number') {
        newValue = [value];
    } else if (typeof value === 'string' && !isNaN(+value)) {
        newValue = [+value];
    }

    return newValue;
};

const promiseObject = async (objPromises = {}) => {
    try {
        if (Object.keys(objPromises).length) {
            const formatPromiseToKeyValueForm = Object.entries(
                objPromises
            ).map(([key, promisePhrase]) =>
                promisePhrase.then(response => ({key, response}))
            );

            let executePromises = await Promise.all(formatPromiseToKeyValueForm);

            if (executePromises) {
                return (
                    executePromises.reduce((result, data) => {
                        const {key = '', response = null} = data;

                        if (key && response) {
                            result = {
                                ...result,
                                [key]: response
                            };
                        }

                        return result;
                    }, {}) || {}
                );
            }
        }

        return {};
    } catch (error) {
        handleError(error, {
            action: new Error().stack,
            path: PATH,
            args: {objPromises}
        });
    }
};

const runJob = (instant, queueName, className, functionName, msg) => {
    try {
        if (instant && queueName && className && functionName && msg) {
            const RabbitMQ = require('@library/RabbitMQ');

            RabbitMQ.publishMessageToQueue(instant, queueName, className, functionName, msg).catch((error) => {
                // Run job error
                handleError(error, {
                    action: new Error().stack,
                    path: PATH,
                    args: {
                        instant, queueName, className, functionName, msg
                    }
                });
            });
        }
    } catch (e) {
        // Error
        handleError(e, {
            action: new Error().stack,
            path: PATH,
            args: {
                instant, queueName, className, functionName, msg
            }
        });
    }
};

const runJobGearman = (payload, jobName) => {
    try {
        const GearmanClient = require('@library/Gearman/Client');
        GearmanClient.submitJob(payload, jobName);
    } catch (e) {
        handleError(e, {
            action: new Error().stack,
            path: PATH,
            args: {
                payload,
                jobName
            }
        });
    }
};

/**
 * Encode a value using JWT
 * @param {any} value Value to be encoded
 * @param {string} algorithm Algorithm name
 */
const jwtEncode = (value, algorithm = 'HS256') => {
    if (!value) {
        return null;
    }

    return jwt.sign(value, constant.get('JWT_SECRET_KEY'), {
        algorithm
    });
};

/**
 * Decode a token using JWT
 * @param {string} encodedToken Encoded token using the same algorithm
 * @param {string} algorithm Algorithm name
 * @param {boolean} ignoreExpiration
 */
const jwtDecode = (encodedToken, algorithm = 'HS256', ignoreExpiration = true) => {
    if (!encodedToken) {
        return null;
    }

    return jwt.verify(encodedToken, constant.get('JWT_SECRET_KEY'), {
        algorithm,
        ignoreExpiration
    });
};

const formatRequestParams = (req = {}) => {
    const {query: reqQuery, params: reqParams, body: reqBody} = req;
    const params = {
        ...reqParams,
        ...reqQuery,
        ...reqBody
    };

    if (reqQuery['_network_id']) {
        params.networkId = reqQuery['_network_id'];

        delete params['_network_id'];
    }

    if (reqQuery['_user_id']) {
        params.userId = reqQuery['_user_id'];

        delete params['_user_id'];
    }

    if (reqQuery['_account_id']) {
        params.accountId = reqQuery['_account_id'];

        delete params['_account_id'];
    }

    if (reqQuery['_manager_id']) {
        params.managerId = reqQuery['_manager_id'];

        delete params['_manager_id'];
    }

    if (reqQuery['_token']) {
        params.token = reqQuery['_token'];

        delete params['_token'];
    }

    return params;
};

function encodeUtf8(str) {
    return unescape(encodeURIComponent(str));
}

function decodeUtf8(str) {
    return decodeURIComponent(escape(str));
}

const getObjectPropSafely = (fn, defaultValue = '') => {
    try {
        return fn();
    } catch (e) {
        return defaultValue;
    }
};

const removeDir = path => {
    const fs = require('fs');

    if (fs.existsSync(path)) {
        const files = fs.readdirSync(path);

        if (files.length > 0) {
            files.forEach(filename => {
                if (fs.statSync(path + '/' + filename).isDirectory()) {
                    removeDir(path + '/' + filename);
                } else {
                    fs.unlinkSync(path + '/' + filename);
                }
            });
            fs.rmdirSync(path);
        } else {
            fs.rmdirSync(path);
        }
    }
};

const redisGetAsync = async (key) => {
    try {
        if (key) {
            const Redis = require('@library/Redis');

            const redis = await Redis.getInstances('caching');

            const get = promisify(redis.get).bind(redis);

            return await get(key);
        }
        return new Error('Not found key');
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                key
            }
        });
    }
};

const redisSetAsync = async (key, value, expireDuration) => {
    try {
        if (key && value) {
            const Redis = require('@library/Redis');

            const redis = await Redis.getInstances('caching');

            const set = promisify(redis.set).bind(redis);

            const data = await set(key, value);

            if (expireDuration && typeof expireDuration === 'number') {
                let duration = expireDuration < 1000 ? expireDuration * 1000 : expireDuration;
                redis.expireat(key, parseInt((+new Date()) / 1000 + duration));
            }
            return data;
        }

        return new Error('Not found key or value');
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                key
            }
        });
    }
};

const debugEvent = new EventEmitter();
let debugData = [];

const sendDebugEvent = async (datum) => {
    try {
        const ORACLE = 'oracle';
        const ELASTIC = 'elastic-search';
        const REDIS = 'redis';
        let result = {};
        const dataSource = [ORACLE, ELASTIC, REDIS];
        datum.forEach((data, index) => {
            if (dataSource.includes(data.source)) {
                result[data.source] = {...result[data.source]};
                result[data.source][data.type] = {...data, order: index};
            }
        });

        // expire after 1 day
        redisSetAsync(`debug:id:${global.currentTime}`, JSON.stringify(result), 86400);
    } catch (e) {
        handleError(e, {
            path: 'library/Redis.js',
            action: new Error().stack,
        });
    }
};

let timeoutDebugEvent;

debugEvent.on('saveDebugInfo', data => {
    if (global.debugMode) {
        if (timeoutDebugEvent) {
            clearTimeout(timeoutDebugEvent);
        }

        if (debugData.length > 0 && debugData[debugData.length - 1].currentTime !== data.currentTime) {
            const result = [...debugData];
            sendDebugEvent(result);
            debugData.length = 0;
        }
        debugData.push(data);

        if (debugData.length > 0) {
            timeoutDebugEvent = setTimeout(() => {
                const result = [...debugData];
                sendDebugEvent(result);
                debugData.length = 0;
            }, 1000);
        }
    }
});

const logData = (source) => (fn) => {
    return async (...args) => {
        try {
            debugEvent.emit('saveDebugInfo', {currentTime: global.currentTime, data: args, source, type: 'request'});
            const data = await fn(...args);
            debugEvent.emit('saveDebugInfo', {currentTime: global.currentTime, data, source, type: 'response'});

            return data;
        } catch (e) {
            throw new Error(e);
        }
    };
};

/**
 * Reduce an array to dictionary-like object
 * @param {string} key: Key to reduce
 * @param {array} arr: Array to reduce
 * @returns {Object} dictionary
 * @example
 * arrayToObject([
 *    {
 *       id: 1,
 *       label: 'Obj1'
 *    }
 * ]);
 * // Return
 * {
 *   1: {
 *      id: 1,
 *      label: 'Obj1'
 *   }
 * }
 */
const arrayToObject = (arr, key = 'id') => (arr || []).reduce((obj, cur) => ({
        ...obj,
        ...(cur && cur[key] && {[cur[key]]: cur})
    }), {}
);

/**
 * Clone Object.fromEntries in nodejs
 * @param {array} entries - array of [key, value]
 */
const fromEntries = (entries) => {
    return entries.reduce((obj, [key, val]) => ({...obj, [key]: val}), {});
};

/**
 * Reduce an array to an object contains keys with calculated values from provided function
 * @param {array} arr
 * @param {array} keys
 * @param {function} fn function to do when reduce
 * @param {any} initVal init value
 * @example
 * aggregateByKeys([{id: 1}, {id: 2}], ['id', 'name']) // {id: [1, 2], name: [undefined, undefined]}
 */
const aggregateByKeys = (arr, keys, fn = (a, b) => [...a, b], initVal = []) => {
    try {
        return fromEntries(keys.map(key =>
            [key, arr.reduce((res, cur) => fn(res, cur[key]), initVal)]
        ));
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {arr, keys}
        });
    }
};

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const uuidv4 = () => {
    try {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0;
            var v = c === 'x' ? r : (r & 0x3 | 0x8);

            return v.toString(16);
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });
    }
};

const recursiveFormat = el => {
    try {
        if (![null, undefined].includes((typeof el))) {
            if (Array.isArray(el)) {
                return el.map(item => recursiveFormat(item));
            } else if (typeof el === 'string') {
                if (isNaN(+el)) {
                    // String
                    if (checkJSON(el)) {
                        const parseEl = JSON.parse(el);

                        return Object.keys(parseEl).reduce((initObj, key) => {
                            initObj[snakeToCamel(key)] = recursiveFormat(parseEl[key]);

                            return initObj;
                        }, {});
                    } else {
                        return el;
                    }
                } else if (el.length) {
                    // Number
                    return +el;
                } else {
                    // Empty string
                    return el;
                }
            } else if (typeof el === 'number') {
                return el;
            } else if (el && Object.keys(el).length) {
                return Object.keys(el).reduce((initObj, key) => {
                    initObj[snakeToCamel(key)] = recursiveFormat(el[key]);

                    return initObj;
                }, {});
            }
        }

        return el;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {...el}
        });
    }
};

const formatFilter = (filters, allowedFields) => {
    if (!filters || !checkJSON(filters) || !(allowedFields && Object.keys(allowedFields).length)) {
        return [];
    }

    allowedFields = Object.keys(allowedFields);

    filters = JSON.parse(filters);

    filters = filters.map(filterElement => {
        filterElement = Object.keys(filterElement).reduce((result, metric) => {
            const formattedMetric = metric.toString().toLowerCase();

            if (!allowedFields.some(fieldName => fieldName === formattedMetric)) {
                return;
            }

            result.name = formattedMetric;

            Object.entries(filterElement[metric]).map(([operator, value]) => {
                result.operator = operator;

                switch (operator) {
                    case 'does_not_contain':
                        if (typeof value === 'string') {
                            result.operator = 'doesNotContain';
                            result.value = value;
                        }
                        break;
                    case 'start_with':
                        if (typeof value === 'string') {
                            result.operator = 'startWith';
                            result.value = value;
                        }
                        break;
                    case 'matches_any':
                        if (typeof value === 'string') {
                            value = value.split(',');

                            if (value.length) {
                                result.operator = 'matchesAny';
                                result.value = value;
                            }
                        }
                        break;
                    case 'contain':
                    case 'is':
                        if (typeof value === 'string') {
                            result.operator = operator;
                            result.value = value;
                        }
                        break;
                    case 'on':
                    case 'before':
                    case 'after':
                        const moment = require('moment');

                        if (typeof value === 'string' && moment(value, 'DD/MM/YYYY').isValid()) {
                            result.operator = operator;
                            result.value = value;
                        }

                        break;
                }
            });

            return result;
        }, {});

        return filterElement;
    });

    return filters.filter(Boolean);
};

/**
 * Capitalize a string
 *
 * @param str String to be capitalized
 * @return {string} Capitalized string
 * @example toCapital('abc') => 'Abc'
 */
const toCapital = str => {
    let res = str.toLowerCase()

    return res.charAt(0).toUpperCase() + res.slice(1);
};

/**
 * Prepare file before write content to it
 *
 * @param path Directory's path
 * @param fileName Filename (without extension)
 * @param option Other options.
 *  Current available options:
 *  -  dateTimeSuffix: Enable date-time suffix on filename
 *
 * @return {Promise<{fullPath: string, fileName: string}>}
 */
const prepareFile = async (path, fileName, option) => {
    let fullPath = `${path}${fileName}`;
    try {
        // Date and time as suffix
        let dateTimeSuffix = false;
        // Some configs
        if (typeof option === 'object') {
            ({dateTimeSuffix} = option);
        }
        if (path) {
            // Check path
            if (!fs.existsSync(path)) {
                try {
                    await fs.promises.mkdir(path, {
                        recursive: true
                    });
                } catch (err) {
                    handleError(err, {
                        action: new Error().stack,
                        path: PATH,
                        args: {
                            path
                        }
                    });
                }
            }
            const lstat = await fs.promises.lstat(path);
            // If path not a dir, empty it
            if (!lstat.isDirectory()) {
                path = '';
            }
        }
        const now = moment();
        const date = now.format('DD_MM_YYYY');
        const time = now.format('HH_mm');
        if (dateTimeSuffix) {
            fileName = `${fileName}_${date}_${time}.xlsx`;
        }
        fullPath = `${path}${fileName}`;
        if (fs.existsSync(fullPath)) {
            // Remove file
            fs.unlinkSync(fullPath);
        }
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {
                path, fileName, option
            }
        });
    }
    return {fullPath, fileName};
};

/**
 * Option object of {@link moveFile} function
 * @typedef {Object} MoveFileOption
 * @property {boolean} shouldHashedFileName - Should the file name be hashed
 */
/**
 * Move a file to another path
 *
 * @param srcFilePath {string} - Full path of the source file
 * @param destPath {string} - Destination directory path
 * @param option {MoveFileOption=} - Optional option
 *
 * @return {string} The destination path after file are written
 *
 * @see {@link MoveFileOption}
 */
const moveFile = async (srcFilePath, destPath, option) => {
    Logger.debug("Params: ", inspect({srcFilePath, destPath, option}, {
        breakLength: Infinity,
        colors: true,
        depth: Infinity,
        showHidden: true,
    }));

    let destFilePath = destPath;

    // Create sub-directory based on file extension
    const fileExt = path.extname(srcFilePath).toLowerCase();
    switch (fileExt) {
        case ".docx":
        case ".doc":
        case ".pdf":
        case ".txt":
            destFilePath += "/documents";
            break;
        case ".xlsx":
        case ".xls":
        case ".csv":
            destFilePath += "/worksheets";
            break;
        case ".jpg":
        case ".gif":
        case ".png":
            destFilePath += "/images";
            break;
        default:
            destFilePath += "/etc";
            break;
    }

    // Create sub-directory based on moved time (i.e. YYYY/MM/DD)
    destFilePath += "/" + moment().format("YYYY/MM/DD");

    return new Promise((resolve, reject) => {
        // Create destination directory
        try {
            if (!fs.existsSync(destFilePath)) {
                // Make a directory with permission to read/write and execute
                fs.mkdirSync(destFilePath, {mode: 0o775, recursive: true});
            }
        } catch (err) {
            console.log("Error when creating directory: ", err.message);

            return reject(err);
        }

        if (!fs.existsSync(srcFilePath)) {
            return reject(new Error("File not existed"));
        }

        // Hashing file name
        let fileName = path.basename(srcFilePath, fileExt);
        if (option && option.shouldHashedFileName) {
            fileName = md5(fileName + moment());
        }

        // Create final path with directory and file name
        destFilePath += "/" + fileName + fileExt;

        const readStream = fs.createReadStream(srcFilePath);

        readStream.on("error", err => {
            console.log("Error when reading stream: ", err);

            return reject(err);
        });

        if (readStream) {
            const writeStream = fs.createWriteStream(destFilePath);

            readStream.pipe(writeStream);

            writeStream.on("error", err => {
                console.log("Error when writing stream: ", err);

                return reject(err);
            });
            writeStream.on("finish", () => {
                console.log("Finish");

                return resolve(destFilePath);
            })
        }
    });

}

/**
 * RabbitMQ instance argument
 * @typedef {Object} RabbitMqArgs
 * @property {string} instanceName - Name of the Rabbit MQ instance
 * @property {string=} exchangeName - Exchange name (optional)
 * @property {string} queueName - Queue name (shouldn't let the RabbitMQ implicit creates this by itself)
 * @property {string=} routingKey - Routing Key mapping (optional)
 * @property {number} numberWorker - Number of messages this queue will hold at a time
 */
/**
 * Distribute a message to RabbitMQ service.
 *
 * If the RabbitMQ instance has exchange name and routing key, the message is distributed by using publish-subscribe
 * approach.
 * If the RabbitMQ instance has queue name only, the message is distributed directly.
 * Otherwise, throw IllegalArgument Error instead.
 *
 * @param {RabbitMqArgs} rabbitMqArgs - Argument to get a RabbitMQ instance
 * @param {string} className - Classname of the Task to be distributed
 * @param {string} functionName - Function name belongs to the ``className``
 * @param {Object} msg - Function arguments of ``functionName``
 *
 * @throws IllegalArgument
 */
const distributeRabbitMqMessage = async (rabbitMqArgs, className, functionName, msg) => {
    const RabbitMQ = require("@library/RabbitMQ/Index");
    const rabbitMqInstance = new RabbitMQ(rabbitMqArgs);

    if (rabbitMqArgs.exchangeName && rabbitMqArgs.routingKey) {
        return await rabbitMqInstance.publishMessage(className, functionName, msg);
    }

    if (rabbitMqArgs.queueName) {
        return await rabbitMqInstance.sendMessage(className, functionName, msg);
    }

    throw new Error("Illegal RabbitMQ Argument");
}

const distributeSlackMessage = async (channel, message) => {
    if (!channel || typeof channel !== "string") {
        return;
    }

    const Slack = require("@library/Slack");
    const slack = new Slack();
}

/**
 * Return a random string from [A-Z], [a-z] and [0-9]
 *
 * @param {number} length - Number of randomized characters
 * @return {string} - Randomized string with the ``length`` of characters
 */
const getRandomString = length => {
    const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}

/**
 * Comparing two objects to check whether their properties are equals to each other.
 * If some properties are object reference, deep comparing them.
 *
 * @param o1 {Object} - The first object to be compared
 * @param o2 {Object} - The other object
 * @return {boolean} - True if these two objects have the exactly same properties, including children object's properties
 */
const compareObject = (o1, o2) => {
    Logger.debug("Params: ", inspect({o1, o2}, {
        breakLength: Infinity,
        colors: true,
        depth: Infinity,
        showHidden: true,
    }));

    // If both o1 and o2 are null or undefined and exactly the same
    if (o1 === o2) return true;

    // If they are not strictly equal, they both need to be Objects
    if (!(o1 instanceof Object) || !(o2 instanceof Object)) return false;

    for (let p in o1) {
        if (o1.hasOwnProperty(p)) {
            // Check data type of property p
            if (typeof o1[p] !== typeof o2[p]) {
                return false;
            }

            // If p is a object, deep comparing it
            if (typeof o1[p] === "object") {
                let deepCompareResult = compareObject(o1[p], o2[p]);

                // Only return if deep comparing is false
                if (!deepCompareResult) {
                    return false;
                }
            } else {
                // Otherwise, p is a primitive, normally compare
                if (o1[p] !== o2[p]) {
                    return false;
                }
            }
        }
    }

    // Double check to make sure o1 and o2 have the exactly same properties
    for (let p in o2) {
        if (o2.hasOwnProperty(p)) {
            // Check data type of property p
            if (typeof o2[p] !== typeof o1[p]) {
                return false;
            }

            // If p is a object, deep comparing it
            if (typeof o2[p] === "object") {
                let deepCompareResult = compareObject(o2[p], o1[p]);

                // Only return if deep comparing is false
                if (!deepCompareResult) {
                    return false;
                }
            } else {
                // Otherwise, p is a primitive, normally compare
                if (o2[p] !== o1[p]) {
                    return false;
                }
            }
        }
    }

    return true;
}

module.exports = {
    writeLog,
    handleError,
    checkJSON,
    random,
    fromEntries,
    aggregateByKeys,
    snakeToCamel,
    camelKeysClone,
    arrayToObject,
    convertNumberToColumnLetter,
    convertColumnLetterToNumber,
    replaceDoubleToSingleQuote,
    removeAccent,
    removeSpecialCharacter,
    convertValueToArray,
    promiseObject,
    runJob,
    runJobGearman,
    jwtEncode,
    jwtDecode,
    formatRequestParams,
    getObjectPropSafely,
    removeDir,
    redisGetAsync: logData('redis')(redisGetAsync),
    redisSetAsync: logData('redis')(redisSetAsync),
    logData,
    debugEvent,
    getRandomInt,
    uuidv4,
    recursiveFormat,
    formatFilter,
    toCapital,
    prepareFile,
    moveFile,
    distributeRabbitMqMessage,
    distributeSlackMessage,
    getRandomString,
    compareObject,
};
