// Libraries
const nodemailer = require('nodemailer');
const Logger = require("@library/Logger")

// Utils
const {handleError} = require('@library/Utils');

// Constants
const Constant = require('@config/common/constant');
const PATH = '@library/Email';
const EMAIL_CONFIG = Constant.get('EMAIL_CONFIG');
const GMAIL_OPTIONS = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true
};

const getEmailConfig = (networkId) => {
    return EMAIL_CONFIG[networkId] || EMAIL_CONFIG[10507] || {};
};

const getEmailFolder = networkId => {
    const emailConfig = getEmailConfig(networkId);

    return emailConfig.FOLDER || 'NewSubmission';
};

const getEmailTemplate = (networkId, type, payload) => {
    if (!type) {
        return null;
    }

    try {
        const folder = getEmailFolder(networkId);

        return require(`@library/EmailTemplates/${folder}/${type}`)(payload);
    } catch (err) {
        Logger.error(err.message);

        return null;
    }
};

/**
 * Get default sender with networkId
 * @param {*} networkId
 * @returns {Sender}
 */
const getDefaultSender = networkId => {
    const emailConfig = getEmailConfig(networkId);

    return emailConfig.SENDER || {};
};

/**
 * @typedef {String} Email
 */

/**
  * @typedef {Object} Sender
  * @property {String} name
  * @property {Email} email
  * @property {String} password
  */

/**
 * @callback TemplateCreator
 * @param {*} payload
 * @returns {string}
 */

/**
  * @typedef {Object} Template
  * @property {String | TemplateCreator} html
  * @property {*} payload
  */

/**
* @typedef {Object} EmailOption
* @property {String} host
* @property {Number} port
* @property {Boolean} secure
*/

/**
 * @typedef {Object} EmailConfig
 * @property {String} subject
 * @property {Sender} from
 * @property {Email | Array.<Email>} to
 * @property {Email | Array.<Email>} cc
 * @property {Template} template
 * @property {EmailOption} options
 * @property {Array.<Object>} attachments
 */

/**
  * Send email
  * @param {EmailConfig} config
  * @returns {Boolean} status
  */
const sendEmail = async config => {
    const {
        template,
        options = GMAIL_OPTIONS,
        from = getDefaultSender(),
        ...emailInfo
    } = config || {};

    if (!from) {
        return false;
    }

    const {name, email: address, password} = from;

    if (options.secure) {
        options.auth = {
            user: address,
            pass: password
        };
    }

    const transporter = nodemailer.createTransport(options);

    const mailOptions = {
        from: {
            name,
            address
        },
        ...emailInfo,
        html: template
    };

    try {
        await transporter.sendMail(mailOptions);

        return true;
    } catch (error) {
        handleError(error, {
            action: new Error().stack,
            path: PATH,
            args: {
                mailOptions
            }
        });

        return false;
    }
};

module.exports = {
    sendEmail,
    getEmailTemplate,
    getDefaultSender
};
