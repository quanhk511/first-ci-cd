// PostgreSQL Connection
const Config = require('@library/Config');
const {Pool, Client} = require('pg');

// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/Database';
const pools = {};

const getInstance = async instance => {
    let connection = null;

    try {
        let config = Config.get('database');

        if (instance && config && config.postgreSQL && config.postgreSQL[instance]) {
            let instanceInfo = config.postgreSQL[instance];

            if (instanceInfo.username && instanceInfo.host && instanceInfo.port && instanceInfo.password && instanceInfo.database) {
                const {username: user, host, port, password, database} = instanceInfo;

                connection = new Client({
                    user,
                    host,
                    database: database,
                    password,
                    port
                });

                await connection.connect();
            }
        }
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {
                instance
            }
        });
    }

    return connection;
};

const getInstanceFromBool = async (instance) => {
    let connection = null;

    try {
        if (instance) {
            let pool;

            if (pools[instance]) {
                pool = pools[instance];
            } else {
                pool = await createPoolConnection(instance);
            }

            if (pool) {
                connection = await pool.connect();
            }
        }
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {
                instance
            }
        });
    }

    return connection;
};

const createPoolConnection = async instance => {
    let pool;
    try {
        let config = Config.get('database');

        if (instance && config && config.postgreSQL && config.postgreSQL[instance]) {
            let instanceInfo = config.postgreSQL[instance];

            if (instanceInfo.username && instanceInfo.host && instanceInfo.port && instanceInfo.password && instanceInfo.database) {
                const {username: user, host, port, password, database} = instanceInfo;

                pool = new Pool({
                    user,
                    host,
                    database,
                    password,
                    port,
                    max: 100,
                });

                pool.on('error', (err, client) => {
                    console.error('Unexpected error on idle client', err);
                    process.exit(-1);
                });

                pools[instance] = pool;
            }
        }
    } catch (error) {
        handleError(error, {
            action: new Error().stack,
            path: PATH,
            args: {
                instance
            }
        });
    }

    return pool;
};

/**
 * Init all pools in config file
 */
const init = async () => {
    try {
        let config = Config.get('database');

        // Init PostgreSQL databases
        if (config.postgreSQL && Object.keys(config.postgreSQL).length) {
            Object.entries(config.postgreSQL).forEach(([instance, instanceInfo]) => {
                if (!instanceInfo.initOnDemain) {
                    createPoolConnection(instance);
                }
            });
        }
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
        });
    }
};

const destroy = async () => {
    try {

    } catch (err) {
        handleError(err, {
            action: new Error().stack,
        });
    }
};

module.exports = {
    getInstance,
    getInstanceFromBool,
    init,
    destroy
};
