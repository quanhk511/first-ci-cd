const XLSX = require('xlsx');
const axios = require('axios');
const {handleError} = require('@library/Utils');

const PATH = '@library/Excel.js';

/**
 * Excel data
 * @typedef {String[][]} Data
 */

/**
 * Excel Header definition
 * @typedef {Array.<HeaderItem>} Header
 */

/**
 * @typedef {Object} HeaderItem
 * @property {String} key
 * @property {String} label
 * @property {String|Number} width - Column width
 * @property {(row: []) => any} get - Label getter from row
 */

/**
 * Read an excel file from url
 * @param {string} fileUrl
 * @returns {Promise<Data | {message: string}>} excel data
 */
const readExcelUrl = async fileUrl => {
    try {
        const res = await axios(fileUrl, {responseType: 'arraybuffer'});

        if (res && res.data) {
            const data = new Uint8Array(res.data);

            const wb = XLSX.read(data, {type: 'array'});

            if (wb.SheetNames.length) {
                return XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]], {
                    header: 1
                });
            }

            handleError(new Error('File is empty'), {
                action: new Error().stack,
                path: PATH,
                args: {
                    fileUrl
                }
            });

            return {
                message: 'File is empty'
            };
        }

        handleError(new Error('File URL invalid'), {
            action: new Error().stack,
            path: PATH,
            args: {
                fileUrl
            }
        });

        return {
            message: 'File URL invalid'
        };
    } catch (err) {
        handleError(err, {
            name: new Error().stack,
            path: PATH,
            args: fileUrl
        });
    }
};

/**
 * Write excel to file in sync
 * @param {Header} header
 * @param {Data} data
 * @param {String} fileName
 */
const writeFileSync = (header, data, fileName) => {
    let writeData = [];

    if (Array.isArray(data)) {
        if (!data.length) {
            return {
                error: 'Data is empty'
            };
        }

        const headerRow = [];
        const arrWidth = [];

        if (Array.isArray(header) && header.length) {
            header.forEach((detail) => {
                if (detail.label) {
                    headerRow.push(detail.label);
                }

                if (detail.width) {
                    arrWidth.push({wch: +detail.width});
                }
            });

            data.forEach(row => {
                const item = {};

                header.forEach(({key, ...detail}) => {
                    if (typeof detail === 'object' && detail.label) {
                        if (detail.get && typeof detail.get === 'function') {
                            // Get data
                            item[detail.label] = detail.get(row);
                        } else {
                            item[detail.label] = row[key];
                        }
                    }
                });

                if (Object.keys(item).length) {
                    // Update the column width with the data's length (if the data's length is greater)
                    const keys = Object.keys(item);
                    const values = Object.values(item);

                    for (let i = 0; i < keys.length; i++) {
                        arrWidth[i].wch = Math.max(arrWidth[i].wch, values[i] ? values[i].toString().trim().length : 0);
                    }

                    writeData.push(item);
                }
            });
        } else {
            writeData = data;
        }

        const sheet = XLSX.utils.json_to_sheet(writeData, {
            header: headerRow
        });

        // Assign width
        sheet['!cols'] = arrWidth;

        /* add to workbook */
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, sheet, 'Sheet 1');

        /* generate an XLSX file */
        try {
            XLSX.writeFile(workbook, fileName);
        } catch (err) {
            handleError(err, {
                name: new Error().stack,
                path: PATH
            });

            return {
                error: 'Can not write file data'
            };
        }
    } else {
        return {
            error: 'Data must be an array'
        };
    }
};

module.exports = {
    readExcelUrl,
    writeFileSync
};
