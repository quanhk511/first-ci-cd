const moment = require('moment');
const monitor = require('@antscorp/monitor-nodejs');
const Config = require('@library/Config');

const {handleError, debugEvent} = require('@library/Utils');

var connections = [];
const PATH = '@library/Elastic';

class ElasticSearch {
    constructor(instanceName = 'info_master', indexName, objectInfo) {
        this.instanceName = instanceName;
        this.indexName = indexName;
        this.objectInfo = objectInfo;
        this.instance = getInstance(this.instanceName) || {};
        this.client = this.instance.client || {};

        if (this.instance.prefix) {
            this.indexName = `${this.instance.prefix}${this.indexName}`;
        }
    }

    async index(data) {
        try {
            if (!data) {
                return null;
            }

            await validateIndex(this.client, this.indexName, this.objectInfo);

            let id = '';
            let body = {};

            Object.entries(data).forEach(([key, val]) => {
                const newKey = key.toLowerCase();

                if (this.objectInfo.privateKey.some(privateKey => privateKey === newKey)) {
                    id = `${id}${val}`;
                }

                if (this.objectInfo.name.some(name => name === newKey)) {
                    body[`${newKey}_raw`] = removeSpecialCharacter(removeAccent(`${val}`.toString().toLocaleLowerCase()));
                }

                if (this.objectInfo.search.some(name => name === newKey)) {
                    if (!body['data_search']) {
                        body['data_search'] = '';
                    }

                    body['data_search'] = removeSpecialCharacter(removeAccent(`${body['data_search']} ${val}`.toString().toLocaleLowerCase())).trim();
                }

                if (typeof this.objectInfo.mapping[key] !== 'undefined') {
                    body[newKey] = val;
                }
            });

            const request = {
                index: this.indexName,
                type: this.objectInfo.type,
                id,
                body
            };

            const response = await this.client.index(request).then().catch((error) => {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params: error.meta.body.error
                    }
                });
            });

            await this.client.indices.refresh();

            if (response && response.body.created && response.body.created === true) {
                return response;
            }

            return null;
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    data
                }
            });
        }
    }

    async bulk(data) {
        try {
            if (!data) {
                return null;
            }

            await validateIndex(this.client, this.indexName, this.objectInfo);

            let body = [];

            data.forEach(doc => {
                let id = this.objectInfo.privateKey.reduce((result, privateKey) => `${result}${doc[privateKey]}`, '') || '';
                let index = {
                    _index: this.indexName,
                    _type: this.objectInfo.type,
                    _id: id
                };

                let newDoc = {};

                Object.entries(doc).forEach(([key, val]) => {
                    const newKey = key.toLowerCase();

                    if (this.objectInfo.name.some(name => name === newKey)) {
                        newDoc[`${newKey}_raw`] = removeSpecialCharacter(removeAccent(`${val}`.toString().toLocaleLowerCase()));
                    }

                    if (this.objectInfo.search.some(name => name === newKey)) {
                        if (!newDoc['data_search']) {
                            newDoc['data_search'] = '';
                        }

                        newDoc['data_search'] = removeSpecialCharacter(removeAccent(`${newDoc['data_search']} ${val}`.toString().toLocaleLowerCase())).trim();
                    }

                    if (typeof this.objectInfo.mapping[key] !== 'undefined') {
                        newDoc[newKey] = val;
                    }
                });
                body = [...body, {index}, newDoc];
            });

            // Put mapping before update (to ensure elastic search always has latest mapping)
            await this.putMapping();

            return this.client.bulk({refresh: true, body});
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    data
                }
            });
        }
    }

    async search({page = 1, limit = 10, ...searchFields}) {
        try {
            let request = {
                index: this.indexName,
                type: this.objectInfo.type,
                from: page > 1 ? ((page - 1) * limit) : 0,
                size: limit,
                body: {
                    query: {
                        bool: {
                            must: []
                        }
                    }
                }
            };

            if (searchFields) {
                Object.entries(searchFields).map(([key, val]) => {
                    const searchTerm = {
                        [this.objectInfo.searchMapping[key].alias]: val
                    };

                    if (Array.isArray(val)) {
                        request.body.query.bool.must.push({
                            terms: searchTerm
                        });
                    } else {
                        request.body.query.bool.must.push({
                            term: searchTerm
                        });
                    }
                });
            }

            return this.client.search(request).then(res => {
                const rows = res.body.hits.hits.map(row => recursiveFormat(row._source));
                const total = res.body.hits.total;

                return {rows, total};
            }).catch(error => ({error}));
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    searchFields, page, limit
                }
            });
        }
    }

    async filter({page = 1, limit = 10, ...filter}) {
        try {
            let request = {
                index: this.indexName,
                type: this.objectInfo.type,
                from: page > 1 ? ((page - 1) * limit) : 0,
                size: limit,
                body: {
                    query: {
                        bool: {
                            must: [],
                            must_not: [],
                            should: []
                        }
                    }
                }
            };

            if (filter) {
                filter = formatFilter(filter, this.objectInfo.filterMapping);
                filter = validateFilters(filter, this.objectInfo.filterMapping);

                if (filter.must && Array.isArray(filter.must)) {
                    request.body.query.bool.must = filter.must;
                }

                if (filter.mustNot && Array.isArray(filter.mustNot)) {
                    request.body.query.bool.must_not = filter.mustNot;
                }

                if (filter.should && Array.isArray(filter.should)) {
                    request.body.query.bool.should = filter.should;
                }
            }

            return this.client.search(request).then(res => {
                const rows = res.body.hits.hits.map(row => recursiveFormat(row._source));
                const total = res.body.hits.total;

                return {rows, total};
            }).catch(error => ({error}));
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    filter
                }
            });
        }
    }

    async update(data) {
        try {
            if (!data) {
                const error = new Error('Data not found');

                return {error};
            }

            let id = this.objectInfo.privateKey.reduce((result, privateKey) => `${result}${data[privateKey]}`, '') || '';

            let body = Object.entries(data)
                .filter(([key, _]) => this.objectInfo.mapping[key] !== undefined)
                .reduce((accum, [key, value]) => {
                    accum[key.toLowerCase()] = value;
                    return accum;
                }, {});

            if (id && Object.entries(body).length !== 0) {
                const request = {
                    index: this.indexName,
                    type: 'data',
                    id,
                    retryOnConflict: 1, // Retry 1 time if there is conflict
                    body: {
                        doc: body
                    }
                };

                await this.client.update(request);

                await this.client.indices.refresh();

                return data;
            }
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    data
                }
            });
        }
    }

    async delete(data) {
        try {
            if (!data) {
                const error = new Error('Data not found');

                return {error};
            }

            await this.client.indices.delete({
                index: data.index
            });

            return data;
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {
                    data
                }
            });
        }
    }

    putMapping() {
        try {
            return this.client.indices.putMapping({
                index: this.indexName,
                type: 'data',
                body: {
                    properties: this.objectInfo.mapping
                }
            });
        } catch (error) {
            handleError(error, {
                component: PATH,
                action: new Error().stack,
                args: {}
            });
        }
    }

    get get() {
        return {
            client: this.client,
            indexName: this.indexName
        };
    }
}

const getInstance = (instanceName) => {
    try {
        const Config = require('@library/Config');
        const {getObjectPropSafely} = require('@library/Utils');
        let config = Config.get('elastic');
        let instance = null;

        if (getObjectPropSafely(() => config.elastic[instanceName])) {
            let {host, transportPort, httpPort, prefix} = config.elastic[instanceName];

            if (!host || !httpPort) {
                return;
            }

            const pattern = `${host}:${httpPort}`;

            if (!connections[pattern]) {
                const {Client} = require('@elastic/elasticsearch');

                connections[pattern] = {
                    host,
                    transportPort,
                    httpPort,
                    prefix,
                    client: new Client({
                        node: 'http://' + host + ':' + httpPort,
                        requestTimeout: 30000
                    })
                };

                connections[pattern].client.on('request', onRequest);
                connections[pattern].client.on('response', onResponse);
            }

            instance = getObjectPropSafely(() => connections[pattern]);
        }

        return instance;
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                instanceName
            }
        });
    }
};

const validateIndex = async (client, indexName, objectInfo) => {
    try {
        if (!indexName || !objectInfo) {
            return;
        }

        const exists = await client.indices.exists({
            index: indexName
        });

        if (!exists.body) {
            return client.indices.create({
                index: indexName,
                body: {
                    settings: {
                        number_of_shards: 2,
                        number_of_replicas: 1,
                        max_result_window: 1000000,
                        analysis: {
                            analyzer: {
                                my_analyzer: {
                                    tokenizer: 'icu_tokenizer',
                                    char_filter: ['html_strip'],
                                    filter: ['lowercase']
                                },
                                my_analyzer_folding: {
                                    tokenizer: 'icu_tokenizer',
                                    char_filter: ['html_strip'],
                                    filter: ['icu_folding', 'lowercase']
                                }
                            }
                        }
                    },
                    mappings: {
                        data: {
                            _source: {
                                enabled: true
                            },
                            properties: objectInfo.mapping
                        }
                    }
                }
            });
        }
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                client, indexName, objectInfo
            }
        });
    }
};

const validateFilters = (filters, filterMapping) => {
    try {
        const moment = require('moment');

        if (!filters || !(Array.isArray(filters) && filters.length) ||
            !(filterMapping && Object.keys(filterMapping).length)) {
            return;
        }

        let must = [];
        let mustNot = [];
        let should = [];

        filters.map(filter => {
            const {name, operator, value} = filter;

            if (!Object.keys(filterMapping).some(field => field === name)) {
                return;
            }

            const columnName = filterMapping[name].alias || '';

            if (!columnName) {
                return;
            }

            switch (operator) {
                case 'contain':
                    must.push({
                        wildcard: {
                            [columnName]: `*${removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))}*`
                        }
                    });
                    break;
                case 'doesNotContain':
                    mustNot.push({
                        wildcard: {
                            [columnName]: `*${removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))}*`
                        }
                    });
                    break;
                case 'is':
                    must.push({
                        wildcard: {
                            [columnName]: removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))
                        }
                    });
                    break;
                case 'startWith':
                    must.push({
                        prefix: {
                            [columnName]: value.toString().toLocaleLowerCase()
                        }
                    });
                    break;
                case 'matchesAny':
                    must.push({
                        terms: {
                            [columnName]: value
                        }
                    });
                    break;
                case 'on': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 23:59:59`,
                                gt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'before': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'after': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                gt: `${date} 23:59:59`
                            }
                        }
                    });
                    break;
                }
            }
        });

        return {must, mustNot, should};
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                filters, filterMapping
            }
        });
    }
};

const formatFilter = (filters, filterMapping) => {
    try {
        if (!filters || !checkJSON(filters) || !(filterMapping && Object.keys(filterMapping).length)) {
            return [];
        }

        filterMapping = Object.keys(filterMapping);

        filters = JSON.parse(filters);

        filters = filters.map(filterElement => {
            filterElement = Object.keys(filterElement).reduce((result, metric) => {
                const formattedMetric = metric.toString().toLowerCase();

                if (!filterMapping.some(fieldName => fieldName === formattedMetric)) {
                    return;
                }

                result.name = formattedMetric;

                Object.entries(filterElement[metric]).map(([operator, value]) => {
                    result.operator = operator;

                    switch (operator) {
                        case 'does_not_contain':
                            if (typeof value === 'string') {
                                result.operator = 'doesNotContain';
                                result.value = value;
                            }
                            break;
                        case 'start_with':
                            if (typeof value === 'string') {
                                result.operator = 'startWith';
                                result.value = value;
                            }
                            break;
                        case 'matches_any':
                            if (typeof value === 'string') {
                                value = value.split(',');

                                if (value.length) {
                                    result.operator = 'matchesAny';
                                    result.value = value;
                                }
                            }
                            break;
                        case 'contain':
                        case 'is':
                            if (typeof value === 'string') {
                                result.operator = operator;
                                result.value = value;
                            }
                            break;
                        case 'on':
                        case 'before':
                        case 'after':
                            const moment = require('moment');

                            if (typeof value === 'string' && moment(value, 'DD/MM/YYYY').isValid()) {
                                result.operator = operator;
                                result.value = value;
                            }

                            break;
                    }
                });

                return result;
            }, {});

            return filterElement;
        });

        return filters.filter(Boolean);
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                filters, filterMapping
            }
        });
    }
};

const onRequest = (error, result) => {
    try {
        const moment = require('moment');
        const monitor = require('@antscorp/monitor-nodejs');

        if (error) {
            handleError(error, {
                path: PATH,
                action: new Error().stack,
                args: {
                    result
                }
            });
        }

        if (result && result.meta && result.meta.connection) {
            let connection = result.meta.connection;
            let request = result.meta.request;
            let breadcrumbs = [];

            if (connection && request) {
                const time = moment().format('DD/MM/YYYY HH:mm:ss');

                breadcrumbs.push({
                    type: 'elasticsearch',
                    value: {
                        type: 'onRequest',
                        connection: connection.id || '',
                        params: request.params || {}
                    },
                    time: time
                });
            }

            if (breadcrumbs.length) {
                monitor.setBreadcrumbs(breadcrumbs);
            }
        }
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                error, result
            }
        });
    }
};

const onResponse = (error, result) => {
    try {
        const moment = require('moment');
        const monitor = require('@antscorp/monitor-nodejs');
        const {handleError, debugEvent} = require('@library/Utils');

        if (error) {
            handleError(error, {
                path: PATH,
                action: new Error().stack,
                args: {
                    result
                }
            });
        }

        if (result && result.body && result.body.hits) {
            const time = moment().format('DD/MM/YYYY HH:mm:ss');

            monitor.setBreadcrumbs([{
                type: 'elasticsearch',
                value: {
                    type: 'onResponse',
                    hits: result.body.hits || {}
                },
                time: time
            }]);
        }

        debugEvent.emit('saveDebugInfo', {
            currentTime: global.currentTime,
            source: 'elastic-search',
            data: result.statusCode === null ? {
                body: result.meta.request.params.body,
                path: result.meta.request.params.path,
                querystring: result.meta.request.params.querystring
            } : result.body.hits,
            statusCode: result.statusCode || 200,
            type: result.statusCode === null ? 'request' : 'response'
        });
    } catch (error) {
        handleError(error, {
            component: PATH,
            action: new Error().stack,
            args: {
                error, result
            }
        });
    }
};

module.exports = ElasticSearch;
