const fs = require('fs');

module.exports.get = function (fileName) {
    if (fileName) {
        let path = process.env.PWD + '/config/autoload/' + process.env.APPLICATION_ENV + '/' + fileName + '.js';

        if (fs.existsSync(path)) {
            return require(path);
        }

        return false;
    }
};
