const log4js = require("log4js");

const logConfig = {
    appenders: {
        console: {
            type: "console",
            layout: {
                type: "pattern",
                pattern: "%[%d{dd/MM/yyyy hh:mm:ss} %p%] - %f{3} at %l:%o - %m%n"
            }
        }
    },
    categories: {
        default: {
            appenders: ["console"],
            level: process.env.LOG_LEVEL,
            enableCallStack: true
        }
    },
    pm2: true,
    pm2InstanceVar: "NODE_APP_INSTANCE"
};

log4js.configure(logConfig);

const Logger = log4js.getLogger();

module.exports = Logger;