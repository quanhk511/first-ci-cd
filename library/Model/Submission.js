// DAO
const SubmissionDAO = require("@library/DAO/Submission")

// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/Model/Submission.js';

const create = async params => {
    try {
        return await SubmissionDAO.createSubmission(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
}

module.exports = {
    create,
};