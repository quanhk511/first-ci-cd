const FilterDAO = require('@library/DAO/Filter');
const Redis = require('@library/Redis');
const {promisify} = require('util');

// Utils
const {handleError, checkJSON} = require('@library/Utils');

const PATH = '@library/Model/Metric.js';

const constant = {
    FILTER_STATUS_ACTIVE: 1,
    FILTER_STATUS_INACTIVE: 0
};

const getSavedFilterKey = (params) => {
    const {type: objectId, userId, networkId} = params;

    return `antalyser:savedFilter:objectId:${objectId}:userId:${userId}:networkId:${networkId}`;
};

const getListing = async params => {
    try {
        return await FilterDAO.getListing(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const createFilter = async params => {
    try {
        const result = await FilterDAO.create(params);

        return result
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const updateFilter = async params => {
    try {
        const result = await FilterDAO.update(params);

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    ...constant,
    getListing,
    createFilter,
    updateFilter,
    getSavedFilterKey
};
