// DAOs
const FormDAO = require('@library/DAO/Form');
const FormElasticSearch = require('@library/ElasticSearch/Form');

// Utils
const { handleError } = require("@library/Utils");

const PATH = "@library/Model/Form.js";

const constant = {
    STATUS: {
        DRAFT: 22,
        PUBLISHED: 55,
        REMOVED: 80,
        REMOVED_ES: 0,
    }
}

const getAllForm = async params => {
    try {
        return await FormDAO.getAllForms(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getAllGdprOptions = async (params) => {
    try {
        return await FormDAO.getAllGdprOptions(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const createNewDraftForm = async (params) => {
    try {
        return await FormElasticSearch.index(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

const createNewDraftFormDB = async (params) => {
    try {
        return await FormDAO.createNewForm(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

const updateForm = async (params) => {
    try {
        return await FormDAO.updateForm(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

const getFormDetail = async (params) => {
    try {
        return await FormDAO.getFormDetail(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

const isFormNameExisted = async params => {
    try {
        return await FormDAO.isFormNameExisted(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
}

module.exports = {
    ...constant,
    createNewDraftForm,
    getAllForm,
    getAllGdprOptions,
    createNewDraftFormDB,
    updateForm,
    getFormDetail,
    isFormNameExisted,
};
