const MetricDAO = require('@library/DAO/Metric');
const Redis = require('@library/Redis');
const {promisify} = require('util');

// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/Model/Metric.js';

const constant = {
    DATA_TYPE: {
        TEXT: 1,
        NUMBER: 2,
        PERCENT: 3,
        CURRENCY: 4,
        DATE_TIME: 5,
        LIST: 8,
        GDPR: 13,
        FILE: 14,
    },
    FORMAT_TYPE: {
        CHART: 'chart',
        GRID: 'grid',
    },
    PAGE_TYPE: {
        ALL_FORM: 1,
        FORM_DETAIL: 3,
    },
    SORT_TYPE: {
        ASCENDING: "ASC",
        DESCENDING: "DESC",
    },
};

const getFiltersKey = (params) => {
    const {type: objectId, userId, networkId} = params;

    return `form:filters:objectId:${objectId}:userId:${userId}:networkId:${networkId}`;
};

const getListing = async (params) => {
    try {
        return await MetricDAO.getListing(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                ...params
            }
        });
    }
};

const getDetailMetric = async params => {
    try {
        return await MetricDAO.getDetailMetric(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    ...constant,
    getListing,
    getFiltersKey,
    getDetailMetric
};
