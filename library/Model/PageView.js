// DAO
const PageViewDAO = require("@library/DAO/PageView")

// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/Model/PageView.js';

const create = async params => {
    try {
        return await PageViewDAO.createPageView(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
}

module.exports = {
    create,
};