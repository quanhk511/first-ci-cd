// Library
const Logger = require("@library/Logger");

// Utils
const util = require("util");
const {handleError} = require('@library/Utils');

// DAO
const ColumnDAO = require('@library/DAO/Column');

const PATH = '@library/Model/Column.js';

const constant = {
    ADD_COLUMN: 1,
    ADD_COLUMN_CUSTOM: 2,
    MODIFY_COLUMN_NAME_NOT_EXIST: 0,
    MODIFY_COLUMN_NAME_EXIST: 1,
    MODIFY_COLUMN_LASTED: 1,
    MODIFY_COLUMN_ACTIVE: 1,
    COLUMN_FIXED: 1,
    COLUMN_NOT_FIXED: 0,

    DATA_TYPE: {
        TEXT: 1,
        NUMBER: 2,
        PERCENT: 3,
        CURRENCY: 4,
        DATE: 5,
        ARRAY: 6,
        HOUR_OF_DAY: 7,
        DAY_OF_WEEK: 8,
        WEEK: 9,
        YEAR_QUARTER: 10,
        YEAR: 11,
        COUNTRY: 12,
        CITY: 13,
        YEAR_MONTH: 14
    }
};

const getListingModifyColumn = async params => {
    try {
        const {
            userId = null,
            networkId = null,
            formId = null,
            type = null,
            columns = null,
            order = null
        } = params;

        if (!userId || !networkId || !type) {
            return null;
        }

        return await ColumnDAO.getListingModifyColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            type: +type,
            columns,
            order
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getLatestModifyColumn = async params => {
    try {
        const {
            userId = null,
            networkId = null,
            formId = null,
            type = null
        } = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true
        }));

        if (!userId || !networkId || !type) {
            return null;
        }

        return await ColumnDAO.getLatestModifyColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            type: +type
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const checkNameColumn = async params => {
    try {
        const {
            userId = null,
            networkId = null,
            formId = null,
            modifyName = null,
            type = null
        } = params;

        if (!userId || !networkId || !modifyName || !type) {
            return null;
        }

        return await ColumnDAO.checkNameColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            modifyName,
            type: +type
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const addModifyColumn = async params => {
    try {
        const {
            userId = null,
            networkId = null,
            modifyName = null,
            columns = null,
            formId = null,
            type = null,
            status = null,
            isLasted = null,
        } = params;

        if (!userId || !networkId) {
            return null;
        }

        return await ColumnDAO.addModifyColumn({
            userId: +userId,
            networkId: +networkId,
            modifyName,
            columns,
            formId: formId,
            type: +type,
            status: +status,
            isLasted: +isLasted,
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return null;
    }
};

const updateModifyColumn = async params => {
    try {
        const {
            id = null,
            userId = null,
            networkId = null,
            modifyName = null,
            form_id = null,
            columns = null,
            objType = null,
            status = null,
            isLasted = null,
        } = params;

        if (!id || !userId || !networkId || !objType) {
            return null;
        }

        return await ColumnDAO.updateModifyColumn({
            id: +id,
            userId: +userId,
            networkId: +networkId,
            formId: form_id,
            modifyName,
            columns,
            type: objType,
            status: status,
            isLasted: isLasted,
        });
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return null;
    }
};

module.exports = {
    ...constant,
    getListingModifyColumn,
    getLatestModifyColumn,
    checkNameColumn,
    addModifyColumn,
    updateModifyColumn
};

