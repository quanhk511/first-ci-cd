// Library
const Logger = require("@library/Logger");

// DAO
const FieldDAO = require('@library/DAO/Field');

// Utils
const {handleError} = require('@library/Utils');
const util = require("util");

// Constant
const PATH = '@library/Model/Field';
const constant = {
    FIELD_TYPE: {
        SINGLE_LINE_TEXT: 1,
        NUMBER: 2,
        SINGLE_CHECKBOX: 3,
        MULTIPLE_CHECKBOX: 4,
        DROP_DOWN: 5,
        MULTI_LINE_TEXT: 6,
        RADIO: 7,
        DATE_PICKER: 8,
        FILE: 9,
        HEADER_TEXT: 10,
        PARAGRAPH: 11,
        IMAGE: 12,
        GDPR: 13,
    },
    STATUS: {
        INACTIVE: 0,
        ACTIVE: 1,
    }
};

const createField = async params => {
    try {
        return await FieldDAO.createField(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getAllYourFields = async params => {
    try {
        return await FieldDAO.getAllYourFields(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getFieldTypeList = async params => {
    try {
        return FieldDAO.getFieldTypes(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const isFieldNameValid = async params => {
    try {
        return await FieldDAO.isFieldNameValid(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getField = async params => {
    try {
        return await FieldDAO.getField(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getListFieldsByFormId = async params => {
    try {
        return await FieldDAO.getListFieldsByFormId(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getAllDefaultFields = async params => {
    try {
        return await FieldDAO.getListDefaultFields(params);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const updateField = async params => {
    try {
        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const result = await FieldDAO.updateField(params);

        Logger.debug("Result: ", util.inspect(result, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    ...constant,
    createField,
    getAllYourFields,
    getFieldTypeList,
    isFieldNameValid,
    getField,
    getListFieldsByFormId,
    getAllDefaultFields,
    updateField,
};

