// Libraries
const Joi = require('@hapi/joi');
const Logger = require("@library/Logger");
const monitor = require('@antscorp/monitor-nodejs');

// Model
const MetricModel = require('@library/Model/Metric');
const FieldModel = require("@library/Model/Field");
const FilterModel = require('@library/Model/Filter');

// Utils
const util = require("util");
const {handleError, checkJSON, snakeToCamel, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/Business/Filter.js';

const getFilter = async (params) => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            form_id: Joi.when("object", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            object: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            userId = "",
            networkId = '',
            form_id: formId,
            object,
        } = params;

        if (!userId || !networkId || !object) {
            return {
                ...result,
                message: 'Request invalid',
                data: {
                    status: 0
                }
            };
        }

        // Get list of filter metrics
        let filterRaw = await MetricModel.getListing({
            userId: userId,
            networkId: networkId,
            formId,
            type: +object,
            byFnc: 'FILTER',
            columns: 'METRIC_ID, METRIC_NAME,METRIC_CODE, METRIC_LEVEL, LEVEL_POSITION, OPERATOR, FORMULA, PARENT_ID, STATUS, FIX_COLUMN, IS_SORT, DATA_TYPE, IS_PERFORMANCE',
            order: 'FIX_COLUMN DESC, LEVEL_POSITION',
        }) || [];

        monitor.setBreadcrumbs([{
            filterRaw
        }]);

        let filters = [];
        let objChildFilters = {};

        // Handle filter data
        if (filterRaw.length) {
            // Handle parents
            filterRaw.map(field => {
                const {
                    operator = {},
                    metric_name: metricName = '',
                    data_type: dataType = '',
                    parent_id: parentId = '',
                    metric_id: metricId = ''
                } = field;
                const metricCode = field.metric_code ? field.metric_code : metricId;

                let filterFormatted = {
                    metricName,
                    metricCode,
                    dataType,
                };

                // Validate filter operator from DAO
                switch (typeof operator) {
                    // If the operator is a JSON string, parse it
                    case "string":
                        filterFormatted.operator = checkJSON(operator) ? JSON.parse(operator) : {};
                        break;
                    // If the operator is an object already
                    case "object":
                        filterFormatted.operator = operator;
                        break;
                    // Otherwise, no operator
                    default:
                        filterFormatted.operator = {};
                }

                if (!parentId || parentId === '0') {
                    filters.push({
                        ...filterFormatted,
                        metricId
                    });
                } else {
                    if (!objChildFilters[parentId]) {
                        objChildFilters[parentId] = [];
                    }

                    objChildFilters[parentId].push(filterFormatted);
                }

            });

            // Handle child
            if (filters.length) {
                filters = filters.map(filter => {
                    const {metricId = ''} = filter;

                    if (objChildFilters[metricId]) {
                        filter.child = objChildFilters[metricId];
                    }

                    delete filter.metricId;

                    return filter;
                });
            }
        }

        // If the page type is Form Detail, find corresponding fields of the form and append to list of filter metrics
        if (+object === MetricModel.PAGE_TYPE.FORM_DETAIL) {
            // Get a parent metric for Form Metric or create a new one
            const formMetric = filters.find(filters => filters.metricName === "Form metric") || {
                metricName: "Form metric",
                metricCode: "METR_00000030",
                dataType: "1",
                operator: {},
            };

            // Get list fields of the form
            const listFieldsByFormId = await FieldModel.getListFieldsByFormId(params);

            // Append fields to list children
            formMetric.child = formMetric.child
                .concat(listFieldsByFormId
                    .filter(field => (
                        (+field["field_type_id"] !== FieldModel.FIELD_TYPE.GDPR) &&
                        (+field["field_type_id"] !== FieldModel.FIELD_TYPE.HEADER_TEXT) &&
                        (+field["field_type_id"] !== FieldModel.FIELD_TYPE.PARAGRAPH) &&
                        (+field["field_type_id"] !== FieldModel.FIELD_TYPE.IMAGE))
                    )
                    .map(field => ({
                        metricName: field["field_label"],
                        metricCode: field["field_id"],
                        dataType: `${MetricModel.DATA_TYPE.TEXT}`,
                        operator: [
                            {name: "contains", value: "contain"},
                            {name: "does not contain", value: "does_not_contain"},
                            {name: "is", value: "is"},
                            {name: "start with", value: "start_with"}
                        ]
                    }))
                );
        }

        return {
            ...result,
            data: {
                filters,
                status: 1
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

const getSavedFilter = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            form_id: Joi.when("object", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            object: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            userId = "",
            networkId = '',
            form_id: formId,
            object
        } = params;

        if (!userId || !networkId || !object) {
            return {
                ...result,
                message: 'Request invalid',
                data: {
                    status: 0
                }
            };
        }

        // Get listing saved filters
        let savedFilter = await FilterModel.getListing({
            userId: userId,
            networkId: networkId,
            formId,
            type: +object,
            columns: 'FILTER_ID, NETWORK_ID, USER_ID, FILTER_NAME, RULES, TYPE, STATUS, CTIME, UTIME',
        }) || [];

        Logger.trace("FilterModel.getListing() result: ", util.inspect(savedFilter, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        // Handle saved filter
        if (savedFilter.length) {
            savedFilter = savedFilter.reduce((filter, field) => {
                const {rules = '', status = null, filter_name: filterName = '', filter_id: filterId = ''} = field;
                let filterFormatted = {
                    filterName,
                    filterId
                };

                filterFormatted.rules = checkJSON(rules) ? JSON.parse(rules) : [];

                Logger.trace("filterFormatted: ", util.inspect(filterFormatted, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

                if (Array.isArray(filterFormatted.rules) && filterFormatted.rules.length) {
                    filterFormatted.rules = filterFormatted.rules.map(rule => {
                        rule = Object.keys(rule).reduce((newRule, ruleKey) => {
                            newRule[snakeToCamel(ruleKey)] = rule[ruleKey];

                            return newRule;
                        }, {});

                        return rule;
                    });
                }

                if (+status === +FilterModel.FILTER_STATUS_ACTIVE) {
                    filter.push(filterFormatted);
                }

                return filter;
            }, []);
        }

        return {
            ...result,
            data: {
                filters: savedFilter,
                status: 1
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

const createFilter = async (params) => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * filterName is a string and required
        // * rules is a json string and required
        // * form_id is a number and required if object is FORM_DETAIL
        // * object is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            filterName: Joi.string().required(),
            rules: Joi.string().required(),
            form_id: Joi.when("object", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            object: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        const {
            userId = "",
            networkId = '',
            filterName,
            rules,
            form_id: formId,
            object
        } = params;

        if (!userId || !networkId || !object) {
            return {
                ...result,
                message: 'Request invalid',
                data: {
                    status: 0
                }
            };
        }

        const saveFilterStatus = await FilterModel.createFilter({
            userId,
            networkId,
            filterName,
            formId,
            rules: rules,
            type: +object,
            status: FilterModel.FILTER_STATUS_ACTIVE,
        }) || 0;

        return {
            ...result,
            data: {
                status: saveFilterStatus
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

const updateFilter = async (params) => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * filterName is a string and required
        // * rules is a json string and required
        // * object is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            filterName: Joi.string().required(),
            rules: Joi.any().required(),
            object: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        const {
            _user_id: userId = null,
            _network_id: networkId = '',
            id: filterId = '',
            filterName = '',
            rules = [],
            object = null
        } = params;

        if (!userId || !networkId || !filterId) {
            return {
                ...result,
                message: 'Request invalid',
                data: {
                    status: 0
                }
            };
        }

        let updateParams = {
            userId: +userId,
            networkId: +networkId,
            filterId,
            status: FilterModel.FILTER_STATUS_ACTIVE
        };

        if (filterName) {
            updateParams.filterName = filterName.toString();
        }

        if (rules.length) {
            updateParams.rules = JSON.stringify(rules);
        }

        if (object) {
            updateParams.type = object;
        }

        const updateFilterInfo = await FilterModel.updateFilter(updateParams) || 0;

        return {
            ...result,
            data: {
                status: +updateFilterInfo
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const removeFilter = async (params) => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * id is a number and required
        const validateParams = Joi.object({
            id: Joi.number().required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        const {
            _user_id: userId = null,
            _network_id: networkId = '',
            id: filterId
        } = params;

        if (!userId || !networkId || !filterId) {
            return {
                ...result,
                message: 'Request invalid',
                data: {
                    status: 0
                }
            };
        }

        const removeFilterStatus = await FilterModel.updateFilter({
            userId: +userId,
            networkId: +networkId,
            status: FilterModel.FILTER_STATUS_INACTIVE,
            filterId,
        }) || 0;

        return {
            ...result,
            data: {
                status: +removeFilterStatus
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const checkFilterName = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * name is a string and required
        // * form_id is a number and required if object is FORM_DETAIL
        // * object is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            name: Joi.string().required(),
            form_id: Joi.when("object", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            object: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        const {name = ''} = params;

        const getListSavedFilters = await getSavedFilter(params);

        const filters = getObjectPropSafely(() => getListSavedFilters.data.filters) || [];

        if (!filters.length) {
            return result;
        }

        const duplicateFilter = filters.find(filter => filter["filterName"] === name);

        const data = {};
        if (duplicateFilter) {
            data.filterId = +duplicateFilter.filterId;
            data.filterName = duplicateFilter.filterName;
        }

        return {
            ...result,
            data,
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    getFilter,
    getSavedFilter,
    createFilter,
    updateFilter,
    removeFilter,
    checkFilterName
};
