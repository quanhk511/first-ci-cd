// Library
const Axios = require("axios");
const Joi = require("@hapi/joi");
const Logger = require("@library/Logger");

// Utils
const util = require("util");
const {handleError} = require("@library/Utils");

// Constant
const Constant = require("@config/common/constant");
const PATH = "@library/Business/Network.js";

const getNetworkInfo = async params => {
    try {
        // Validate params
        const validateParams = Joi.object({
            network_id: Joi.number().required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const AuthApiHost = Constant.get('AUTH_DOMAIN');
        const {network_id: networkId = 10507} = params;

        const networkInfoApi = `http${process.env.APPLICATION_ENV === "development" ? "" : "s"}:${AuthApiHost}/api/3rd/info?type=network-info&networkId=${+networkId}`;

        Logger.trace("Axios.get() URL: ", networkInfoApi);

        const data = await Axios({
            url: networkInfoApi,
            method: "GET",
        })
            .then(response => response && response.data ? response.data : null)
            .catch(error => {
                // Axios response error
                if (error.response) {
                    Logger.error("Axios.get() response error: ", util.inspect(error.response, {
                        breakLength: Infinity,
                        colors: true,
                        depth: Infinity,
                        showHidden: true,
                    }));
                }
                // Error on request
                else if (error.response) {
                    Logger.error("Axios.get() request error: ", util.inspect(error.request, {
                        breakLength: Infinity,
                        colors: true,
                        depth: Infinity,
                        showHidden: true,
                    }));
                }
                // Other error
                else {
                    Logger.error("Axios.get() error: ", util.inspect(error.message, {
                        breakLength: Infinity,
                        colors: true,
                        depth: Infinity,
                        showHidden: true,
                    }));

                }

                return null;
            });

        Logger.trace("Axios.get() return: ", util.inspect(data, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!data) {
            return {code: 404, message: `Cannot get network ${networkId}`};
        }

        return data;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
}

module.exports = {
    getNetworkInfo,
}