// Library
const Joi = require('@hapi/joi');
const Logger = require("@library/Logger");

// Model
const FormModel = require("@library/Model/Form");
const FieldModel = require('@library/Model/Field');

// ElasticSearch
const FormElasticSearch = require('@library/ElasticSearch/Form');

// Utils
const util = require("util");
const {handleError, getObjectPropSafely, moveFile, getRandomString, compareObject} = require('@library/Utils');

// Constants
const constant = require('@config/common/constant');
const PATH = '@library/Business/Field.js';

/**
 * Consent Item
 * @typedef {Object} GdprConsentItem
 * @property {string} label - Text label of this consent
 * @property {boolean} required - Determine this consent is required or not
 */

/**
 * Field Number Format type
 * @typedef {Object} NumberFormat
 * @property {string} type - Type of Format (currently supported format types are: Number | Currency | Percentage
 * @property {string} unit - Unit name (such as currency unit)
 */

/**
 * Field Image URL type
 * @typedef {Object} ImageUrl
 * @property {string} domain - Domain of the URL
 * @property {string} path - URL Path
 */

/**
 * Label Style field properties
 * @typedef {Object} LabelStyle
 * @property {"bottom"|"left"|"right"|"top"} align - Label alignment
 * @property {boolean} is_hidden - Should this label be hidden
 */

/**
 * Multiple Selection field Options
 * @typedef {Object} MultipleSelectionOptionItem
 * @property {string} label - Text label of this option
 * @property {boolean} preselect - Determine this option is preselected or not
 */

/**
 * Field Properties
 * @typedef {Object} FieldProperties
 * @property {"left"|"center"|"right"} [alignment] - Image Alignment (Field Type: Image)
 * @property {number} [allow_multiple_files] - Maximum number of uploading files per submission (Field Type: File)
 * @property {boolean} [checked] - Determine checkbox is checked or not (Field Type: Single Checkbox)
 * @property {string} [communication_consent] - Communication Consent text (Field Type: GDPR)
 * @property {Array<GdprConsentItem>} [consent_to_communicate] - Consent to Communicate checkboxes
 * (Field Type: GDPR)
 * @property {Array<GdprConsentItem>} [consent_to_process] - Consent to Process checkboxes (Field Type: GDPR)
 * @property {string | number | Date} [default_value] - Default value of field (Field Types: Single-line Text,
 * Number, Multi-line Text, Date Picker)
 * @property {Array<MultipleSelectionOptionItem>} [dropdown_options] - Options list of dropdown field
 * (Field Type: Dropdown)
 * @property {Array<MultipleSelectionOptionItem>} [field_options] - Options list of multiple checkboxes field
 * (Field Type: Multiple Checkboxes)
 * @property {Array<string>} [file_types] - List of allowed file extensions (Field Type: File)
 * @property {number} [height] - Height of Image (Field Type: Image)
 * @property {string} [help_text] - Field help text (Field Type: *)
 * @property {ImageUrl} [image] - URL of uploaded Image (Field Type: Image)
 * @property {boolean} [is_allow_multiple_files] - Determine multiple files uploading is allowed or not (Field Type: File)
 * @property {LabelStyle} [label_style] - Style of the field label (Field Type: all)
 * @property {number} [max] - Maximum value of a number (Field Type: Number)
 * @property {number} [max_file_size] - Maximum file size in MB (Field Type: File)
 * @property {number} [min] - Minimum value of a number (Field Type: Number)
 * @property {NumberFormat} [number_format] - Format type of number (Field Type: Number)
 * @property {string} [place_holder] - Field place holder (Field Types: Single-line Text, Number, Multiple-Line
 * Text)
 * @property {string} [privacy] - Privacy text (Field Type: GDPR)
 * @property {string} [process_consent_text] - Process to Consent text (Field Type: GDPR)
 * @property {Array<MultipleSelectionOptionItem>} [radio_options] - Options list of radio fields (Field Type: Radio)
 * @property {string} [required_message] - Custom required message (Field Type: all)
 * @property {string} [rich_text_editor] - HTML Text of Header or Paragraph (Field Types: Header Text, Paragraph)
 * @property {boolean} [set_number_limit] - Determine should apply limitations on this field (Field Type: Number)
 * @property {number} [width] - Width of Image (Field Type: Image)
 */

const createField = async params => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {}
        };

        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * form_id is number and required
        // * field_type_id is number and required
        // * label is string and optional
        // * internal_name is string and required
        // * description is string, empty is allowed and optional
        // * is_required is boolean and required
        const validator = Joi.object({
            form_id: Joi.number().required(),
            field_type_id: Joi.number().required(),
            label: Joi.string().allow("").required(),
            internal_name: Joi.string().required(),
            is_required: Joi.boolean().optional(),
        }).unknown();

        if (validator.validate(params).error) {
            return {
                ...result,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid",
                )
            };
        }
        //endregion

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FormModel.getFormDetail() params: ",
            util.inspect({formId: params.form_id, networkId: params.networkId}, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        // Validate form existence
        const formDetail = await FormModel.getFormDetail({
            formId: params.form_id,
            networkId: params.networkId,
        });

        Logger.trace("FormModel.getFormDetail() result: ",
            util.inspect(formDetail, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!Array.isArray(formDetail) || !formDetail.length) {
            return {
                code: 413,
                message: `Form with ID of ${params.form_id} is not existed`
            }
        }

        let fieldName = params.internal_name;

        Logger.trace("FieldModel.isFieldNameValid() params: ",
            util.inspect({...params, field_name: fieldName}, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        let isFieldNameValid = await FieldModel.isFieldNameValid({
            ...params,
            field_name: fieldName,
        });

        Logger.trace("FieldModel.isFieldNameValid() result: ",
            util.inspect(isFieldNameValid, {
                breakLength: Infinity, colors: true, depth: Infinity, showHidden: true
            }));

        if (!isFieldNameValid) {
            await new Promise((resolve, reject) => {
                const generateFieldNameRecursively = (() => {
                    const randomString = getRandomString(6);
                    fieldName = params.internal_name + "_" + randomString;

                    Logger.trace("FieldModel.isFieldNameValid() params: ",
                        util.inspect({...params, field_name: fieldName}, {
                            breakLength: Infinity,
                            colors: true,
                            depth: Infinity,
                            showHidden: true,
                        }));

                    return FieldModel.isFieldNameValid({
                        ...params,
                        field_name: fieldName,
                    })
                        .then(res => {
                            Logger.trace("FieldModel.isFieldNameValid() result: ",
                                util.inspect(res, {
                                    breakLength: Infinity,
                                    colors: true,
                                    depth: Infinity,
                                    showHidden: true,
                                }));

                            if (!res) {
                                return generateFieldNameRecursively();
                            }

                            return resolve(fieldName)
                        })
                        .catch(error => {
                            if (error) {
                                Logger.error("FieldModel.isFieldNameValid() error",
                                    util.inspect(error.message, {
                                        breakLength: Infinity,
                                        colors: true,
                                        depth: Infinity,
                                        showHidden: true,
                                    }));

                                return reject(error);
                            }
                        })
                })

                return generateFieldNameRecursively();
            })
        }

        params.internal_name = fieldName;

        // Form dynamic properties

        /** @type {FieldProperties} */
        const properties = {};

        //region Form dynamic properties mapping

        if (params.alignment) {
            properties.alignment = params.alignment;
        }

        if (params.allow_multiple_files) {
            properties.allow_multiple_files = params.allow_multiple_files;
        }

        if (params.checked) {
            properties.checked = params.checked;
        }

        if (params.communication_consent) {
            properties.communication_consent = params.communication_consent;
        }

        if (params.consent_to_communicate) {
            properties.consent_to_communicate = params.consent_to_communicate;
        }

        if (params.consent_to_process) {
            properties.consent_to_process = params.consent_to_process;
        }

        if (params.default_value) {
            properties.default_value = params.default_value;
        }

        if (params.dropdown_options) {
            properties.dropdown_options = params.dropdown_options;
        }

        if (params.field_options) {
            properties.field_options = params.field_options;
        }

        if (params.file_types) {
            properties.file_types = params.file_types;
        }

        if (params.height) {
            properties.height = params.height;
        }

        if (params.help_text) {
            properties.help_text = params.help_text;
        }

        if (params.is_allow_multiple_files) {
            properties.is_allow_multiple_files = params.is_allow_multiple_files;
        }

        if (params.label_style) {
            properties.label_style = params.label_style;
        }

        if (params.max) {
            properties.max = params.max;
        }

        if (params.max_file_size) {
            properties.max_file_size = params.max_file_size;
        }

        if (params.min) {
            properties.min = params.min;
        }

        if (params.number_format) {
            properties.number_format = params.number_format;
        }

        if (params.place_holder) {
            properties.place_holder = params.place_holder;
        }

        if (params.privacy) {
            properties.privacy = params.privacy;
        }

        if (params.process_consent_text) {
            properties.process_consent_text = params.process_consent_text
        }

        if (params.radio_options) {
            properties.radio_options = params.radio_options;
        }

        if (params.required_message) {
            properties.required_message = params.required_message;
        }

        if (params.rich_text_editor) {
            properties.rich_text_editor = params.rich_text_editor;
        }

        if (params.set_number_limit) {
            properties.set_number_limit = params.set_number_limit;
        }

        if (params.width) {
            properties.width = params.width;
        }

        //endregion

        let destFilePath;
        switch (+params.field_type_id) {
            // Move the image from static/ to CDN/ if the field is image
            case FieldModel.FIELD_TYPE.IMAGE: {
                if (!params.image) {
                    return {
                        code: 400,
                        message: 'Field image is required',
                    }
                }

                const fullSrcPath = constant.get("ROOT_PATH") + constant.get("STATIC_PATH") + params.image.path;
                const fullDesPath = constant.get("UPLOAD_PATH");

                destFilePath = await moveFile(fullSrcPath, fullDesPath);
                destFilePath = destFilePath.replace(constant.get("UPLOAD_PATH"), "");

                properties.image = {
                    domain: constant.get("C0_DOMAIN"),
                    path: destFilePath,
                };

                break;
            }
            default:
                break;
        }

        Logger.trace("FieldModel.createField() params: ",
            util.inspect({...params, properties: properties}, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        const createResult = await FieldModel.createField({...params, properties: properties});

        Logger.trace("FieldModel.createField() result: ",
            util.inspect(createResult, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!createResult || createResult <= 0) {
            return {
                code: 500,
                message: 'Cannot create field',
            }
        }

        let data = {
            field_id: +createResult
        };

        if (destFilePath) {
            data.image = properties.image;
        }

        return {
            ...result,
            data
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

const getAllYourFields = async params => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {},
        };

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FieldModel.getAllYourFields() params: ",
            util.inspect(params, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        let listYourFields = (await FieldModel.getAllYourFields(params)) || [];

        Logger.trace("FieldModel.getAllYourFields() result: ",
            util.inspect(listYourFields, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        // Handle response
        listYourFields = listYourFields.map(field => ({
            field_id: +field["field_id"],
            internal_name: field["field_name"],
            label: field["field_label"],
            field_type_id: +field["field_type_id"],
            description: field["description"],
            is_required: Boolean(+field["is_required"]),
            is_default: Boolean(+field["is_default"]),
            ...field["properties"]
        }));

        return {
            ...result,
            data: {
                listYourFields,
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
};

const getFieldTypes = async params => {
    let result = {
        code: 200,
        message: 'Success',
        data: {params}
    };

    Logger.debug("Params: ", util.inspect(params, {
        breakLength: Infinity,
        colors: true,
        depth: Infinity,
        showHidden: true,
    }));

    try {
        Logger.trace("FieldModel.getFieldTypeList() params: ",
            util.inspect(params, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        let fieldTypes = await FieldModel.getFieldTypeList(params) || [];

        Logger.trace("FieldModel.getFieldTypeList() result: ",
            util.inspect(fieldTypes, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        // Handle response
        fieldTypes = fieldTypes
            .filter(fieldType => +fieldType.field_type_id !== FieldModel.FIELD_TYPE.GDPR)
            .sort((f1, f2) => (+f1.field_type_id) - (+f2.field_type_id))
            .map(fieldType => ({
                "fieldTypeId": +fieldType['field_type_id'],
                "fieldTypeName": fieldType['field_type_name'],
                "properties": fieldType['properties'],
                "status": fieldType['status']
            }));

        return {
            ...result,
            data: {
                fieldTypes
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

const isFieldNameValid = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Create a Joi validator object
        // Validate rule:
        // * form_id is number and required
        // * field_name is string and required
        const validator = Joi.object({
            form_id: Joi.number().required(),
            field_name: Joi.string().required()
        }).unknown();

        // Validate request params
        if (validator.validate(params).error) {
            return {
                ...result,
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FieldModel.isFieldNameValid() params: ",
            util.inspect(params, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        let isFieldNameValid = await FieldModel.isFieldNameValid(params);

        Logger.trace("FieldModel.isFieldNameValid() result: ",
            util.inspect(isFieldNameValid, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        result.data = {is_valid: isFieldNameValid};

        // Continuously generate a unique field name
        let fieldName = params.field_name;

        if (!isFieldNameValid) {
            await new Promise((resolve, reject) => {
                const generateFieldNameRecursively = (() => {
                    let randomString = getRandomString(6);
                    fieldName = params.field_name + "_" + randomString;

                    Logger.trace("FieldModel.isFieldNameValid() params: ",
                        util.inspect({...params, field_name: fieldName}, {
                            breakLength: Infinity,
                            colors: true,
                            depth: Infinity,
                            showHidden: true,
                        }));

                    return FieldModel.isFieldNameValid({
                        ...params,
                        field_name: fieldName,
                    })
                        .then(res => {
                            Logger.trace("FieldModel.isFieldNameValid() result: ",
                                util.inspect(res, {
                                    breakLength: Infinity,
                                    colors: true,
                                    depth: Infinity,
                                    showHidden: true,
                                }));

                            if (!res) {
                                return generateFieldNameRecursively();
                            }

                            return resolve(fieldName)
                        })
                        .catch(error => {
                            if (error) {
                                Logger.error("FieldModel.isFieldNameValid() error: ",
                                    util.inspect(error.message, {
                                        breakLength: Infinity,
                                        colors: true,
                                        depth: Infinity,
                                        showHidden: true,
                                    }));

                                return reject(error);
                            }
                        })
                })

                return generateFieldNameRecursively();
            })
        }

        if (!result.data.is_valid) {
            result.data.suggested_name = fieldName;
        }

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
};

const testEs = async params => {
    let result = {
        code: 200,
        message: 'Success',
        data: {params}
    };

    try {

        const {
            formId = 0,
            networkId = 0,
            formName = "ngu",
            properties = {},
            formOptions = "ngu",
            shareLink = "ngu",
            versionId = 0,
            status = "draft",
            userId = 1,
            ctime = new Date().toISOString(),
            utime = new Date().toISOString(),
        } = params;

        let data = await FormElasticSearch.index({
            // "testId": 1,
            // "testName": "Nguyễn Văn B",
            // "properties": "{}",
            // "status": 1,
            // "ctime": "2020-09-25",
            // "utime": "2020-09-25",
            // "test": "testNewField"
            "formId": 0,
            "networkId": 0,
            "formName": "ngu",
            "properties": "{}",
            "formOptions": "ngu",
            "shareLink": "ngu",
            "versionId": 0,
            "status": "draft",
            "userId": 1,
            "ctime": new Date().toISOString(),
            "utime": new Date().toISOString(),
        }) || [];

        return {
            ...result,
            data: {
                data
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const testGetEs = async params => {
    let result = {
        code: 200,
        message: 'Success',
        data: {params}
    };

    try {

        let data = await FormElasticSearch.get({}) || [];

        return {
            ...result,
            data: {
                data
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getField = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Create a Joi validator object
        // Validate rule: id is a number and required
        const validator = Joi.object({
            id: Joi.number().required()
        }).unknown();

        // Validate request params
        if (validator.validate(params).error) {
            return {
                ...result,
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FieldModel.getField")

        const data = await FieldModel.getField(params) || [];

        if (!data.length) {
            return {
                code: 500,
                message: `Cannot get field ${params.id}`
            }
        }

        const field = data[0] || {};

        return {
            ...result,
            data: {
                field: {
                    field_id: +field.field_id,
                    form_id: +field.form_id,
                    internal_name: field.field_name,
                    label: field.field_label,
                    field_type_id: +field.field_type_id,
                    description: field.description,
                    is_required: Boolean(+field.is_required),
                    is_default: Boolean(+field.is_default),
                    status: +field.status,
                    ...field.properties,
                }
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
};

const getAllDefaultFields = async params => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {},
        };

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        let listDefaultFields = (await FieldModel.getAllDefaultFields(params)) || [];

        // Handle response
        listDefaultFields = listDefaultFields.map(field => ({
            field_id: +field["field_id"],
            internal_name: field["field_name"],
            label: field["field_label"],
            field_type_id: +field["field_type_id"],
            description: field["description"],
            is_required: Boolean(+field["is_required"]),
            is_default: Boolean(+field["is_default"]),
            ...field["properties"]
        }));

        return {
            ...result,
            data: {
                list_default_fields: listDefaultFields,
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
};

const updateField = async params => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {}
        };

        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * field_id is number and required
        // * label is string and required
        // * description is string, empty is allowed and optional
        // * is_required is boolean and required
        const validator = Joi.object({
            field_id: Joi.number().required(),
            label: Joi.string().allow("").required(),
            is_required: Joi.boolean().optional(),
        }).unknown();

        if (validator.validate(params).error) {
            return {
                code: 413,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid",
                )
            };
        }
        //endregion

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FieldModel.getField() params: ", util.inspect({networkId: params.networkId, id: params.field_id},
            {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }
        ));

        const fieldDetail = await FieldModel.getField({
            networkId: params.networkId,
            id: params.field_id,
        }).then(res => res && Array.isArray(res) ? res[0] : null);

        Logger.trace("FieldModel.getField() result", util.inspect(fieldDetail,
            {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }
        ));

        if (!fieldDetail) {
            return {code: 400, message: `Field ${params.field_id} is not exited`}
        }

        // Form dynamic properties

        /** @type {FieldProperties} */
        const properties = fieldDetail.properties || {};

        //region Form dynamic properties mapping

        if (params.alignment) {
            properties.alignment = params.alignment;
        }

        if (params.allow_multiple_files) {
            properties.allow_multiple_files = params.allow_multiple_files;
        }

        if (params.checked) {
            properties.checked = params.checked;
        }

        if (params.communication_consent) {
            properties.communication_consent = params.communication_consent;
        }

        if (params.consent_to_communicate) {
            properties.consent_to_communicate = params.consent_to_communicate;
        }

        if (params.consent_to_process) {
            properties.consent_to_process = params.consent_to_process;
        }

        if (params.default_value) {
            properties.default_value = params.default_value;
        }

        if (params.dropdown_options) {
            properties.dropdown_options = params.dropdown_options;
        }

        if (params.field_options) {
            properties.field_options = params.field_options;
        }

        if (params.file_types) {
            properties.file_types = params.file_types;
        }

        if (params.height) {
            properties.height = params.height;
        }

        if (params.help_text) {
            properties.help_text = params.help_text;
        }

        if (params.is_allow_multiple_files) {
            properties.is_allow_multiple_files = params.is_allow_multiple_files;
        }

        if (params.label_style) {
            properties.label_style = params.label_style;
        }

        if (params.max) {
            properties.max = params.max;
        }

        if (params.max_file_size) {
            properties.max_file_size = params.max_file_size;
        }

        if (params.min) {
            properties.min = params.min;
        }

        if (params.number_format) {
            properties.number_format = params.number_format;
        }

        if (params.place_holder) {
            properties.place_holder = params.place_holder;
        }

        if (params.privacy) {
            properties.privacy = params.privacy;
        }

        if (params.process_consent_text) {
            properties.process_consent_text = params.process_consent_text
        }

        if (params.radio_options) {
            properties.radio_options = params.radio_options;
        }

        if (params.required_message) {
            properties.required_message = params.required_message;
        }

        if (params.rich_text_editor) {
            properties.rich_text_editor = params.rich_text_editor;
        }

        if (params.set_number_limit) {
            properties.set_number_limit = params.set_number_limit;
        }

        if (params.width) {
            properties.width = params.width;
        }

        //endregion

        let destFilePath;
        switch (+params.field_type_id) {
            // Move the image from static/ to CDN/ if the field is image
            case FieldModel.FIELD_TYPE.IMAGE: {
                if (!params.image) {
                    return {code: 400, message: 'Field image is required'}
                }

                if (!compareObject(properties.image, params.image)) {
                    const fullSrcPath = constant.get("ROOT_PATH") + constant.get("STATIC_PATH") + params.image.path;
                    const fullDesPath = constant.get("UPLOAD_PATH");

                    Logger.trace("moveFile() params: ", util.inspect({fullSrcPath, fullDesPath},
                        {
                            breakLength: Infinity,
                            colors: true,
                            depth: Infinity,
                            showHidden: true,
                        }
                    ));

                    destFilePath = await moveFile(fullSrcPath, fullDesPath);

                    Logger.trace("moveFile() result: ", util.inspect(destFilePath, {colors: true}));

                    destFilePath = destFilePath.replace(constant.get("UPLOAD_PATH"), "");

                    properties.image = {
                        domain: constant.get("C0_DOMAIN"),
                        path: destFilePath,
                    };
                }

                break;
            }
            default:
                break;
        }

        const descriptionsList = [];
        if (params.description) {
            descriptionsList.push(params.description || "");
        }

        const fieldIds = [];
        fieldIds.push(params.field_id);

        const labelsList = [];
        labelsList.push(params.label);

        const propertiesList = [];
        propertiesList.push(properties);

        const statusesList = [];
        statusesList.push(FieldModel.STATUS.ACTIVE);

        Logger.trace("FieldModel.updateField() params: ", util.inspect(
            {
                ...params,
                description: descriptionsList,
                field_id: fieldIds,
                label: labelsList,
                properties: propertiesList,
                status: statusesList,
            },
            {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }
        ));

        const updateResult = await FieldModel.updateField({
            ...params,
            description: descriptionsList,
            field_id: fieldIds,
            label: labelsList,
            properties: propertiesList,
            status: statusesList,
        });

        Logger.trace("FieldModel.updateField() result: ", util.inspect(updateResult, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!updateResult || updateResult <= 0) {
            return {code: 500, message: 'Cannot update field'};
        }

        let data = {
            result: Boolean(updateResult)
        };

        if (destFilePath) {
            data.image = properties.image;
        }

        return {
            ...result,
            data
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"};
    }
};

module.exports = {
    createField,
    getAllYourFields,
    getFieldTypes,
    isFieldNameValid,
    testGetEs,
    testEs,
    getField,
    getAllDefaultFields,
    updateField,
};