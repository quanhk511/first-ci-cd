// Redis
const Redis = require("@library/Redis");

// Library
const Joi = require("@hapi/joi")
    .extend(require("@hapi/joi-date"));
const Logger = require("@library/Logger");
const Moment = require('moment-range')
    .extendMoment(require("moment"));
const SlackChannel = require("@library/Slack");

// Models
const ColumnModel = require("@library/Model/Column");
const FieldModel = require("@library/Model/Field");
const FormModel = require("@library/Model/Form");
const MetricModel = require("@library/Model/Metric");

const CommonElasticSearch = require('@library/ElasticSearch/Common');

// Constant
const Constant = require('@config/common/constant');

// ES
const FormElasticSearch = require("@library/ElasticSearch/Form");

// Utils
const util = require("util");
const {checkJSON, compareObject, handleError, getObjectPropSafely, toCapital, writeLog, moveFile, distributeRabbitMqMessage} = require("@library/Utils");

const constant = require('@config/common/constant');
const PATH = "@library/Business/Form.js";

const MAX_LIMIT = 100000;

const response = {
    SUCCESS: {
        name: "OK",
        code: 200,
    },
    SERVER_ERROR: {
        name: "Service Unavailable",
        code: 503,
    },
    CLIENT_ERROR: {
        name: "Bad request",
        code: 400,
    },
    NOT_FOUND: {
        name: 'Not found',
        code: 404,
    }
};

const getAllForm = async (params) => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {},
        };

        //region Validate params
        // Create a Joi validator object
        // Validate rule:
        // * format is a string either "chart" or "grid" and required
        // * form_id is a number, and required if objType is FORM_DETAIL
        // * from_date is a specific date and required
        // * to_date is a date after from date and required
        // * fields is a string and required if format is Chart
        // * limit is a positive non-zero integer, and required if objType is ALL_FORM
        // * page is a positive integer, and required if objType is ALL_FORM
        // * az is a string either "ASC" or "DESC", and required if objType is ALL_FORM
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validator = Joi.object({
            format: Joi.string()
                .allow(
                    MetricModel.FORMAT_TYPE.CHART,
                    MetricModel.FORMAT_TYPE.GRID,
                )
                .required(),
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            from_date: Joi.date().utc().format(["DD/MM/YYYY"]).required(),
            to_date: Joi.date().utc().format(["DD/MM/YYYY"]).min(Joi.ref("from_date")).required(),
            fields: Joi.when("format", {is: MetricModel.FORMAT_TYPE.CHART, then: Joi.string().required()}),
            limit: Joi.number().integer().greater(0).optional(),
            page: Joi.number().integer().positive().optional(),
            az: Joi.string()
                .allow(
                    MetricModel.SORT_TYPE.ASCENDING,
                    MetricModel.SORT_TYPE.DESCENDING,
                )
                .optional(),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validator.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    "Request invalid"
                ),
            };
        }
        //endregion

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        //region Get the Latest Modify Columns list
        const latestModifyColumns = await ColumnModel.getLatestModifyColumn({
            ...params,
            formId: params.form_id ? +params.form_id : null,
            type: +params.objType,
        })
            .then(res => res && Array.isArray(res) ? res[0] : {})
            .then(res => res["columns"] || [])
            .then(async modifyColumnsList => await Promise.all(modifyColumnsList
                // Get element at 0 since the getLatestModifyColumn method returns an array of 1 modifyColumn object
                // For each column from the modifyColumn object, we find the corresponding detail if that column is the metric;
                // otherwise, that column is a form metric (i.e. a field within form), return itself
                .map(async (column) => {
                    const detailMetric = await MetricModel.getDetailMetric({
                        // TODO new SP to call get list of detail metric
                        ...params,
                        metricCode: column["metricCode"],
                        type: +params.objType
                    }) || [];

                    return detailMetric.length ?
                        detailMetric[0] : // Same reason with modifyColumn object
                        {
                            metric_code: column["metricCode"],
                            metric_name: column["metricName"],
                            data_type: column["dataType"],
                            fix_column: column["fixColumn"],
                            is_performance: 0,
                        };
                })
            ));
        //endregion

        if (!latestModifyColumns || !latestModifyColumns.length) {
            return {code: 404, message: `Cannot get latest Modify Columns`};
        }

        // Response header (based on the Latest Modify Columns list)
        const header = latestModifyColumns.map((column) => ({
            name: column["metric_code"].toLowerCase(),
            label: column["metric_name"],
            sortable: Boolean(column["is_sort"]),
            editable: true,
            type: +column["data_type"],
        }));

        //region Format Request params

        // Build segments params attribute
        // segments is the string of list Modify Column Metrics with is_performance = 0, i.e. metrics cannot be calculated
        // each of which is separated by a semi-colon (if format is grid) or the value "SUBMITTED_DATE;FORM_ID" (if format is chart)
        let segments =
            params["format"] === MetricModel.FORMAT_TYPE.GRID
                ? latestModifyColumns
                    .filter((modifyColumn) => +modifyColumn["is_performance"] === 0)
                    .map((column) => column["metric_code"])
                    .filter(segment => Boolean(segment))
                    .join(";")
                : "SUBMITTED_DATE";

        // Form ID is needed to link from Listing all Form to Listing Form detail
        if (+params["objType"] === MetricModel.PAGE_TYPE.ALL_FORM && params["format"] === MetricModel.FORMAT_TYPE.GRID) {
            if (segments.indexOf("FORM_ID") <= -1) { // Prevent duplicate
                segments += Boolean(segments) ? ";FORM_ID" : "FORM_ID";
            }
        }

        // Build columns params attribute
        // columns is the string of list Modify Column Metrics with is_performance = 1, i.e. metrics can be calculated
        // each of which is separated by a semi-colon (if format is grid) or the list of string from params "fields"
        const columns =
            params["format"] === MetricModel.FORMAT_TYPE.GRID
                ? latestModifyColumns
                    .filter((modifyColumn) => +modifyColumn["is_performance"] === 1)
                    .map((column) => column["metric_code"])
                    .filter(segment => Boolean(segment))
                    .join(";")
                : params["fields"].replace(",", ";");

        // Build filter attribute
        let formId = (!isNaN(+params["form_id"])) ? [+params["form_id"]] : [];
        if (params.filter) {
            // Example filter by ID object: {"FORM_ID":{"matches_any":"44973,44983,44984"}}
            const filterById = JSON.parse(params.filter).find(f => f.hasOwnProperty("FORM_ID"));

            if (filterById) {
                // Get the filter expression
                const filterExpression = filterById["FORM_ID"];
                const filterValue = filterExpression["matches_any"];

                filterValue.split(",").map(v => +v).forEach(v => formId.push(v));
            }
        }

        //region Validate form status whether it's published or not (if a form is not published, it submission is not a valid data)

        if (+params.objType === MetricModel.PAGE_TYPE.FORM_DETAIL) {
            const formDetail = await FormModel.getFormDetail({
                formId: formId[0],
                networkId: params.networkId
            }).then(res => res[0]);

            if (!formDetail) {
                return {code: 400, message: `Form ${formId[0]} not existed`};
            }

            if (+formDetail.status !== FormModel.STATUS.PUBLISHED) {
                return {
                    ...result,
                    data: {
                        header,
                        body: [],
                        total: 0,
                    },
                }
            }
        }

        //endregion

        // Build filter_col and filter_val params attribute
        // A filter object is separated into 2 objects:
        // * filter_col is the string of Metric Codes to be filtered, each separated by a semi-colon
        // * filter_val is the string of operator and value, each separated by a semi-colon
        let filterCol = "",
            filterVal = "";

        // Mandatory filter: From Date, To Date
        const standardDateFmt = "DD-MM-YYYY"; // The date format from request
        const iso8601Fmt = "YYYY-MM-DD HH:mm:ss"; // The date time format accepted by DB

        // Parse params to Moment object
        const fromDate = Moment(params["from_date"], standardDateFmt);
        const toDate = Moment(params["to_date"], standardDateFmt);

        filterCol = "FROM_DATE;TO_DATE";
        filterVal =
            `${fromDate.startOf("day").format(iso8601Fmt)};` +
            `${toDate.endOf("day").format(iso8601Fmt)}`;

        // Format filter params
        if (params["filter"]) {
            // Example filer params:
            /* [{"FORM_NAME":{"contain":"a"}},{"SUBMISSION":{"greater_than_equals":"10"}},{"STATUS":{"matches_any":"22,72,80"}}] */

            const _filterCol = JSON.parse(params["filter"])
                .map((filter) => Object.keys(filter)[0].toString().toUpperCase())
                .reduce((accumulator, currentValue) => `${accumulator};${currentValue}`, "");

            const _filterVal = await Promise.all(
                JSON.parse(params["filter"])
                    .map((f) => Object.entries(f))
                    // Flat to reduce array level since Object.entries return an array of [key,value] => [[key,value], [key,value],...]
                    .reduce((acc, cur) => acc.concat(cur), [])
                    .map(async ([metricCode, operatorObject]) => {
                        // Example filter entry => [metricCode: "FORM_NAME": operatorObject {contain:"a"}]
                        Logger.trace("MetricModel.getDetailMetric() params: ", util.inspect(
                            {
                                ...params,
                                metricCode,
                                type: +params.objType,
                            },
                            {
                                breakLength: Infinity,
                                colors: true,
                                depth: Infinity,
                                showHidden: true,
                            }));

                        // For each Metric Code, we find its corresponding Metric
                        let metricDetail = await MetricModel.getDetailMetric({
                            ...params,
                            metricCode,
                            type: +params.objType,
                        }).then(res => res && Array.isArray(res) ? res[0] : null);

                        Logger.trace("MetricModel.getDetailMetric() result: ", util.inspect(metricDetail, {
                            breakLength: Infinity,
                            colors: true,
                            depth: Infinity,
                            showHidden: true,
                        }));

                        // If the Metric is not found, we continue to find the corresponding Fields (in case objType is 3)
                        if (!metricDetail && +params["objType"] === MetricModel.PAGE_TYPE.FORM_DETAIL) {
                            Logger.trace("FieldModel.getField() params: ", util.inspect(
                                {
                                    networkId: params["networkId"],
                                    id: metricCode
                                },
                                {
                                    breakLength: Infinity,
                                    colors: true,
                                    depth: Infinity,
                                    showHidden: true,
                                }));

                            metricDetail = await FieldModel.getField({
                                networkId: params["networkId"],
                                id: metricCode
                            }).then(res => res && Array.isArray(res) ? {
                                ...res[0],
                                data_type: MetricModel.DATA_TYPE.TEXT
                            } : null);


                            Logger.trace("FieldModel.getField() result: ", util.inspect(metricDetail, {
                                breakLength: Infinity,
                                colors: true,
                                depth: Infinity,
                                showHidden: true,
                            }));
                        }

                        // With the Operator object and the Data Type, we make the filterVal params
                        return metricDetail
                            ? makeFilterValParams(Object.entries(operatorObject)[0], +metricDetail["data_type"])
                            : "";
                    })
            ).then((values) => values.reduce((accumulator, currentValue) => `${accumulator};${currentValue}`, ""));

            filterCol += _filterCol;
            filterVal += _filterVal;
        }

        // Build sort params attribute (format is grid only), default sort type is CTIME ASC
        let sort;
        if (params["format"] === MetricModel.FORMAT_TYPE.GRID) {
            if (params["sort"] && params["az"]) {
                sort = `${params["sort"].toUpperCase()} ${params["az"]}`;
            } else {
                if (+params["objType"] === MetricModel.PAGE_TYPE.ALL_FORM) {
                    sort = `CTIME ${MetricModel.SORT_TYPE.DESCENDING}`;

                    if (segments.indexOf("CTIME") <= -1) { // Prevent duplicate
                        segments += Boolean(segments) ? ";CTIME" : "CTIME";
                    }
                }

                if (+params["objType"] === MetricModel.PAGE_TYPE.FORM_DETAIL) {
                    sort = `SUBMITTED_DATE ${MetricModel.SORT_TYPE.DESCENDING}`;
                }
            }
        }

        // Build offset params attribute (format is grid only)
        const {page = 1, limit = MAX_LIMIT} = params;
        const offset = params["format"] === MetricModel.FORMAT_TYPE.GRID
            ? (+page - 1) * +limit
            : null;

        //endregion

        const listForms = await FormModel.getAllForm({
            ...params,
            formId,
            segments,
            columns,
            filterCol,
            filterVal,
            sort,
            offset,
            type: params.objType,
        }) || [];

        //region Format and Return Response data

        if (!listForms) {
            return {code: 404, message: "Cannot get list of forms"};
        }

        // Response for format chart
        if (params["format"] === MetricModel.FORMAT_TYPE.CHART) {
            const rows = listForms.length
                ? Array.from(Moment.range(fromDate, toDate).by("day"))
                    .map(date => {
                        let formattedDate = date.format("YYYY-MM-DD");
                        let formData = listForms.find(form => form["submitted_date"] === formattedDate)

                        return {
                            time: date.format("YYYYMMDD"),
                            dimension: [],
                            metrics: params["fields"].split(",")
                                .map(field => field.toLowerCase())
                                .map(field => ({[field]: formData ? formData[field] : 0}))
                                .reduce((acc, cur) => ({...acc, [Object.keys(cur)[0]]: [+Object.values(cur)]}), {})
                        }
                    })
                : [];

            return {
                ...result,
                data: {
                    rows
                },
            };

        }
        // Response for format Grid
        else {
            const body = listForms.map(form => {
                // Reduce object attribute to get necessary fields (defined in the header name) only
                let res = header.reduce((acc, header) => {
                    let key = header.name;
                    let val = form[key];

                    switch (header.type) {
                        case MetricModel.DATA_TYPE.DATE_TIME:
                            val = Moment(val).format("MMM DD, YYYY HH:mm:ss");
                            break;
                        case MetricModel.DATA_TYPE.NUMBER:
                            val = +val;
                            break;
                        case MetricModel.DATA_TYPE.PERCENT:
                            val = +parseFloat(val).toFixed(2);
                            break;
                        case MetricModel.DATA_TYPE.FILE:
                            const listPaths = checkJSON(val) ? JSON.parse(val) : val;
                            val = listPaths.map(path => Constant.get("C0_DOMAIN") + path);
                            break;
                        case MetricModel.DATA_TYPE.LIST:
                            val = checkJSON(val) ? JSON.parse(val) : val;
                            break;
                    }

                    return {...acc, [key]: val};
                }, {});

                // Form ID is needed to create link between Listing all Form and Listing Form detail
                if (form.form_id) {
                    res.form_id = +form.form_id;
                }

                // Convert status from code to readable text
                if (form.status) {
                    res.status_id = +form.status;
                    res.status = toCapital(Object.keys(FormModel.STATUS)
                        [Object.values(FormModel.STATUS).findIndex(x => x === +form.status)]);
                }

                return res;
            });

            let summary = {};
            if (listForms.length) {
                for (let [key, val] of Object.entries(listForms[0])) {
                    if (!header.map(header => header.name).some(name => name === key)) {
                        summary[key.replace("total_", "")] = +parseFloat(val).toFixed(2);
                    }
                }
            }

            return {
                ...result,
                data: {
                    header,
                    body,
                    summary,
                    total: summary.row_count,
                },
            };
        }

        //endregion

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: error.message};
    }
};

const getAllGdprOptions = async (params) => {
    try {
        const result = {
            code: 200,
            message: "Success",
            data: {},
        };

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        let listGdprOptions = (await FormModel.getAllGdprOptions(params)) || [];

        // Handle response
        listGdprOptions = listGdprOptions.map(option => ({
            formGdprId: +option["form_gdpr_id"],
            formGdprName: option["form_gdpr_name"],
            description: option["description"],
            properties: option["properties"],
            status: +option["status"],
        }));

        return {
            ...result,
            data: {
                listGdprOptions,
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const insertNewFormToDBandES = async (params) => {
    // Logs file name
    const fileSuccess = 'Business_Insert_New_Form_To_DB_And_ES_Success';
    const fileError = 'Business_Insert_New_Form_To_DB_And_ES_Error';

    // Logs params
    let arrLog = {params: params};

    let result = {
        code: 200,
        message: "Success",
        data: {params},
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            networkId: Joi.number().required(),
            formName: Joi.string().required(),
            userId: Joi.number().required(),
            properties: Joi.any().required()
        }).unknown();

        if (validateParams.validate(params).error) {
            arrLog.error = getObjectPropSafely(
                () => validateParams.validate(params).error.details[0].message,
                "Request invalid",
            );

            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        const {
            changed = {},
            networkId = null,
            formName = null,
            properties = {},
            emailNotification = [],
            language = Constant.get("DEFAULT_LANGUAGE"),
            shareLink = null,
            status = FormModel.STATUS.DRAFT,
            userId = null,
            ctime = Moment().format("YYYY-MM-DD HH:mm:ss"),
        } = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const isFormNameExisted = await FormModel.isFormNameExisted({...params, form_name: formName});

        if (isFormNameExisted) {
            return {code: 400, message: `Form ${formName} is existed`};
        }

        // RETURNS numeric (0 / form_id)
        let formId = await FormModel.createNewDraftFormDB({
            networkId: networkId,
            formName: formName,
            properties: properties,
            formOptions: {
                emailNotification: emailNotification,
                language: language
            },
            shareLink: shareLink,
            versionId: 1,
            status: status,
            userId: userId,
        });

        if (!formId) {
            arrLog.error = "Error when creating Draft Form in DB";
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.SERVER_ERROR.code,
                message: response.SERVER_ERROR.name
            };
        }

        let data =
            (await FormElasticSearch.index({
                formId: +formId,
                networkId: +networkId,
                properties: JSON.stringify(properties),
                listChanged: JSON.stringify(changed),
                status: status,
                userId: userId,
                ctime: ctime,
            })) || [];

        if (!data && !data.length) {
            arrLog.error = "Cannot indexing form version to ElasticSearch";
            writeLog(fileError, arrLog);

            return {
                code: 500,
                message: "Cannot indexing form version to ElasticSearch"
            };
        }

        const updateResult = await FormModel.updateForm({
            networkId: networkId,
            formId: +formId,
            versionId: data.body._id,
        });

        if (!updateResult) {
            arrLog.params.versionId = data.body._id;
            arrLog.error = "Error when updating Draft Form in DB";
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.SERVER_ERROR.code,
                message: response.SERVER_ERROR.name
            };
        }

        let ids = [];
        ids.push(+formId);

        const rabbitMqArgs = {
            instanceName: "info_form",
            queueName: "data_sync",
            numberWorker: 6,
        };

        arrLog.rabbit_mq_message = {
            ...rabbitMqArgs,
            className: "DataSync",
            functionName: "bulkToES",
            msg: {
                objectName: "form",
                formId: ids,
                networkId: networkId
            }
        };

        await distributeRabbitMqMessage(rabbitMqArgs, "DataSync", "bulkToES", {
            objectName: "form",
            formId: ids,
            networkId: networkId
        });

        writeLog(fileSuccess, arrLog);

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                formId,
            },
        };
    } catch (error) {
        arrLog.error = error;
        writeLog(fileError, arrLog);

        const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
        await slack.sendMessage(
            `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when inserting a new Form to DB and ES
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(params,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                })}`
        );

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

// const getNewestDraftFormEs = async (params) => {
//     let result = {
//         code: 200,
//         message: "Success",
//         data: {params},
//     };
//
//     try {
//         let data = (await FormElasticSearch.get({})) || [];
//
//         return {
//             ...result,
//             data: {
//                 data,
//             },
//         };
//     } catch (error) {
//         handleError(error, {
//             path: PATH,
//             action: new Error().stack,
//             args: {
//                 params,
//             },
//         });
//     }
// };

const getAllESFormVersion = async (params) => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params},
    };

    try {
        const {
            id = null,
            networkId = null
        } = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const data = await FormModel.getFormDetail({
            formId: id,
            networkId: networkId,
        }) || [];

        if (!data || !data.length) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form ${id} is not exited`
            }
        }

        if (FormModel.STATUS.REMOVED === +data[0].status) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form ${id} had been removed`
            }
        }

        let dataES = await FormElasticSearch.getLastestFormVersion({
            form_id: id,
            page: 1,
            limit: 20,
            ignoredStatus: FormModel.STATUS.REMOVED_ES
        });

        if (Array.isArray(dataES.rows)) {
            for (let formVersion of dataES.rows) {
                // Rename response field: _id to form_version_id
                const form_version_id = formVersion._id
                delete formVersion._id;
                formVersion.form_version_id = form_version_id;
                // Rename response field: list_changed to changed
                formVersion.changed = formVersion.list_changed;
                delete formVersion.list_changed;
            }
        }

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                // ...data[0],
                ...dataES
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const updateDraftForm = async (params) => {
    // Log file name
    const fileSuccess = "Business_Update_Draft_Form_Success";
    const fileError = "Business_Update_Draft_Form_Error";

    // Log params
    let arrLog = {params};

    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params},
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            formId: Joi.number().required(),
            formName: Joi.string().optional(),
        }).unknown();

        if (validateParams.validate(params).error) {
            arrLog.error = getObjectPropSafely(
                () =>
                    validateParams.validate(params).error.details[0]
                        .message
            );
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        const {
            formId = null,
            networkId = null,
            formName = null,
            emailNotification = null,
            language = null,
            changed = null,
            shareLink = null,
            status = FormModel.STATUS.DRAFT,
            userId = null,
            ctime = Moment().format("YYYY-MM-DD HH:mm:ss"),
        } = params;
        let {properties = null} = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (formName) {
            const isFormNameExisted = await FormModel.isFormNameExisted({...params, form_name: formName});

            if (isFormNameExisted) {
                return {code: 400, message: `Form ${formName} is existed`};
            }
        }

        const data = await FormModel.getFormDetail({
            formId: formId,
            networkId: networkId,
        }) || [];

        Logger.trace("FormModel.getFormDetail() result: ", util.inspect(data, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!data.length) {
            arrLog.error = "Form is not existed";
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form is not existed`
            }
        }

        if (FormModel.STATUS.REMOVED === +data[0].status) {
            arrLog.error = "Form has been removed";
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form had been removed`
            }
        }

        let updateES = {};
        let updateDB = {};

        //region Check if the background image is present, then move the image from static to c0

        if (properties !== null) {
            //region Validate the background image properties

            const propertiesValidator = Joi.object({
                style: Joi.object({
                    background: Joi.object({
                        image: Joi.object({
                            domain: Joi.string()
                                .messages({"any.required": `"domain" is required`})
                                .required(),
                            path: Joi.string()
                                .messages({"any.required": `"path" is required`})
                                .required()
                        }).optional()
                    }).unknown().optional()
                }).unknown().optional()
            }).unknown();

            if (propertiesValidator.validate(properties).error) {
                arrLog.error = getObjectPropSafely(() => propertiesValidator.validate(params).error.details[0].message);
                writeLog(fileError, arrLog);

                return {
                    code: 400,
                    message: getObjectPropSafely(
                        () => propertiesValidator.validate(properties).error.details[0].message,
                        "Background image URL is required")
                };
            }

            //endregion

            const dataES = await FormElasticSearch.getLastestFormVersion({
                form_id: formId,
                page: 1,
                limit: 1,
                ignoredStatus: FormModel.STATUS.REMOVED_ES
            }) || {};

            Logger.trace("FormElasticSearch.getLastestFormVersion() result: ", util.inspect(dataES, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

            if (!dataES.rows) {
                return {code: 400, message: "Cannot get latest Form Version from ES"};
            }

            const currentProp = checkJSON(dataES.rows[0].properties) ? JSON.parse(dataES.rows[0].properties) : {};

            // Prevent calling on null or undefined
            const {style: {background: {image: currentBackgroundImage = null} = {}} = {}} = currentProp;
            const {style: {background: {image: paramsBackgroundImage = null} = {}} = {}} = properties;

            // Prevent background image hasn't been changed will cause moveFile() execute on a not existed path
            if (paramsBackgroundImage && compareObject(currentBackgroundImage, paramsBackgroundImage) === false) {
                let destFilePath;

                const fullSrcPath = constant.get("ROOT_PATH") + constant.get("STATIC_PATH") + paramsBackgroundImage.path;
                const fullDesPath = constant.get("UPLOAD_PATH");

                destFilePath = await moveFile(fullSrcPath, fullDesPath);
                destFilePath = destFilePath.replace(constant.get("UPLOAD_PATH"), "");

                properties.style.background.image = {
                    domain: constant.get("C0_DOMAIN"),
                    path: destFilePath,
                }
            }

            updateES.properties = JSON.stringify(properties);
        }

        //endregion

        if (changed !== null) {
            updateES.listChanged = JSON.stringify(changed);
        }

        if (formName !== null) {
            updateDB.formName = formName;
        }

        if (emailNotification !== null || language !== null) {
            updateDB.formOptions = {};
            if (emailNotification !== null) {
                updateDB.formOptions.emailNotification = emailNotification;
            }
            if (language !== null) {
                updateDB.formOptions.language = language;
            }
        }

        if (shareLink !== null) {
            updateDB.shareLink = shareLink;
        }

        let success = false;
        let resultES;
        let resultDB;

        if (Object.keys(updateES).length > 0) {
            updateES.formId = formId;
            updateES.networkId = networkId;
            updateES.userId = userId;
            updateES.status = status;
            updateES.ctime = ctime;

            resultES = await FormElasticSearch.index(updateES);
        }

        if (Object.keys(updateDB).length > 0) {
            updateDB.formId = formId;
            updateDB.networkId = networkId;
            updateDB.userId = userId;

            resultDB = await FormModel.updateForm(updateDB);
        }

        if (Object.keys(updateES).length > 0 && Object.keys(updateDB).length > 0) {
            if (resultES.body._id !== null && resultDB !== 0) {
                success = true;
            }
        }

        if (Object.keys(updateES).length > 0) {
            if (resultES.body._id !== null) {
                success = true;
            }
        }

        if (Object.keys(updateDB).length > 0) {
            if (resultDB !== 0) {
                success = true;
            }
        }

        if (success === false) {
            return {
                ...result,
                code: response.SERVER_ERROR.code,
                message: response.SERVER_ERROR.name,
            };
        }

        let ids = [];
        ids.push(+formId);

        const rabbitMqArgs = {
            instanceName: "info_form",
            queueName: "data_sync",
            numberWorker: 6,
        };
        await distributeRabbitMqMessage(rabbitMqArgs, "DataSync", "bulkToES", {
            objectName: "form",
            formId: ids,
            networkId: networkId
        });

        writeLog(fileSuccess, arrLog);

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                formId: formId,
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const publishForm = async (params) => {
    // Logs file name
    const fileSuccess = "Business_Publish_Form_Success";
    const fileError = "Business_Publish_Form_Error";

    // Logs params
    let arrLog = {params: params};

    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params},
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            formIds: Joi.array().items(Joi.number().required()),
        }).unknown();

        if (validateParams.validate(params).error) {
            arrLog.error = getObjectPropSafely(
                () => validateParams.validate(params).error.details[0].message,
                "Request invalid",
            );
            writeLog(fileError, arrLog);

            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            formIds = [],
            status = FormModel.STATUS.PUBLISHED,
            networkId
        } = params;

        let successIds = [];
        let failedIds = [];

        for (let id of formIds) {

            const formDetail = await FormModel.getFormDetail({
                formId: id,
                networkId: networkId,
            }).then(res => res && Array.isArray(res) ? res[0] : {});

            if (!formDetail || +FormModel.STATUS.REMOVED === +formDetail.status) {
                failedIds.push(id);

                continue;
            }

            let data = await FormElasticSearch.getLastestFormVersion({
                form_id: id,
                page: 1,
                limit: 1,
                ignoredStatus: FormModel.STATUS.REMOVED_ES
            });

            Logger.trace("FormElasticSearch.getLastestFormVersion() result: ", util.inspect(data, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

            if (Object.keys(data).length === 0) {
                // ES Die
                return {
                    ...result,
                    code: response.SERVER_ERROR.code,
                    message: "Cannot find form version on ES"
                }

            }

            const esUpdRs = await FormElasticSearch.updateStatusFormVersion({
                _id: data.rows[0]._id,
                status: FormModel.STATUS.PUBLISHED
            });

            if (!esUpdRs) {
                return {
                    ...result,
                    code: response.SERVER_ERROR.code,
                    message: "Cannot update form version to ES"
                }
            }

            const rs = await FormModel.updateForm({
                formId: id,
                properties: JSON.parse(data.rows[0].properties),
                versionId: data.rows[0]._id,
                status: status,
                networkId: networkId
            });

            if (!rs) {
                failedIds.push(id);

                continue;
            }

            successIds.push(id);
            const key = 'form:' + id;

            arrLog.redis_records = {
                instance: "caching",
                [key]: {
                    properties: data.rows[0].properties,
                    networkId,
                    status: FormModel.STATUS.PUBLISHED
                }
            };

            const redis = await Redis.getInstances('caching');

            if (!redis) {
                throw new Error("Cannot get Redis instance");
            }

            let redisResult = await redis.hset(key, 'properties', data.rows[0].properties);
            redisResult = await redis.hset(key, 'networkId', networkId).then(result => result && redisResult);
            redisResult = await redis.hset(key, 'status', FormModel.STATUS.PUBLISHED).then(result => result && redisResult);

            if (!redisResult) {
                failedIds.push(id);
            }
        }

        const rabbitMqArgs = {
            instanceName: "info_form",
            queueName: "data_sync",
            numberWorker: 6,
        };

        arrLog.rabbit_mq_message = {
            ...rabbitMqArgs,
            className: "DataSync",
            functionName: "bulkToES",
            msg: {
                objectName: "form",
                formId: successIds,
                networkId: networkId
            }
        };

        await distributeRabbitMqMessage(rabbitMqArgs, "DataSync", "bulkToES", {
            objectName: "form",
            formId: successIds,
            networkId: networkId
        });

        writeLog(fileSuccess, arrLog);

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                successIds: successIds,
                failedIds: failedIds,
            },
        };
    } catch (error) {
        arrLog.error = error;
        writeLog(fileError, error);

        const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
        await slack.sendMessage(
            `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when publishing Form(s)
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(params,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                })}`
        );


        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getPublishedFormDetail = async (params) => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params},
    };

    try {

        // Validate params
        const validateParams = Joi.object({
            id: Joi.number().required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            id = null,
            networkId = 1
        } = params;

        // Get data on Redis
        const key = "form:" + id;
        const redis = await Redis.getInstances("caching");
        if (!redis) {
            throw new Error("Cannot get Redis instance");
        }

        let data = await redis.hgetall(key);

        // Data not existed on Redis, get data from DB to response instead
        if (!data) {
            const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
            await slack.sendMessage(
                `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when getting a Form from Redis
                    Environment: ${process.env.APPLICATION_ENV}
                    Error: Data on Redis not found
                    Params: ${util.inspect({...params, redis_key: key},
                    {
                        breakLength: Infinity,
                        compact: true,
                        depth: Infinity,
                        showHidden: true
                    })}`);

            const dataDB = await FormModel.getFormDetail({
                formId: id,
                networkId: networkId
            }) || [];

            // Form not existed
            if (!dataDB.length) {
                return {
                    ...result,
                    code: response.NOT_FOUND.code,
                    message: `Form is not existed`
                }
            }

            // Form existed => Sync Redis
            data = {
                properties: dataDB[0].properties,
                status: dataDB[0].status
            };

            let redisResult = await redis.hset(key, 'properties', JSON.stringify(dataDB[0].properties)).then(res => res > 0);
            redisResult = await redis.hset(key, 'networkId', networkId).then(res => (res > 0) && redisResult);
            redisResult = await redis.hset(key, 'status', dataDB[0].status).then(res => (res > 0) && redisResult);

            if (!redisResult) {
                const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
                await slack.sendMessage(
                    `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when setting a Form to Redis
                        Environment: ${process.env.APPLICATION_ENV}
                        Error: Cannot update Form Detail to Redis
                        Params: ${util.inspect({...params, redis_key: key},
                        {
                            breakLength: Infinity,
                            compact: true,
                            depth: Infinity,
                            showHidden: true
                        })}`);
            }
        }

        // Form status must eq Publish
        if (FormModel.STATUS.PUBLISHED !== +data.status) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: response.NOT_FOUND.name
            };
        }

        return {
            ...result,
            code: response.SUCCESS.code,
            data
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const getFormDetail = async (params) => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params},
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            id: Joi.number().required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message:
                    getObjectPropSafely(
                        () =>
                            validateParams.validate(params).error.details[0]
                                .message
                    ) || `Error: ${response.CLIENT_ERROR.name}`,
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            id = null,
            networkId = null
        } = params;

        const data = await FormModel.getFormDetail({
            formId: id,
            networkId: networkId,
        }) || [];

        Logger.trace("FormModel.getFormDetail() result: ", util.inspect(data, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        if (!data.length) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form is not existed`
            }
        }

        if (FormModel.STATUS.REMOVED === +data[0].status) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form had been removed`
            }
        }

        let dataES = await FormElasticSearch.getLastestFormVersion({
            form_id: id,
            page: 1,
            limit: 1,
            ignoredStatus: FormModel.STATUS.REMOVED_ES
        });

        if (Object.keys(dataES).length === 0) {
            return {
                ...result,
                code: response.SERVER_ERROR.code,
                message: `Form versions (ES) are not found`
            }
        }

        const formOptions = checkJSON(data[0].form_options) ? JSON.parse(data[0].form_options) : data[0].form_options;
        delete data[0].form_options;

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                ...data[0],
                ...formOptions,
                properties: dataES.rows[0].properties,
                changed: dataES.rows[0].list_changed,
            },
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

const isFormNameExisted = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Create a Joi validator object
        // Validate rule:
        // * form_name is string and required
        const validator = Joi.object({form_name: Joi.string().required()}).unknown();

        // Validate request params
        if (validator.validate(params).error) {
            return {
                ...result,
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("FormModel.isFormNameExisted() params: ",
            util.inspect(params, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        const isFormNameExited = await FormModel.isFormNameExisted(params);

        Logger.trace("FormModel.isFormNameExisted() result: ",
            util.inspect(isFormNameExited, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        result.data = {is_form_existed: isFormNameExited};

        return result;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"};
    }
}

const removeForm = async params => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params}
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            formIds: Joi.array()
                .items(
                    Joi.number().required()
                ).required()
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message: getObjectPropSafely(() => validateParams.validate(params).error.details[0].message) || `Error: ${response.CLIENT_ERROR.name}`
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            color: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            formIds = [],
            status = FormModel.STATUS.REMOVED,
            networkId = null
        } = params;

        let successIds = [];
        let failedIds = [];

        const redis = await Redis.getInstances('caching');
        if (!redis) {
            throw new Error("Cannot get Redis instance");
        }

        for (let id of formIds) {
            const key = 'form:' + id;

            const rs = await FormModel.updateForm({
                formId: id,
                status: status,
                networkId: networkId
            }) || 0;

            // Form DB update failed
            if (!rs) {
                failedIds.push(id);

                continue;
            }

            // Form Redis update failed
            const redisResult = await redis.hset(key, 'status', FormModel.STATUS.REMOVED);
            if (!redisResult) {
                failedIds.push(id);

                continue;
            }

            successIds.push(id);
        }

        const rabbitMqArgs = {
            instanceName: "info_form",
            queueName: "data_sync",
            numberWorker: 6,
        };
        await distributeRabbitMqMessage(rabbitMqArgs, "DataSync", "bulkToES", {
            objectName: "form",
            formId: successIds,
            networkId: networkId
        });

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                successIds: successIds,
                failedIds: failedIds
            }
        };

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const revertFormVersion = async params => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params}
    };

    try {
        // Validate params
        const validateParams = Joi.object({
            form_id: Joi.number().required(),
            form_version_id: Joi.string().required()
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                ...result,
                code: response.CLIENT_ERROR.code,
                message: getObjectPropSafely(() => validateParams.validate(params).error.details[0].message) || `Error: ${response.CLIENT_ERROR.name}`
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true
        }));

        const {
            networkId = null,
            userId = null,
            changed = null,
            ctime = Moment().format("YYYY-MM-DD HH:mm:ss"),
        } = params;

        const formId = params.form_id;
        const formVersionId = params.form_version_id;
        const listChanged = changed;

        const dataDB = await FormModel.getFormDetail({
            formId: formId,
            networkId: networkId,
        }) || [];

        if (!dataDB.length) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form is not existed`
            }
        }

        if (FormModel.STATUS.REMOVED === +dataDB[0].status) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: `Form had been removed`
            }
        }

        let data = await FormElasticSearch.searchFormVersionById({
            _id: formVersionId
        });

        let newFormVersion = (await FormElasticSearch.index({
            formId: +formId,
            networkId: +networkId,
            properties: data.properties,
            listChanged: JSON.stringify(listChanged),
            status: FormModel.STATUS.DRAFT,
            userId: userId,
            ctime: ctime,
        })) || {};

        if (!newFormVersion) {
            throw new Error("Cannot indexing form to ElasticSearch");
        }

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                form_version_id: newFormVersion.body._id,
            }
        };

    } catch (error) {
        const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
        await slack.sendMessage(
            `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when reverting a Form version
                Environment: ${process.env.APPLICATION_ENV}
                Error: ${error.message}
                Params: ${util.inspect(params,
                {
                    breakLength: Infinity,
                    compact: true,
                    depth: Infinity,
                    showHidden: true
                })}`);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

// const removeUnpublishedChanges = async params => {
//     let result = {
//         code: response.SUCCESS.code,
//         message: response.SUCCESS.name,
//         data: {params}
//     };
//
//     try {
//         // Validate params
//         const validateParams = Joi.object({
//             id: Joi.number().required()
//         }).unknown();
//
//         if (validateParams.validate(params).error) {
//             return {
//                 ...result,
//                 code: response.CLIENT_ERROR.code,
//                 message: getObjectPropSafely(() => validateParams.validate(params).error.details[0].message) || `Error: ${response.CLIENT_ERROR.name}`
//             };
//         }
//
//         Logger.debug("Params: ", util.inspect(params, {
//             breakLength: Infinity,
//             colors: true,
//             depth: Infinity,
//             showHidden: true
//         }));
//
//         const {
//             id = null,
//             status = FormModel.STATUS.PUBLISHED
//         } = params;
//
//         let data = await FormElasticSearch.getLastestPublishedFormVersion({
//             form_id: id,
//             status: status
//         });
//
//         if (!data || !Array.isArray(data.rows)) {
//             return {
//                 ...result,
//                 code: response.NOT_FOUND.code,
//                 message: "No published form version is found"
//             };
//         }
//
//         let unpublishedChangesFormVersion = await FormElasticSearch.getDraftsAfterLatestPublishedFormVersion({
//             form_id: id,
//             ctime: data.rows[0].ctime,
//             status: FormModel.STATUS.DRAFT
//         })
//
//         if (unpublishedChangesFormVersion && Array.isArray(unpublishedChangesFormVersion.rows)) {
//             for (let formVersion of unpublishedChangesFormVersion.rows) {
//                 FormElasticSearch.updateStatusFormVersion({
//                     _id: formVersion._id,
//                     status: FormModel.STATUS.REMOVED_ES
//                 })
//             }
//         }
//
//         return {
//             ...result,
//             code: response.SUCCESS.code
//         };
//
//     } catch (error) {
//         handleError(error, {
//             path: PATH,
//             action: new Error().stack,
//             args: {
//                 params
//             }
//         });
//     }
// };

const searchForm = async params => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {params}
    };

    try {
        const {
            search = "",
            networkId,
            userId
        } = params;

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true
        }));

        let data = await FormElasticSearch.searchForm({
            search: search,
            networkId,
            userId
        });

        if (!data || !data.rows) {
            return {
                ...result,
                code: response.NOT_FOUND.code,
                message: "Not found"
            };
        }

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                data
            }
        };

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getAllFormStatus = async params => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name,
        data: {}
    };

    try {
        const listStatuses = Object.entries(FormModel.STATUS)
            .map(([key, value]) => ({
                id: value,
                name: key
            }))
            .filter(x => x.id !== FormModel.STATUS.REMOVED_ES);

        return {
            ...result,
            code: response.SUCCESS.code,
            data: {
                list_statuses: listStatuses
            }
        };

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const recoverFormVersion = async (formIds) => {
    let result = {
        code: response.SUCCESS.code,
        message: response.SUCCESS.name
    };

    let successIds = [];
    let failedIds = [];

    try {
        const ids = formIds || [];

        for (let formId of ids) {

            const dataDB = await FormModel.getFormDetail({
                formId: formId,
                networkId: 1
            });

            // Form not existed
            if (dataDB.length === 0) {
                failedIds.push(formId);
                continue;
            }

            let data =
                (await FormElasticSearch.index({
                    formId: +formId,
                    networkId: +dataDB[0].network_id,
                    properties: dataDB[0].properties,
                    listChanged: "{}",
                    status: +dataDB[0].status,
                    userId: +dataDB[0].user_id,
                    ctime: Moment().format("YYYY-MM-DD HH:mm:ss"),
                })) || [];

            let rs = await FormModel.updateForm({
                networkId: +dataDB[0].network_id,
                formId: +formId,
                versionId: data.body._id,
            });

            let syncId = [];
            syncId.push(formId);

            await CommonElasticSearch.bulkData('form', {
                formId: syncId,
                networkId: +dataDB[0].network_id,
                isMapping: false
            });

            successIds.push(+formId);
        }

        return {
            ...result,
            code: response.SUCCESS.code,
            success_ids: successIds,
            failed_ids: failedIds
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

module.exports = {
    getAllForm,
    getAllGdprOptions,
    insertNewFormToDBandES,
    // getNewestDraftFormEs,
    getAllESFormVersion,
    updateDraftForm,
    publishForm,
    getPublishedFormDetail,
    getFormDetail,
    removeForm,
    revertFormVersion,
    searchForm,
    getAllFormStatus,
    recoverFormVersion,
    isFormNameExisted,
};

/* Private Business methods (do not exclude) */

/**
 * Convert a filter operator object from API request params into a filter value params.
 *
 * A operator entry is a key-value object with an operator as a key and one corresponding value.
 * A filter value params is a string as following format: "${operator} ${value}"
 *
 * @param operatorEntry The operator entry [key-value] from API request params
 * @param dataType The metric data type (String/Text, Number, etc.)
 *
 * @example
 * - operatorEntry: ["contain":"a"]
 * - dataType: 1 (i.e. String/Text)
 */
const makeFilterValParams = (operatorEntry, dataType) => {
    Logger.debug("Params: ", util.inspect(
        {operatorEntry, dataType},
        {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

    const operator = operatorEntry[0];
    const value = operatorEntry[1];

    const standardDateFmt = "DD-MM-YYYY"; // The date format from request
    const iso8601Fmt = "YYYY-MM-DD"; // The date format accepted by DB

    switch (operator) {
        case "equals":
        case "is":
        case "on":
            if (+dataType === MetricModel.DATA_TYPE.DATE_TIME) {
                return `''${Moment(value, standardDateFmt).format(iso8601Fmt)}''`;
            }

            if (+dataType === MetricModel.DATA_TYPE.TEXT) {
                return `= ''${value}''`;
            }

            return `= ${value}`;
        case "after":
            if (+dataType === MetricModel.DATA_TYPE.DATE_TIME) {
                return `> ''${Moment(value, standardDateFmt).format(iso8601Fmt)}''`;
            }

            return `> ${value}`;
        case "greater_than_equals":
            if (+dataType === MetricModel.DATA_TYPE.DATE_TIME) {
                return `>= ''${Moment(value, standardDateFmt).format(iso8601Fmt)}''`;
            }

            return `>= ${value}`;
        case "before":
            if (+dataType === MetricModel.DATA_TYPE.DATE_TIME) {
                return `< ''${Moment(value, standardDateFmt).format(iso8601Fmt)}''`;
            }

            return `< ${value}`;
        case "less_than_equals":
            if (+dataType === MetricModel.DATA_TYPE.DATE_TIME) {
                return `<= ''${Moment(value, standardDateFmt).format(iso8601Fmt)}''`;
            }

            return `<= ${value}`;
        case "start_with":
            return `ILIKE ''${value}%''`;
        case "contain":
            return `ILIKE ''%${value}%''`;
        case "does_not_contain":
            return `NOT ILIKE ''%${value}%''`;
        case "matches_any": {
            let _ = value; // Temp variable to process

            if (+dataType === MetricModel.DATA_TYPE.TEXT) {
                _ = value
                    .split(",")
                    .map((x) => `''${x.trim()}''`)
                    .join();
            }

            return `IN (${_})`;
        }
        default:
            return null;
    }
};
