// Library
const Joi = require('@hapi/joi');
const Logger = require("@library/Logger");

// Model
const ColumnModel = require('@library/Model/Column');
const MetricModel = require("@library/Model/Metric");

// Utils
const util = require("util");
const {handleError, getObjectPropSafely, snakeToCamel} = require('@library/Utils');

const PATH = '@library/Business/Column.js';

const getListingModifyColumn = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            userId = '',
            networkId = '',
            objType = '',
            form_id: formId,
            columns = 'MODIFY_COL_ID, MODIFY_NAME, COLUMNS, IS_LASTED, PRE_DEFINE',
            order = 'MODIFY_COL_ID DESC'
        } = params;

        let hasLatest = false;
        let preDefine = null;

        Logger.trace("ColumnModel.getListingModifyColumn() params: ",
            util.inspect(
                {userId: +userId, networkId: +networkId, formId, type: +objType, columns, order},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        // Get list modify column
        let modifyColumns = await ColumnModel.getListingModifyColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            type: +objType,
            columns,
            order
        }) || [];

        Logger.trace("ColumnModel.getListingModifyColumn() result: ",
            util.inspect(modifyColumns, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!modifyColumns.length) {
            return {
                ...result,
                data: {
                    modifyColumns
                }
            };
        }

        // Handle response
        modifyColumns = modifyColumns.map(modifyItem => {
            Logger.trace("modifyColumns.map -> input modifyItem: ",
                util.inspect(modifyItem, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

            modifyItem = Object.keys(modifyItem).reduce((initModifyObj, modifyItemKey) => {
                const formatModifyItemKey = snakeToCamel(modifyItemKey);

                switch (formatModifyItemKey) {
                    case 'columns': {
                        if (!initModifyObj[formatModifyItemKey]) {
                            initModifyObj[formatModifyItemKey] = [];
                        }

                        modifyItem[modifyItemKey].map(column => {
                            let newColumns = {};

                            newColumns['metricName'] = column['metric_name'] || column['metricName'] || '';
                            newColumns['metricCode'] = column['metric_code'] || column['metricCode'] || '';
                            newColumns['fixColumn'] = column['fix_column'] || column['fixColumn'] || ColumnModel.COLUMN_NOT_FIXED;
                            newColumns['dataType'] = column['data_type'] || column['dataType'] || '';

                            initModifyObj[formatModifyItemKey].push(newColumns);
                        });

                        break;
                    }
                    default: {
                        initModifyObj[formatModifyItemKey] = modifyItem[modifyItemKey];
                        break;
                    }
                }

                return initModifyObj;
            }, {});

            if (!preDefine) {
                preDefine = modifyItem;
            }

            if (+modifyItem.isLasted === 1) {
                hasLatest = true;
            }

            Logger.trace("modifyColumns.map -> return modifyItem: ",
                util.inspect(modifyItem, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

            return modifyItem;
        });

        if (!hasLatest && preDefine && preDefine.columns) {
            // New user, create a modify column
            await addModifyColumn({
                networkId,
                userId,
                form_id: formId,
                columns: JSON.stringify(preDefine.columns),
                name: 'Custom',
                objType
            });

            // Re-get the list
            return getListingModifyColumn(params);
        } else {
            // remove preDefine = 1
            modifyColumns = modifyColumns.filter(item => +item["preDefine"] === 0);
        }

        return {
            ...result,
            data: {
                modifyColumns
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const getLatestModifyColumn = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true
        }));

        const {
            userId = '',
            networkId = '',
            form_id: formId,
            objType = ''
        } = params;

        Logger.trace("ColumnModel.getLatestModifyColumn() params: ",
            util.inspect(
                {userId: +userId, networkId: +networkId, formId, type: +objType},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        // Get latest modify column
        let modifyColumn = await ColumnModel.getLatestModifyColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            type: +objType
        }) || [];

        Logger.trace("ColumnModel.getLatestModifyColumn() result: ",
            util.inspect(
                {userId: +userId, networkId: +networkId, formId, type: +objType},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        if (!modifyColumn.length) {
            return {
                ...result,
                data: {
                    modifyColumn
                }
            };
        }

        // Handle response
        modifyColumn = modifyColumn.map(modifyItem => {
            Logger.trace("modifyColumns.map -> input modifyItem: ",
                util.inspect(modifyItem, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

            modifyItem = Object.keys(modifyItem).reduce((initModifyObj, modifyItemKey) => {
                const formatModifyItemKey = snakeToCamel(modifyItemKey);

                switch (formatModifyItemKey) {
                    case 'columns': {
                        initModifyObj[formatModifyItemKey] = modifyItem[modifyItemKey].map(column => {
                            column['metricId'] = column['metricId'] || '';
                            column['metricName'] = column['metricName'] || '';
                            column['metricCode'] = column['metricCode'] || '';
                            column['metricLevel'] = column['metricLevel'] || '';
                            column['levelPosition'] = column['levelPosition'] || '';
                            column['formula'] = column['formula'] || '';
                            column['parentId'] = column['parentId'] || '';
                            column['fixColumn'] = column['fixColumn'] || ColumnModel.COLUMN_NOT_FIXED;
                            column['isSort'] = column['is_sort'] || 0;
                            column['dataType'] = column['dataType'] || '';
                            column['isPerformance'] = column['isPerformance'] || 0;

                            return column;
                        });
                        break;
                    }
                    default: {
                        initModifyObj[formatModifyItemKey] = modifyItem[modifyItemKey];
                        break;
                    }
                }

                return initModifyObj;
            }, {});

            Logger.trace("modifyColumns.map -> return modifyItem: ",
                util.inspect(modifyItem, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

            return modifyItem;
        });

        return {
            ...result,
            data: {
                modifyColumn
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const addModifyColumn = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * columns is a json string and required
        // * name is a string and required
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            columns: Joi.string().required(),
            name: Joi.string().required(),
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const {
            userId = '',
            networkId = '',
            columns = '',
            name = '',
            form_id: formId = null,
            objType = null
        } = params;

        Logger.trace("ColumnModel.checkNameColumn() params: ",
            util.inspect({userId: +userId, networkId: +networkId, formId, modifyName: name, type: +objType},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        const columnInfo = await ColumnModel.checkNameColumn({
            userId: +userId,
            networkId: +networkId,
            formId,
            modifyName: name,
            type: +objType
        })
            .then(res => res && Array.isArray(res) ? res[0] : null);

        Logger.trace("ColumnModel.checkNameColumn() result: ",
            util.inspect(columnInfo, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (columnInfo) {
            return {code: 400, message: `Modify Column name ${params.name} is existed`}
        }

        Logger.trace("ColumnModel.addModifyColumn() params: ",
            util.inspect(
                {
                    userId: +userId,
                    networkId: +networkId,
                    modifyName: name,
                    columns,
                    formId,
                    type: +objType,
                    status: ColumnModel.MODIFY_COLUMN_ACTIVE,
                    isLasted: ColumnModel.MODIFY_COLUMN_LASTED,
                },
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }
            ));

        const addModifyColumn = await ColumnModel.addModifyColumn({
            userId: +userId,
            networkId: +networkId,
            modifyName: name,
            columns,
            formId,
            type: +objType,
            status: ColumnModel.MODIFY_COLUMN_ACTIVE,
            isLasted: ColumnModel.MODIFY_COLUMN_LASTED,
        });

        Logger.trace("ColumnModel.addModifyColumn() result: ",
            util.inspect(addModifyColumn, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!addModifyColumn) {
            return {
                code: 200,
                message: 'Cannot add modify column',
                data: {
                    status: 0
                }
            };
        }

        return {
            ...result,
            data: {
                status: 1
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const updateModifyColumn = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * id is a number and required
        // * columns is a json string and required
        // * modifyName is a string and required
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            id: Joi.number().required(),
            columns: Joi.string().required(),
            modifyName: Joi.string().required(),
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("ColumnModel.updateModifyColumn() params: ",
            util.inspect(params, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        const updateModifyColumn = await ColumnModel.updateModifyColumn(params);

        Logger.trace("ColumnModel.updateModifyColumn() result: ",
            util.inspect(updateModifyColumn, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!updateModifyColumn) {
            return {
                code: 200,
                message: 'Cannot update modify column',
                data: {
                    status: 0
                }
            };
        }

        return {
            ...result,
            data: {
                status: 1
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const deleteModifyColumn = async params => {
    try {
        // Validate params
        // Validate rule:
        // * id is a number and required
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            id: Joi.number().required(),
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("ColumnModel.getLatestModifyColumn() params: ",
            util.inspect({...params, formId: params.form_id, type: params.objType},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        // Validate params, if the deleting id is a latest Modify Column, return invalid
        const latestModifyColumn = await ColumnModel.getLatestModifyColumn({
            ...params,
            formId: params.form_id,
            type: params.objType
        })
            .then(res => res && Array.isArray(res) ? res[0] : null);

        Logger.trace("ColumnModel.getLatestModifyColumn() result: ",
            util.inspect(latestModifyColumn, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (latestModifyColumn && latestModifyColumn["modify_col_id"] === params.id) {
            return {
                code: 400,
                message: `Cannot delete Modify Column ${params.id}. It is a latest Modify Column`
            }
        }

        Logger.trace("ColumnModel.updateModifyColumn() params: ",
            util.inspect({...params, isLasted: 0, status: 0},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        const deleteResult = await ColumnModel.updateModifyColumn({
            ...params,
            isLasted: 0,
            status: 0
        });

        Logger.trace("ColumnModel.updateModifyColumn() result: ",
            util.inspect(deleteResult,
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        return {
            code: 200,
            data: {
                status: +deleteResult
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

const isModifyColumnNameValid = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * name is a string and required
        // * form_id is a number and required if objType is FORM_DETAIL
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validator = Joi.object({
            name: Joi.string().required(),
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        // Validate request params
        if (validator.validate(params).error) {
            return {
                ...result,
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        Logger.trace("ColumnModel.checkNameColumn() params: ",
            util.inspect({...params, modifyName: params.name, formId: params.form_id, type: params.objType},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        const isFieldNameValid = await ColumnModel.checkNameColumn({
            ...params,
            modifyName: params.name,
            formId: params.form_id,
            type: params.objType
        })
            .then(res => res && Array.isArray(res) ? res[0] : null);

        Logger.trace("ColumnModel.checkNameColumn() result: ",
            util.inspect(isFieldNameValid, {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

        return {
            ...result,
            data: {is_valid: !Boolean(isFieldNameValid)}
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {code: 500, message: "Internal server error"}
    }
};

module.exports = {
    getListingModifyColumn,
    getLatestModifyColumn,
    addModifyColumn,
    updateModifyColumn,
    deleteModifyColumn,
    isModifyColumnNameValid
};
