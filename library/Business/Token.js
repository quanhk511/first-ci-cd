const md5 = require('md5');
const Joi = require('@hapi/joi');
const axios = require('axios');
const querystring = require('querystring');

// Utils
const Constant = require('@config/common/constant');
const {handleError} = require('@library/Utils');

const PATH = '@library/Business/Token.js';

const validate = async (params) => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        if (!params._token) {
            return {
                ...result,
                message: 'Token not found'
            };
        }

        let tokenInfo = {
            valid: true,
            logout: false,
            network_id: '',
            manager_id: '',
            allow_access: true,
            token_expire: false
        };

        const token = params._token;

        const AuthApiHost = Constant.get('AUTH_DOMAIN');

        const data = await axios({
            url: `http${process.env.APPLICATION_ENV === "development" ? "" : "s"}:${AuthApiHost}/api/token/validate`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + process.env.SERVER_KEY
            },
            data: {
                token: token
            }
        }).catch(() => ({}));

        const resultAPI = (data.data && data.data.data) || {};

        const getTokenInfo = resultAPI && resultAPI.data;

        if (!getTokenInfo) {
            return {
                ...result,
                data: {
                    tokenInfo: {
                        ...tokenInfo,
                        valid: false,
                        token_expire: true,
                    }
                }
            };
        }

        // if (!getTokenInfo.g2fa || +getTokenInfo.g2fa === 1 || !getTokenInfo.role) {
        //     return {
        //         ...result,
        //         data: {
        //             tokenInfo
        //         }
        //     };
        // }

        if (getTokenInfo['access_level']) {
            let roles = getTokenInfo['access_level'].split('|').map(role => +role);

            if (roles.length && roles.indexOf(Constant.get('TOKEN_ROLE')) >= 0) {
                if (roles.indexOf(Constant.get('TOKEN_ROLE_ADMIN')) >= 0) {
                    result['allow_access'] = true;
                }
            }
        }

        tokenInfo.manager_id = getTokenInfo.user_id;
        tokenInfo.network_id = getTokenInfo.network_id;
        tokenInfo.role = getTokenInfo.role;

        return {
            ...result,
            data: {
                tokenInfo
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getToken = async (params) => {
    try {
        const tokenCheckParams = Joi.object({
            user_name: Joi.string().required(),
            app_id: Joi.string().required(),
            p_id: Joi.string().required()
        }).unknown(true);

        const validate = tokenCheckParams.validate();

        if (!validate.error) {
            let OGS_SANDBOX_DOMAIN = Constant.get('AUTH_ADX_DOMAIN');
            let OGS_AUTHEN;
            if (process.env.APPLICATION_ENV === 'localhost') {
                OGS_AUTHEN = 'http://tuandv.ogs.adxdev.vn/api/account/3rd/authenticate';
            } else {
                OGS_AUTHEN = 'http:' + OGS_SANDBOX_DOMAIN + 'api/login-external/account/3rd/authenticate';
            }

            const {data} = await axios.post(OGS_AUTHEN, querystring.encode({
                user_name: params.user_name,
                app_id: params.app_id,
                p_id: params.p_id
            }), {
                headers: {
                    Authorization: 'Basic YW50c2RldnYzOl0qPS1gN2VqOFBIazclZjM',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

            if (data && data.data) {
                return data.data;
            }
        }
        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getUserInfo = async (params) => {
    try {
        const token = params._token;
        const typeToken = 'user';
        const redis = await Redis.getInstances('caching');

        const keyToken = Constant.get('JOB_PREFIX_BUYER') + 'token:' + md5(token) + ':' + typeToken;
        return await redis.hgetall(keyToken);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    validate,
    getToken,
    getUserInfo
};

