// Library
const Joi = require('@hapi/joi');

const {handleError, getObjectPropSafely, snakeToCamel} = require('@library/Utils');

const PATH = '@library/Business/Menu.js';

const getListMenu = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Create a Joi validator object
        // Validate rule: field_name is string and required
        const validator = Joi.object({
            module: Joi.number().required()
        }).unknown();

        // Validate request params
        if (validator.validate(params).error) {
            return {
                ...result,
                code: 400,
                message: getObjectPropSafely(
                    () => validator.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        let listMenu = {
            headers: [
                {
                    menu_id: 1,
                    menu_parent: null,
                    menu_name: 'Form',
                    menu_order: 1,
                    menu_pos: 1,
                    is_public: 1,
                    icon_class: null,
                    module: 5,
                    path: "/:userId/form",
                    state: "form",
                }
            ],
            lefts: {
                form: [{
                    menu_id: 2,
                    menu_parent: 1,
                    menu_name: 'Form',
                    menu_order: 1,
                    menu_pos: 2,
                    is_public: 1,
                    icon_class: 'icon-sprite-forms',
                    module: 5,
                    path: "/:userId/form",
                    state: "form",
                }]
            }
        };

        return {
            ...result,
            data: {
                ...listMenu
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
    }
};

module.exports = {
    getListMenu
};