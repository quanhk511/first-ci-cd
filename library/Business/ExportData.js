// Library
const Excel = require("@library/Excel");
const Logger = require("@library/Logger");

// Business
const FormBusiness = require("@library/Business/Form");

// Utils
const util = require("util");
const {handleError, prepareFile} = require("@library/Utils");

const PATH = "@library/Business/ExportData.js";

const EXPORT_DIR = "static/export/";

const exportData = async params => {
    try {
        Logger.debug("Params: ", util.inspect(params, {
            breakLength: Infinity,
            colors: true,
            depth: Infinity,
            showHidden: true,
        }));

        const result = await FormBusiness.getAllForm(params);

        Logger.trace("FormBusiness.getAllForm() result: ",
            util.inspect(result, {
                breakLength: Infinity,
                colors: true,
                depth: Infinity,
                showHidden: true,
            }));

        if (!result || result.code !== 200) {
            handleError(new Error(result.message), {
                action: 'exportData',
                path: PATH,
                args: params
            });

            return {error: result.message};
        }

        const listAllForms = result.data;
        if (listAllForms && Array.isArray(listAllForms.body)) {
            const data = listAllForms.body.map(item => {
                for (let prop in item) {
                    if (item.hasOwnProperty(prop) && Array.isArray(item[prop])) {
                        item[prop] = item[prop].join(";")
                    }
                }

                return item;
            });
            const header = listAllForms.header.map(h => ({
                key: h.name,
                label: h.label,
                width: h.label.length,
            }));

            const {fullPath, fileName} = await prepareFile(EXPORT_DIR, 'export_form', {dateTimeSuffix: true});

            Logger.trace("Excel.writeFileSync() params: ", util.inspect({header, data, fullPath},
                {
                    breakLength: Infinity,
                    colors: true,
                    depth: Infinity,
                    showHidden: true,
                }));

            const writeFile = Excel.writeFileSync(header, data, fullPath);

            if (writeFile && writeFile.error) {
                return {
                    error: writeFile.error
                };
            }

            return {fullPath, fileName};
        } else {
            handleError(new Error('Can not get list of forms'), {
                action: 'exportData',
                path: PATH,
                args: params
            });

            return {error: 'Can not get list of forms'};
        }

    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });

        return {error: error.message};
    }
};

module.exports = {
    exportData,
};