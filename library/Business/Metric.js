// Libraries
const Joi = require('@hapi/joi');

// Model
const ColumnModel = require("@library/Model/Column");
const FieldModel = require("@library/Model/Field");
const MetricModel = require('@library/Model/Metric');

// Utils
const {handleError, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/Business/Metric.js';

const getDetailMetric = async params => {
    try {
        const result = {
            code: 200,
            message: 'Success',
            data: {}
        };

        // Validate params
        // Validate rule:
        // * form_id is a number and required if objType is FORM_DETAIL
        // * type is a string either CHART, or GRID and required
        // * objType is a number either ALL_FORM, or FORM_DETAIL and required
        const validateParams = Joi.object({
            form_id: Joi.when("objType", {is: MetricModel.PAGE_TYPE.FORM_DETAIL, then: Joi.number().required()}),
            type: Joi.string()
                .allow(
                    MetricModel.FORMAT_TYPE.CHART,
                    MetricModel.FORMAT_TYPE.GRID,
                )
                .required(),
            objType: Joi.number()
                .allow(
                    MetricModel.PAGE_TYPE.ALL_FORM,
                    MetricModel.PAGE_TYPE.FORM_DETAIL,
                )
                .required(),
        }).unknown();

        if (validateParams.validate(params).error) {
            return {
                code: 400,
                message: getObjectPropSafely(
                    () => validateParams.validate(params).error.details[0].message,
                    'Request invalid'
                )
            };
        }

        // Get detail metric
        const {
            userId = '',
            networkId = '',
            objType = '',
            type = ""
        } = params;

        let metrics = [];
        let groupMetricByParent = {};

        // Get metric
        const getMetric = await MetricModel.getListing({
            userId: +userId,
            networkId: +networkId,
            type: +objType,
            byFnc: type.toUpperCase(),
            columns: 'METRIC_ID, METRIC_NAME, METRIC_CODE, METRIC_LEVEL, LEVEL_POSITION, OPERATOR, FORMULA, PARENT_ID, STATUS, FIX_COLUMN, IS_SORT, DATA_TYPE, IS_PERFORMANCE',
            order: 'FIX_COLUMN DESC, LEVEL_POSITION',
        });

        getMetric.length && getMetric.map(metric => {
            const {
                metric_id: metricId = '',
                metric_name: metricName = '',
                metric_code: metricCode = '',
                metric_level: metricLevel = '',
                parent_id: parentId = '',
                fix_column: fixColumn = '',
                data_type: dataType = ''
            } = metric;

            let metricField = {
                metricName,
                metricCode: metricCode || metricId,
                fixColumn: +fixColumn,
                dataType: +dataType
            };

            metrics.push({
                metricId,
                ...metricField
            });

            // Filter metric
            if (typeof parentId === 'string' && parentId !== '0') {
                // Group metric by parent id
                if (!groupMetricByParent[parentId]) {
                    groupMetricByParent[parentId] = [];
                }

                groupMetricByParent[parentId].push(metricField);
            }
        });

        metrics.map(metric => {
            const {metricId = ''} = metric;

            if (groupMetricByParent[metricId]) {
                metric.child = groupMetricByParent[metricId];
            }

            return metric;
        });

        if (type === MetricModel.FORMAT_TYPE.CHART) {
            return {...result, data: {metrics}};
        }

        metrics = metrics.filter(metric => metric.child && Array.isArray(metric.child));

        // If the page is Form Detail, get list fields by form id as a Form Metric
        if (+objType === MetricModel.PAGE_TYPE.FORM_DETAIL) {
            // Get a parent metric for Form Metric or create a new one
            const formMetric = metrics.find(metric => metric.metricName === "Form metric") || {
                metricName: "Form metric",
                metricCode: "METR_00000030",
                fixColumn: ColumnModel.COLUMN_NOT_FIXED,
            };

            // Get list fields of the form
            const listFieldsByFormId = await FieldModel.getListFieldsByFormId(params);

            // Append fields to list children
            formMetric.child = formMetric.child.concat(listFieldsByFormId
                .filter(field => (
                    (+field["field_type_id"] !== FieldModel.FIELD_TYPE.HEADER_TEXT) &&
                    (+field["field_type_id"] !== FieldModel.FIELD_TYPE.PARAGRAPH) &&
                    (+field["field_type_id"] !== FieldModel.FIELD_TYPE.IMAGE))
                )
                .map(field => {
                    let dataType;
                    switch (+field.field_type_id) {
                        case FieldModel.FIELD_TYPE.FILE:
                            dataType = MetricModel.DATA_TYPE.FILE;
                            break;
                        case FieldModel.FIELD_TYPE.GDPR:
                            dataType = MetricModel.DATA_TYPE.GDPR;
                            break;
                        default:
                            dataType = MetricModel.DATA_TYPE.TEXT;
                    }

                    return {
                        metricName: field["field_label"],
                        metricCode: field["field_id"],
                        fixColumn: ColumnModel.COLUMN_NOT_FIXED,
                        dataType,
                    }
                })
            );

            // Index of Form Metric object, append to last if not any
            const formMetricIndex = metrics.findIndex(metric => metric.metricName === "Form metric");
            if (formMetricIndex >= 0) {
                metrics[formMetricIndex] = formMetric;
            } else {
                metrics.push(formMetric);
            }
        }

        return {
            ...result,
            data: {
                metrics
            }
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });

        return {code: 500, message: "Internal server error"}
    }
};

module.exports = {
    getDetailMetric
};
