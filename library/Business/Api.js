// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/Business/Api.js';

const errorCode = (code) => {
    try {
        let arrError = {
            204: 'No data result.',
            400: 'Bad request',
            401: 'Unauthorized',
            403: 'Forbidden'
        };

        if (code && arrError[code]) {
            return arrError[code];
        } else {
            return 'Error message';
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                code
            }
        });
    }
};

const success = (res, data) => {
    try {
        if (res) {
            let requestId = res.req && res.req.requestId;

            res.send({
                'code': 200,
                'message': 'Success',
                'data': data,
                requestId
            });
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                data
            }
        });
    }
};

const error = (res, code, message) => {
    try {
        if (res) {
            let requestId = res.req && res.req.requestId;

            res.status(code || 200).send({
                requestId: requestId,
                errors: {
                    'code': code,
                    'message': message || errorCode(code)
                }
            });
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                res,
                code,
                message
            }
        });
    }
};

const response = (res, result) => {
    try {
        let {code = 200, message = 'Success', data = {}} = result;

        if (!res) {
            return null;
        }

        let requestId = res.req && res.req.requestId;

        res.send({code, message, data, requestId});
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                result
            }
        });
    }
};

const demoCallJob = (res, result) => {

    // const WorkerAdmin = require('@library/RabbitMQ/worker-admin');
    //
    // await WorkerAdmin.sendMessage('Export', 'handleLargeFile', {
    //     createSheetParams,
    //     checkFileParams,
    //     ...(sendEmail && {
    //         sendEmail: {
    //             email: params.email,
    //             networkId
    //         }
    //     }),
    //     dataSourceId,
    //     token,
    //     userId,
    //     accountId,
    //     reportId
    // });

};

module.exports = {
    response,
    success,
    error
};
