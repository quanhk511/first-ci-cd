// Library
const Axios = require("axios");
const QS = require("querystring");

// Config
const Config = require("@library/Config");

// Slack Constant
const SlackConstant = Config.get("slack");

/**
 * @typedef {Object} SlackChannelArgs - Argument to create a Slack Channel instance
 * @property {string} channel - Channel name
 */

/** A Slack Channel class */
class SlackChannel {
    /**
     * Construct a Slack Channel
     * @param args {SlackChannelArgs}
     */
    constructor(args) {
        /** @type {string} */
        this.channel = args.channel;

        /** @type {string} */
        this.webhook = getWebhookUrl(this.channel);
    }

    /**
     * Return list of current supported channel names
     *
     * @return {{FORM_MONITOR: string}}
     * @constructor
     */
    static get CHANNELS() {
        return {
            FORM_MONITOR: "form-monitor"
        }
    }

    /**
     * Send a message to Slack channel
     * @param {string} message - Message to be sent
     *
     * @return {Promise<string>} - Return "ok" if the message is sent to channel successfully; otherwise return the error message
     */
    async sendMessage(message) {
        if (this.webhook) {
            const data = QS.stringify({
                "payload": `{"channel": "${this.channel}", "text": "${message}"}`
            });
            const config = {
                method: "post",
                url: this.webhook,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                data: data
            };

            return await Axios(config)
                .then(response => `${response.data}`)
                .catch(error => error.toJSON());
        }

        return Promise.resolve(`No webhook found for the channel ${this.channel}`);
    }
}

module.exports = SlackChannel;

/**
 * Get a Slack webhook URL of the corresponding channel.
 * Depending on which environment this function is called, some webhook may be unavailable.
 *
 * @param {String} channel - Channel name
 * @return {String|null} - The corresponding webhook URL or null if the webhook for that channel is unavailable
 */
const getWebhookUrl = channel =>
    SlackConstant.channels && Object.keys(SlackConstant.channels).length &&
    SlackConstant.channels[channel] && Object.keys(SlackConstant.channels[channel]).length
        ? SlackConstant.channels[channel].webhook
        : null;