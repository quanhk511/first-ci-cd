const Config = require('@library/Config');
const {handleError} = require('@library/Utils');

const {promisify} = require("util");

let client = {};
const PATH = '@library/Redis.js';

module.exports.getInstances = async function (instance) {
    try {
        let config = Config.get('redis');

        if (instance && config && config.redis && config.redis[instance]) {
            let instanceInfo = config.redis[instance];
            if (instanceInfo.host && instanceInfo.port) {
                const hostname = instanceInfo.host;
                const port = instanceInfo.port;

                const redis = require('redis');

                let clientInstance;
                if (client[config.redis[instance].host]) {
                    clientInstance = client[config.redis[instance].host];
                } else {
                    client[config.redis[instance].host] = redis.createClient({
                        host: hostname,
                        port: port
                    });

                    client[config.redis[instance].host].on('error', function (err) {
                        handleError(err, {
                            action: new Error().stack,
                            path: PATH
                        });

                        return false;
                    });

                    clientInstance = client[config.redis[instance].host];
                }

                clientInstance = {
                    ...clientInstance,

                    /**
                     * Get the value of key. If the key does not exist the special value nil is returned. An error is
                     * returned if the value stored at key is not a string, because GET only handles string values.
                     *
                     * @param {string} key
                     *
                     * @return {string} - The value of key, or nil when key does not exist.
                     */
                    get: promisify((clientInstance.get)).bind(clientInstance),

                    /**
                     * Removes the specified fields from the hash stored at key. Specified fields that do not exist
                     * within this hash are ignored. If key does not exist, it is treated as an empty hash and this
                     * command returns 0.
                     *
                     * @param {string} key
                     * @param {string} field...
                     *
                     * @return {number} - The number of fields that were removed from the hash, not including specified
                     * but non existing fields.
                     */
                    hdel: promisify(clientInstance.hdel).bind(clientInstance),

                    /**
                     * Returns if field is an existing field in the hash stored at key.
                     *
                     * @param {string} key
                     * @param {string} field
                     *
                     * @return {number}
                     *  - 1 if the hash contains field.
                     *  - 0 if the hash does not contain field, or key does not exist.
                     */
                    hexists: promisify(clientInstance.hexists).bind(clientInstance),

                    /**
                     * Returns the value associated with field in the hash stored at key.
                     *
                     * @param {string} key
                     * @param {string} field
                     *
                     * @return {string} - The value associated with field, or nil when field is not present in the hash
                     * or key does not exist
                     */
                    hget: promisify(clientInstance.hget).bind(clientInstance),

                    /**
                     * Returns all fields and values of the hash stored at key. In the returned value, every field name
                     * is followed by its value, so the length of the reply is twice the size of the hash.
                     *
                     * @param {string} key
                     *
                     * @return {string[]} - List of fields and their values stored in the hash, or an empty list when
                     * key does not exist
                     */
                    hgetall: promisify(clientInstance.hgetall).bind(clientInstance),

                    /**
                     * Increments the number stored at field in the hash stored at key by increment.
                     * If key does not exist, a new key holding a hash is created. If field does not exist the value is
                     * set to 0 before the operation is performed.
                     * The range of values supported by HINCRBY is limited to 64 bit signed integers.
                     *
                     * @param {string} key
                     * @param {string} field
                     * @param {number} increment
                     *
                     * @return {number} - The value at field after the increment operation
                     */
                    hincrby: promisify(clientInstance.hincrby).bind(clientInstance),

                    /**
                     * Increment the specified field of a hash stored at key, and representing a floating point number,
                     * by the specified increment. If the increment value is negative, the result is to have the hash
                     * field value decremented instead of incremented. If the field does not exist, it is set to 0 before
                     * performing the operation. An error is returned if one of the following conditions occur:
                     * - The field contains a value of the wrong type (not a string).
                     * - The current field content or the specified increment are not parsable as a double precision floating point number.
                     *
                     * The exact behavior of this command is identical to the one of the INCRBYFLOAT command,
                     * please refer to the documentation of INCRBYFLOAT for further information.
                     *
                     * @param {string} key
                     * @param {string} field
                     * @param {number} increment
                     *
                     * @return {string} - The value of field after the increment
                     */
                    hincrbyfloat: promisify(clientInstance.hincrbyfloat).bind(clientInstance),

                    /**
                     * Returns all field names in the hash stored at key.
                     *
                     * @param {string} key
                     *
                     * @return {string[]} - List of fields in the hash, or an empty list when key does not exist
                     */
                    hkeys: promisify(clientInstance.hkeys).bind(clientInstance),

                    /**
                     * Returns the number of fields contained in the hash stored at key.
                     *
                     * @param {string} key
                     *
                     * @return {number} - Number of fields in the hash, or 0 when key does not exist
                     */
                    hlen: promisify(clientInstance.hlen).bind(clientInstance),

                    /**
                     * Returns the values associated with the specified fields in the hash stored at key.
                     *
                     * For every field that does not exist in the hash, a nil value is returned. Because non-existing keys
                     * are treated as empty hashes, running HMGET against a non-existing key will return a list of nil values.
                     *
                     * @param {string} key
                     * @param {string} field...
                     *
                     * @return {string[]} - List of values associated with the given fields, in the same order as
                     * they are requested
                     */
                    hmget: promisify(clientInstance.hmget).bind(clientInstance),

                    /**
                     * Sets the specified fields to their respective values in the hash stored at key. This command
                     * overwrites any specified fields already existing in the hash. If key does not exist, a new key
                     * holding a hash is created.
                     *
                     * @param {string} key
                     * @param {string} field
                     * @param {string} value
                     * @param {string} field...
                     * @param {string} value...
                     *
                     * @return {string} - "+OK\r\n"
                     */
                    hmset: promisify(clientInstance.hmset).bind(clientInstance),

                    /**
                     * Iterates fields of Hash types and their associated values.
                     *
                     * @param {string} key
                     * @param {number} cursor
                     * @param {"MATCH"} MATCH The required keyword to identify the next argument is matching pattern
                     * @param {string} pattern
                     * @param {"COUNT"=} COUNT The required keyword to identify the next argument is count
                     * @param {number=10} count The amount of work that should be done at every call in order to retrieve
                     * elements from the collection. Default is 10
                     *
                     * @return {[number, [string, string][]]} - Array of elements contain two elements, where the first
                     * element is a string representing an unsigned 64 bit number (the cursor), and the second element
                     * is a multi-bulk with an array of elements:
                     *  - Each of which is a field and a value, for every returned element of the Hash
                     */
                    hscan: promisify(clientInstance.hscan).bind(clientInstance),

                    /**
                     * Sets field in the hash stored at key to value. If key does not exist, a new key holding a
                     * hash is created. If field already exists in the hash, it is overwritten.
                     *
                     * @param {string} key
                     * @param {string} field
                     * @param {string} value
                     * @param {string} field...
                     * @param {string} value...
                     *
                     * @return {number} - The number of fields that were added
                     */
                    hset: promisify(clientInstance.hset).bind(clientInstance),

                    /**
                     * Sets field in the hash stored at key to value, only if field does not yet exist.
                     * If key does not exist, a new key holding a hash is created. If field already exists,
                     * this operation has no effect.
                     *
                     * @param {string} key
                     * @param {string} field
                     * @param {string} value
                     *
                     * @return {number}
                     * - 1 if field is a new field in the hash and value was set
                     * - 0 if field already exists in the hash and no operation was performed
                     */
                    hsetnx: promisify(clientInstance.hsetnx).bind(clientInstance),

                    /**
                     * Returns the string length of the value associated with field in the hash stored at key.
                     * If the key or the field do not exist, 0 is returned.
                     *
                     * @param {string} key
                     * @param {string} field
                     *
                     * @return {number} - The string length of the value associated with field, or zero when field is
                     * not present in the hash or key does not exist at all
                     */
                    hstrlen: promisify(clientInstance.hstrlen).bind(clientInstance),

                    /**
                     * Returns all values in the hash stored at key.
                     *
                     * @param {string} key
                     *
                     * @return {string[]} - List of values in the hash, or an empty list when key does not exist
                     */
                    hvals: promisify(clientInstance.hvals).bind(clientInstance),

                    /**
                     * Set key to hold the string value. If key already holds a value, it is overwritten, regardless of
                     * its type. Any previous time to live associated with the key is discarded on successful SET operation.
                     *
                     * @param {string} key
                     * @param {string} value
                     * @param {"EX"=} EX - The required keyword to identify the next argument is the EX arguments
                     * @param {number=} seconds - Set the specified expire time, in seconds
                     * @param {"PX"=} PX - The required keyword to identify the next argument is the PX arguments
                     * @param {number=} milliseconds - Set the specified expire time, in milliseconds
                     * @param {"KEEPTTL"=} KEEPTTL - Retain the time to live associated with the key
                     * @param {"NX"=} NX - Only set the key if it does not already exist
                     * @param {"XX"=} XX - Only set the key if it already exist
                     * @param {"GET"=} GET - Return the old value stored at key, or nil when key did not exist
                     *
                     * @return {string} - OK if SET was executed correctly. When GET option is set, the old value stored
                     * at key, or nil when key did not exist. Null Bulk Reply is returned if the SET operation was not
                     * performed because the user specified the NX or XX option but the condition was not met or if user
                     * specified the NX and GET options that do not met.
                     */
                    set: promisify(clientInstance.set).bind(clientInstance),
                }

                return clientInstance;
            }
        }
    } catch (e) {
        handleError(e, {
            action: 'getInstances',
            path: PATH,
            args: {instance}
        });

        return false;
    }
};
