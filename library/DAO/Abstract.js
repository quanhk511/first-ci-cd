const {getObjectPropSafely, camelKeysClone, handleError} = require('@library/Utils');
const {flatten} = require('lodash');

const PATH = '@library/DAO/Abstract';

const formatDataByType = (data, type) => {
    if (data || typeof data === 'number' || data === 0) {
        switch (type) {
            case 'string':
                return `'${data}'`;
            case 'array':
                if ((data && data.length) || typeof data === 'number') {
                    return `array[${data}]`;
                } else {
                    return 'null';
                }
            case 'stringArray':
                if ((data && data.length)) {
                    return `array[${flatten([data]).map(d => d === null || d === undefined ? 'null' : "'" + d + "'")}]`;
                } else {
                    return 'null';
                }
            case 'jsonArray':
                if ((data && data.length)) {
                    return `'{${flatten([data]).map(d => {
                        if ([undefined, null].some(forbiddenType => forbiddenType === d)) {
                            return 'null';
                        } else {
                            let formatData = JSON.stringify(d);
                            // All single quote mark (') must be replace to  double single quote mark ('')
                            formatData = formatData.replace(/'/g, "''");
                            
                            return `${JSON.stringify(formatData)}`;
                        }
                    })}}'`;
                } else {
                    return 'null';
                }
            case 'json': {
                let formatData = JSON.stringify(data);
                // All single quote mark (') must be replace to  double single quote mark ('')
                formatData = formatData.replace(/'/g, "''");

                return `'${formatData}'`;
            }
            case 'number':
                return data;
            default:
                return `'${data}'`;
        }
    } else {
        return 'null';
    }
};

const transformData = (data) => {
    if (getObjectPropSafely(() => Object.values(data.rows[0])[0])) {
        return Object.values(data.rows[0])[0];
    } else {
        return null;
    }
};

/**
 * Execute a function that returning a refcursor in PG
 * @param {*} connection
 * @param {*} statement
 */
const executeCursorReturnedStatement = async (connection, statement) => {
    try {
        await connection.query('BEGIN');

        const result = await connection.query(statement);
        const fetch = transformData(result);
        const fetchResult = await connection.query(`FETCH ALL IN "${fetch}";`);
        await connection.query('COMMIT');

        return transformFNCursorResult(fetchResult);
    } catch (e) {
        throw e;
    }
};

const transformFNCursorResult = result => {
    return getObjectPropSafely(() => result.rows.map(r => camelKeysClone(r, false, true)), []);
};

const createBindingParams = (data, bindVars) => {
    try {
        if (!bindVars || !Object.keys(bindVars).length) {
            return [];
        }

        return Object.entries(bindVars).map(([key, detail]) => {
            let value = data[key];
            let dataType = 'string';
            let defaultValue = null;

            if (typeof detail === 'string') {
                dataType = detail;
            } else if (typeof detail === 'object' && 'type' in detail) {
                dataType = detail.type;

                if (detail.defaultValue !== undefined) {
                    defaultValue = detail.defaultValue;
                }
            }

            if (value === undefined) {
                value = defaultValue;
            }

            const formattedValue = formatDataByType(value, dataType);

            return formattedValue;
        });
    } catch (err) {
        handleError(err, {
            action: new Error().stack,
            path: PATH,
            args: {data, bindVars}
        });
    }
};

module.exports = {
    formatDataByType,
    executeCursorReturnedStatement,
    transformFNCursorResult,
    createBindingParams,
    transformData
};
