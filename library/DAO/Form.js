// Libraries
const PostgreSQL = require("@library/PostgreSQL");
const Logger = require("@library/Logger");

// Utils
const {formatDataByType, transformData} = require("@library/DAO/Abstract");
const {handleError} = require("@library/Utils");

const PATH = "@library/DAO/Form.js";

const createNewForm = async (params) => {
    let connection;

    try {
        const {
            networkId,
            formName,
            properties,
            formOptions,
            shareLink,
            versionId,
            status,
            userId
        } = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
            p_form_name: formatDataByType(formName, "string"),
            p_properties: formatDataByType(properties, "json"),
            p_form_options: formatDataByType(formOptions, "json"),
            p_share_link: formatDataByType(shareLink, "string"),
            p_version_id: formatDataByType(versionId, "string"),
            p_status: formatDataByType(status, "number")
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_add_sv_forms(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const funcName = await connection.query(queryStatement);
            let formId = transformData(funcName);

            return formId;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const updateForm = async (params) => {
    let connection;

    try {

        const {
            formId = null,
            networkId = null,
            formName = null,
            properties = null,
            formOptions = null,
            shareLink = null,
            versionId = null,
            status = null,
            userId = null
        } = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
            p_form_id: formatDataByType(formId, "number"),
            p_form_name: formatDataByType(formName, "string"),
            p_properties: formatDataByType(properties, "json"),
            p_form_options: formatDataByType(formOptions, "json"),
            p_share_link: formatDataByType(shareLink, "string"),
            p_version_id: formatDataByType(versionId, "string"),
            p_status: formatDataByType(status, "number")
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_upd_sv_forms(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const funcName = await connection.query(queryStatement);
            let formId = transformData(funcName);

            return formId;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getFormDetail = async (params) => {
    let connection;

    try {

        const {
            formId = null,
            networkId = null
        } = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_form_id: formatDataByType(formId, "number")
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_get_detail_sv_forms(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const funcName = await connection.query(queryStatement);
            let functionName = transformData(funcName);
            if (!functionName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${functionName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getAllGdprOptions = async (params) => {
    let connection;

    try {
        const {
            formGdprId = []
        } = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_form_gdpr_id: formatDataByType(formGdprId, "array"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_get_sv_form_gdpr(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const funcName = await connection.query(queryStatement);
            let functionName = transformData(funcName);
            if (!functionName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${functionName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getAllForms = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId, userId} = params;

        // Extract params
        let {
            type,
            formId = [],
            segments,
            columns,
            filterCol,
            filterVal,
            sort,
            offset,
            limit,
        } = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
            p_type: formatDataByType(type, "number"),
            p_form_id: formatDataByType(formId, "array"),
            p_segments: formatDataByType(segments, "string"),
            p_columns: formatDataByType(columns, "string"),
            p_filter_col: formatDataByType(filterCol, "string"),
            p_filter_val: formatDataByType(filterVal, "string"),
            p_sort: formatDataByType(sort, "string"),
            p_offset: formatDataByType(offset, "number"),
            p_limit: formatDataByType(limit, "number"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);

            const queryStatement = `SELECT * FROM application_fe.fn_get_sv_form_perf(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);

            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }

}

const isFormNameExisted = async params => {
    let connection;

    try {
        const {networkId, userId} = params;
        const formName = params["form_name"];

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id : formatDataByType(networkId, "number"),
            p_user_id : formatDataByType(userId, "number"),
            p_form_name : formatDataByType(formName, "string"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_check_name_sv_forms(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let result = transformData(queryResult);

            return (+result !== 0);
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

module.exports = {
    getAllGdprOptions,
    getAllForms,
    createNewForm,
    updateForm,
    getFormDetail,
    isFormNameExisted,
};
