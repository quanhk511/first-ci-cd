// Libraries
const PostgreSQL = require("@library/PostgreSQL");

// Utils
const {formatDataByType, transformData} = require("@library/DAO/Abstract");
const {handleError} = require("@library/Utils");

const PATH = "@library/DAO/Caching.js";

const getFormsData = async (params) => {
    let connection;

    try {

        const {
            formId = [],
            networkId = null,
            fromUtime = null,
            toUtime = null,
            page = 1,
            limit = 500
        } = params;

        const offset = (page - 1) * limit || 0;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_form_id: formatDataByType(formId, "array"),
            p_from_utime: formatDataByType(fromUtime, "string"),
            p_to_utime: formatDataByType(toUtime, "string"),
            p_offset: formatDataByType(offset, "number"),
            p_limit: formatDataByType(limit, "number")
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_caching_sv_forms(${values})`;

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getFieldsData = async (params) => {
    let connection;

    try {

        const {
            fieldId = [],
            networkId = null,
            fromUtime = null,
            toUtime = null,
            page = 1,
            limit = 500
        } = params;

        const offset = (page - 1) * limit || 0;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_field_id: formatDataByType(fieldId, "array"),
            p_from_utime: formatDataByType(fromUtime, "string"),
            p_to_utime: formatDataByType(toUtime, "string"),
            p_offset: formatDataByType(offset, "number"),
            p_limit: formatDataByType(limit, "number")
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_caching_sv_fields(${values})`;

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

module.exports = {
    getFormsData,
    getFieldsData
};
