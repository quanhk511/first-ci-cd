// Library
const PostgreSQL = require("@library/PostgreSQL");
const Logger = require("@library/Logger");

// Utils
const {formatDataByType, transformData} = require('@library/DAO/Abstract');
const {handleError} = require('@library/Utils');

const PATH = '@library/DAO/Submission.js';

const createSubmission = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId} = params;

        // Extract params
        const {form_id, submission_data} = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_form_id: formatDataByType(form_id, "number"),
            p_submission_data: formatDataByType(`[${JSON.stringify(submission_data)}]`, "string"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);

            const queryStatement = `SELECT * FROM application_fe.fn_add_submission_sv_form_submission(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let result = transformData(queryResult);

            return result;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
}

module.exports = {
    createSubmission,
}