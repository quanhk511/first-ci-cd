// Libraries
const PostgreSQL = require('@library/PostgreSQL');
const Logger = require("@library/Logger")

// Utils
const {formatDataByType, transformData, createBindingParams} = require('@library/DAO/Abstract');
const {handleError, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/DAO/Filter.js';

const getListing = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            formId = null,
            type = null,
            columns = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number") ,
            p_type: formatDataByType(type, 'number'),
            p_columns: formatDataByType(columns, 'string'),
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_listing_filters(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const create = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            formId = null,
            type = null,
            filterName = null,
            rules = null,
            status = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number") ,
            p_filter_name: formatDataByType(filterName, 'string'),
            p_rules: formatDataByType(rules, 'json'),
            p_type: formatDataByType(type, 'number'),
            p_status: formatDataByType(status, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_add_filters(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const result = await connection.query(queryText);

            return transformData(result);
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const update = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            filterId,
            filterName = null,
            rules = null,
            status = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_USER_ID: formatDataByType(userId, 'number'),
            p_NETWORK_ID: formatDataByType(networkId, 'number'),
            p_FILTER_ID: formatDataByType(filterId, 'number'),
            p_FILTER_NAME: formatDataByType(filterName, 'string'),
            p_RULES: formatDataByType(rules, 'json'),
            p_STATUS: formatDataByType(status, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_upd_filters(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const result = await connection.query(queryText);

            return transformData(result);
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

module.exports = {
    getListing,
    create,
    update
};
