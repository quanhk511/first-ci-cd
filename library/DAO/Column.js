// Libraries
const PostgreSQL = require('@library/PostgreSQL');
const Logger = require("@library/Logger");

// Utils
const {formatDataByType, transformData} = require('@library/DAO/Abstract');
const {handleError} = require('@library/Utils');

const PATH = '@library/DAO/Column.js';

const getListingModifyColumn = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            formId = null,
            type = null,
            columns = null,
            order = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number"),
            p_type: formatDataByType(type, 'number'),
            p_columns: formatDataByType(columns, 'string'),
            p_sort: formatDataByType(order, 'string')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_listing_modify_columns(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const getLatestModifyColumn = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            formId,
            type = null
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number"),
            p_type: formatDataByType(type, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_is_lasted_modify_columns(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const checkNameColumn = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            formId = null,
            type = null,
            modifyName = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_USER_ID: formatDataByType(userId, 'number'),
            p_NETWORK_ID: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number"),
            p_MODIFY_NAME: formatDataByType(modifyName, 'string'),
            p_TYPE: formatDataByType(type, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_check_name_modify_columns(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const addModifyColumn = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            modifyName = null,
            columns = null,
            formId = null,
            type = null,
            status = null,
            isLasted = null
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number"),
            p_modify_name: formatDataByType(modifyName, 'string'),
            p_columns: formatDataByType(columns, 'string'),
            p_type: formatDataByType(type, 'number'),
            p_status: formatDataByType(status, 'number'),
            p_is_lasted: formatDataByType(isLasted, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_add_modify_columns(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const result = await connection.query(queryText);

            return transformData(result);
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const updateModifyColumn = async params => {
    let connection;

    try {
        const {
            id = null,
            userId,
            networkId,
            type = null,
            formId = null,
            modifyName = null,
            columns = null,
            status = null,
            isLasted = null
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_user_id: formatDataByType(userId, 'number'),
            p_network_id: formatDataByType(networkId, 'number'),
            p_form_id: formatDataByType(formId, "number"),
            p_modify_col_id: formatDataByType(id, 'number'),
            p_modify_name: formatDataByType(modifyName, 'string'),
            p_columns: formatDataByType(columns, 'string'),
            p_type: formatDataByType(type, 'number'),
            p_status: formatDataByType(status, 'number'),
            p_is_lasted: formatDataByType(isLasted, 'number')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_upd_modify_columns(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const result = await connection.query(queryText);

            const data = transformData(result);

            return data;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

module.exports = {
    getListingModifyColumn,
    getLatestModifyColumn,
    checkNameColumn,
    addModifyColumn,
    updateModifyColumn
};
