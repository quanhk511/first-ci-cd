// Libraries
const PostgreSQL = require('@library/PostgreSQL');
const Logger = require("@library/Logger")

// Utils
const {formatDataByType, transformData, createBindingParams} = require('@library/DAO/Abstract');
const {handleError, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/DAO/Field.js';

const createField = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId, userId} = params;

        // Extract params
        const {
            description = "",
            field_type_id,
            form_id,
            label,
            internal_name,
            properties,
        } = params;
        const is_required = params.is_required ? 1 : 0;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
            p_form_id: formatDataByType(form_id, "number"),
            p_field_name: formatDataByType(internal_name, "string"),
            p_field_label: formatDataByType(label, "string"),
            p_field_type_id: formatDataByType(field_type_id, "number"),
            p_description: formatDataByType(description, "string"),
            p_is_required: is_required,
            p_properties: formatDataByType(properties, "json"),
            p_status: 1,
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);

            const queryStatement = `SELECT * FROM application_fe.fn_add_sv_fields(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let result = transformData(queryResult);

            return result;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const getAllYourFields = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId, userId} = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_get_sv_fields_by_user(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getFieldTypes = async params => {
    let connection;

    try {
        // get ids
        const {
            fieldTypeIds = null
        } = params;
        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        //get All Field Types
        const bindVar = {
            p_field_type_id: formatDataByType(fieldTypeIds, 'array')
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_sv_field_type(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const isFieldNameValid = async params => {
    let connection;

    try {
        const {networkId} = params;
        const formId = params["form_id"];
        const fieldName = params["field_name"];

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_form_id: formatDataByType(formId, "number"),
            p_field_name: formatDataByType(fieldName, "string"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_check_name_sv_fields(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let result = transformData(queryResult);

            return (+result === 0);
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getField = async params => {
    let connection;

    try {
        const {networkId, userId} = params;
        const fieldId = +params["id"];

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_field_id: formatDataByType(fieldId, "number"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);

            const queryStatement = `SELECT * FROM application_fe.fn_get_detail_sv_fields(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getListDefaultFields = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId, userId} = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_get_sv_fields_default(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const getListFieldsByFormId = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId} = params;

        const {form_id} = params;

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_form_id: formatDataByType(form_id, "number"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);
            const queryStatement = `SELECT * FROM application_fe.fn_get_sv_fields_by_form(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let cursor = transformData(queryResult);
            if (!cursor) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${cursor}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params,
            },
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params,
                    },
                });
            }
        }
    }
};

const updateField = async params => {
    let connection;

    try {
        // Authentication params
        const {networkId, userId} = params;

        // Extract params
        const {
            description = [""],
            field_id,
            label,
            properties,
            status = [1],
        } = params;
        const is_required = params.is_required ? [1] : [0];

        // PostgreSQL Function 's params
        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_user_id: formatDataByType(userId, "number"),
            p_field_id : formatDataByType(field_id, "array"),
            p_field_label: formatDataByType(label, "stringArray"),
            p_description: formatDataByType(description, "stringArray"),
            p_is_required: formatDataByType(is_required, "array"),
            p_properties: formatDataByType(properties, "jsonArray"),
            p_status: formatDataByType(status, "array"),
        };

        connection = await PostgreSQL.getInstanceFromBool(
            "info_master_application"
        );

        if (connection) {
            const values = Object.values(bindVar);

            const queryStatement = `SELECT * FROM application_fe.fn_upd_sv_fields(${values})`;

            Logger.debug("Query String: ", queryStatement);

            // Begin PostgreSQL Function
            await connection.query('BEGIN');

            const queryResult = await connection.query(queryStatement);
            let result = transformData(queryResult);

            return result;
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                // End of PostgreSQL Function
                await connection.query("COMMIT");

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
}

module.exports = {
    createField,
    getAllYourFields,
    getFieldTypes,
    isFieldNameValid,
    getField,
    getListDefaultFields,
    getListFieldsByFormId,
    updateField,
};
