// Libraries
const PostgreSQL = require('@library/PostgreSQL');
const Logger = require("@library/Logger")

// Utils
const {formatDataByType, transformData, createBindingParams} = require('@library/DAO/Abstract');
const {handleError, getObjectPropSafely} = require('@library/Utils');

const PATH = '@library/DAO/Metric.js';

const getListing = async params => {
    let connection;

    try {
        const {
            userId,
            networkId,
            type = null,
            byFnc = null,
            columns = null,
            order = null,
        } = params;

        connection = await PostgreSQL.getInstanceFromBool('info_master_application');

        const bindVar = {
            p_USER_ID: formatDataByType(userId, 'number'),
            p_NETWORK_ID: formatDataByType(networkId, 'number'),
            p_TYPE: formatDataByType(type, 'number'),
            p_BY_FNC: formatDataByType(byFnc, 'string'),
            p_COLUMNS: formatDataByType(columns, 'string'),
            p_SORT: formatDataByType(order, 'string'),
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_listing_metrics(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

const getDetailMetric = async params => {
    let connection;

    try {
        const {
            networkId,
            metricId = '',
            metricCode = '',
            type = null
        } = params;

        connection = await PostgreSQL.getInstanceFromBool("info_master_application");

        const bindVar = {
            p_network_id: formatDataByType(networkId, "number"),
            p_metric_id: formatDataByType(metricId, "string"),
            p_metric_code: formatDataByType(metricCode, "string"),
            p_type: formatDataByType(type, "number")
        };

        if (connection) {
            const values = Object.values(bindVar);
            const queryText = `SELECT * FROM application_fe.fn_get_detail_metrics(${values});`;

            Logger.debug("Query String: ", queryText);

            await connection.query('BEGIN');

            const res = await connection.query(queryText);
            let funcName = transformData(res);
            if (!funcName) {
                return 0;
            }

            const result = await connection.query(`FETCH ALL IN "${funcName}";`);

            if (result.rows) {
                return result.rows;
            }
        }

        return null;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
        return null;
    } finally {
        if (connection) {
            try {
                await connection.query('COMMIT');

                connection.release();
            } catch (error) {
                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params
                    }
                });
            }
        }
    }
};

module.exports = {
    getListing,
    getDetailMetric
};
