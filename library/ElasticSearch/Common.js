// Library
const Moment = require("moment");
const SlackChannel = require("@library/Slack");

// Services
const ElasticSearch = require('@library/Elastic');
const {getObjectInfo} = require('@library/ElasticSearch/Mapping');

const path = require('path');

// Utils
const {handleError, removeSpecialCharacter, removeAccent, snakeToCamel, writeLog} = require('@library/Utils');

const PATH = '@library/ElasticSearch/Common';

const index = async (objectName, data) => {

    // Logs file name
    const fileSuccess = "Library_ES_Common_Index_Success";
    const fileError = "Library_ES_Common_Index_Error";

    const paramsForLog = {};
    paramsForLog.data = JSON.parse(JSON.stringify(data)) || {};
    paramsForLog.objectName = objectName;

    delete paramsForLog.data.properties;
    delete paramsForLog.data.list_changed;

    let arrLog = {params: paramsForLog};

    try {
        if (data) {
            const objectInfo = getObjectInfo(objectName);

            const elastic = new ElasticSearch(data.instance || 'info_master', objectName);
            const ESClient = elastic.get.client;

            // Check if exist index (db)
            const exists = await ESClient.indices.exists({
                index: elastic.get.indexName
            });

            // Create new index if index does not exist
            if (!exists.body) {
                await mapping(objectName);
            }

            let id = null;
            let body = {};

            Object.entries(data).forEach(([key, val]) => {
                const newKey = key.toLowerCase();

                if (objectInfo.private_key.some(privateKey => privateKey === newKey)) {
                    id = `${id}${val}`;
                }

                if (typeof objectInfo.index[key] !== 'undefined') {
                    body[key] = val;
                }

            });
            const request = {
                index: elastic.get.indexName,
                type: 'data',
                id: null,
                body
            };

            const response = await ESClient.index(request).then().catch((error) => {
                arrLog.error = JSON.stringify(error);

                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        params: error.meta.body.error
                    }
                });
            });

            await ESClient.indices.refresh();

            if (response && response.body.created && response.body.created === true) {
                arrLog.success = response.body.created;
                writeLog(fileSuccess, arrLog);

                return response;
            }

            writeLog(fileError, arrLog);
            return null;
        }

        arrLog.error = "Cannot Index this document on ES (Data params not found)";
        writeLog(fileError, arrLog);

        return null;
    } catch (error) {
        arrLog.error = error.message;
        writeLog(fileError, arrLog);

        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {}
        });
    }
};

const bulk = async (objectName, params) => {
    try {
        if (params.data) {
            const objectInfo = getObjectInfo(objectName);
            const elastic = new ElasticSearch(params.instance || 'info_master', objectName);
            const ESClient = elastic.get.client;

            // Check if exist index (db)
            const exists = await ESClient.indices.exists({
                index: elastic.get.indexName
            });

            // Create new index if index does not exist
            if (!exists.body) {
                await mapping(objectName);
            }

            const {data} = params;

            let body = [];
            data.forEach(doc => {
                let id = objectInfo.private_key.reduce((result, privateKey) => `${result}${doc[privateKey]}`, '') || '';
                let index = {
                    _index: elastic.get.indexName,
                    _type: 'data',
                    _id: id
                };
                let newDoc = {};
                Object.entries(doc).forEach(([key, val]) => {
                    if (typeof objectInfo.index[key] !== 'undefined') {
                        newDoc[key] = val;
                    }
                });
                body = [...body, {index}, newDoc];
            });

            const bulkResult = await ESClient.bulk({refresh: true, body});
            const {body: {errors} = {}} = bulkResult;

            if (errors) {
                const {body: {items = []} = {}} = bulkResult;
                const error = new Error('Cannot bulk to elasticsearch');

                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        objectName,
                        params,
                        items
                    }
                });
            }
        }
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName, params
            }
        });


    }
};

const bulkData = async (objectName, params) => {
    try {
        let data = params;

        const objectInfo = getObjectInfo(objectName);
        const elastic = new ElasticSearch(params.instance || 'info_master', objectName);
        const ESClient = elastic.get.client;

        // Check if exist index (db)
        // const exists = await ESClient.indices.exists({
        //     index: elastic.get.indexName
        // });

        // Create new index if index does not exist, Delete and create new index if isMapping === true
        // if (!exists.body) {
            await mapping(objectName, data.isMapping);
        // }

        let isBulkAll = data[snakeToCamel(objectInfo.private_key[0])].length === 0;
        if (isBulkAll) {
            data.page = 1;
            data.limit = 5;
        }

        do {
            let func = objectInfo.dao_catching;
            let dataDB;

            const pathname = path.join(__dirname, '..', 'DAO/Caching');
            const dao = require(pathname);
            if (dao && dao[func]) {
                dataDB = await dao[func]({
                    ...data
                });
            }

            let body = [];

            if (dataDB.length === 0) {
                break;
            }

            dataDB.forEach(doc => {
                let id = objectInfo.private_key.reduce((result, privateKey) => `${result}${doc[privateKey]}`, '') || '';
                let index = {
                    _index: elastic.get.indexName,
                    _type: 'data',
                    _id: id
                };

                let newDoc = {};
                Object.entries(doc).forEach(([key, val]) => {
                    if (typeof objectInfo.index[key] !== 'undefined') {
                        newDoc[key] = val;
                    }

                    // Parse JSON to String
                    if (typeof val === 'object') {
                        newDoc[key] = JSON.stringify(val);
                    }

                });

                for (let fieldRaw of objectInfo.field_raw) {
                    let field = fieldRaw + "_raw";
                    // console.log(fieldRaw);
                    // console.log(newDoc[fieldRaw]);
                    // console.log(removeAccent(newDoc[fieldRaw]));
                    // console.log(removeSpecialCharacter(newDoc[fieldRaw]));
                    // const val = removeSpecialCharacter(newDoc[fieldRaw]).toLowerCase();
                    // console.log("Val: ", val);
                    let val = newDoc[fieldRaw];
                    val = removeAccent(val);
                    val = removeSpecialCharacter(val);
                    val = val.toLowerCase();
                    newDoc[field] = val;
                }

                let dataSearch = [];
                for (let key of objectInfo.data_search) {
                    dataSearch.push(newDoc[key]);
                }

                newDoc.data_search = dataSearch.join(' ');

                body = [...body, {index}, newDoc];

            });

            const bulkResult = await ESClient.bulk({refresh: true, body});
            const {body: {errors} = {}} = bulkResult;

            if (errors) {
                const {body: {items = []} = {}} = bulkResult;
                const error = new Error('Cannot bulk to elasticsearch');

                const slack = new SlackChannel({channel: SlackChannel.CHANNELS.FORM_MONITOR});
                await slack.sendMessage(
                    `${Moment().format("DD-MM-YYYY HH:mm:ss")} Error when bulking Forms to ElasticSearch
                        Environment: ${process.env.APPLICATION_ENV}
                        Error: ${error.message}
                        Params: ${util.inspect(params,
                            {
                                breakLength: Infinity,
                                compact: true,
                                depth: Infinity,
                                showHidden: true
                        })}`);

                handleError(error, {
                    path: PATH,
                    action: new Error().stack,
                    args: {
                        objectName,
                        params,
                        items
                    }
                });
            }
            data.page++;
        } while (isBulkAll);

        return true;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName, params
            }
        });
        return false;
    }
};

const update = async (objectName, data) => {
    try {
        if (data) {
            const objectInfo = getObjectInfo(objectName);
            const elastic = new ElasticSearch('info_master', objectName);
            const ESClient = elastic.get.client;

            let id = objectInfo.private_key.reduce((result, privateKey) => `${result}${data[privateKey]}`, '') || '';

            let body = Object.entries(data)
                .filter(([key, _]) => objectInfo.index[key] !== undefined)
                .reduce((accum, [key, value]) => {
                    accum[key.toLowerCase()] = value;
                    return accum;
                }, {});

            if (id && Object.entries(body).length !== 0) {
                const request = {
                    index: elastic.get.indexName,
                    type: 'data',
                    id,
                    retryOnConflict: 1, // Retry 1 time if there is conflict
                    body: {
                        doc: body
                    }
                };

                const data = await ESClient.update(request);
                await ESClient.indices.refresh();

                return data;
            }
        }

        return null;
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }
};

const del = async (objectName, params) => {
    try {
        if (objectName && params && params.id) {
            const elastic = new ElasticSearch(params.instance || 'info_master', objectName);
            const ESClient = elastic.get.client;

            const request = {
                index: elastic.get.indexName,
                type: 'data',
                id: params.id
            };

            return await ESClient.delete(request);
        }
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }
};

const mapping = async (objectName, isMapping) => {
    try {
        if (objectName) {
            const objectInfo = getObjectInfo(objectName);
            const elastic = new ElasticSearch('info_master', objectName);
            const ESClient = elastic.get.client;

            const params = {
                index: elastic.get.indexName,
                body: {
                    settings: {
                        number_of_shards: 1,
                        number_of_replicas: 1,
                        max_result_window: 1000000,
                    },
                    mappings: {
                        data: {
                            _source: {
                                enabled: true
                            },
                            properties: objectInfo.index
                        }
                    }
                }
            };

            const exist = await ESClient.indices.exists({
                index: elastic.get.indexName
            });

            if (!exist.body) {
                const createRes = await ESClient.indices.create({...params});

                return createRes;
            }

            if (isMapping === true) {
                await ESClient.indices.delete({
                    index: elastic.get.indexName
                })

                return await ESClient.indices.create(params);
            }
        }

        return null;
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }
};

const transform = (response) => {
    try {
        let result = {};
        let arrRows = [];

        if (response && response.body && response.body.hits && response.body.hits.hits) {
            response.body.hits.hits.forEach((hit) => {
                if (hit && hit._source) {
                    arrRows.push(hit._source);
                }
            });

            if (arrRows.length) {
                result['rows'] = arrRows;
            }

            if (response.body.hits.total) {
                result['total'] = response.body.hits.total;
            }
        }

        if (response && response.body && response.body.aggregations) {
            result['aggregations'] = response.body.aggregations;
        }

        return result;
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                response
            }
        });
    }
};

const transformWithId = (response) => {
    try {
        let result = {};
        let arrRows = [];

        if (response && response.body && response.body.hits && response.body.hits.hits) {
            response.body.hits.hits.forEach((hit) => {
                if (hit && hit._source) {
                    hit._source._id = hit._id;
                    arrRows.push(hit._source);
                }
            });

            if (arrRows.length) {
                result['rows'] = arrRows;
            }

            if (response.body.hits.total) {
                result['total'] = response.body.hits.total;
            }
        }

        if (response && response.body && response.body.aggregations) {
            result['aggregations'] = response.body.aggregations;
        }

        return result;
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                response
            }
        });
    }
};

// @TODO must be deprecate
const getInfo = async (objectName, params) => {
    try {
        if (objectName && params) {
            const elastic = new ElasticSearch(params.instance || 'info_master', objectName);
            const ESClient = elastic.get.client;

            let offset = params.offset || 0;
            let limit = params.limit || 10;

            let query;
            if (Object.keys(params.terms).length) {
                query = {
                    bool: {
                        must: {
                            term: {...params.terms}
                        }
                    }
                };
            }

            const request = {
                index: elastic.get.indexName,
                from: offset,
                size: limit,
                type: 'data',
                ...(query && {
                    body: {
                        query
                    }
                })
            };

            const data = await ESClient.search(request);

            return transform(data);
        }
    } catch (error) {
        // Error
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }
};

const get = (objectName, instanceName = 'info_master') => {
    const result = [];

    try {
        if (!objectName) {
            return result;
        }

        const elastic = new ElasticSearch(instanceName, objectName);

        return async request => {
            const data = await elastic.get.client.search({
                index: elastic.get.indexName,
                ...request
            });

            return transform(data);
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }

    return result;
};

const getWithId = (objectName, instanceName = 'info_master') => {
    const result = [];

    try {
        if (!objectName) {
            return result;
        }

        const elastic = new ElasticSearch(instanceName, objectName);

        return async request => {
            const data = await elastic.get.client.search({
                index: elastic.get.indexName,
                ...request
            });

            return transformWithId(data);
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }

    return result;
};

const validateFilters = (filters, allowedFields) => {
    try {
        const moment = require('moment');

        if (!filters || !(Array.isArray(filters) && filters.length) ||
            !(allowedFields && Object.keys(allowedFields).length)) {
            return;
        }

        let must = [];
        let mustNot = [];
        let should = [];

        filters.map(filter => {
            const {name, operator, value} = filter;

            if (!Object.keys(allowedFields).some(field => field === name)) {
                return;
            }

            const columnName = allowedFields[name].alias || '';

            if (!columnName) {
                return;
            }

            switch (operator) {
                case 'contain':
                    must.push({
                        wildcard: {
                            [columnName]: `*${removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))}*`
                        }
                    });
                    break;
                case 'doesNotContain':
                    mustNot.push({
                        wildcard: {
                            [columnName]: `*${removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))}*`
                        }
                    });
                    break;
                case 'is':
                    must.push({
                        wildcard: {
                            [columnName]: removeSpecialCharacter(removeAccent(value.toString().toLocaleLowerCase()))
                        }
                    });
                    break;
                case 'startWith':
                    must.push({
                        prefix: {
                            [columnName]: value.toString().toLocaleLowerCase()
                        }
                    });
                    break;
                case 'matchesAny':
                    must.push({
                        terms: {
                            [columnName]: value
                        }
                    });
                    break;
                case 'on': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 23:59:59`,
                                gt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'before': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'after': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                gt: `${date} 23:59:59`
                            }
                        }
                    });
                    break;
                }
            }
        });

        return {must, mustNot, should};
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                filters
            }
        });
    }
};

const validateRawFilters = (filters, allowedFields) => {
    try {
        const moment = require('moment');

        if (!filters || !(Array.isArray(filters) && filters.length) ||
            !(allowedFields && Object.keys(allowedFields).length)) {
            return;
        }

        let must = [];
        let mustNot = [];
        let should = [];

        filters.map(filter => {
            const {name, operator, value} = filter;

            if (!Object.keys(allowedFields).some(field => field === name)) {
                return;
            }

            const columnName = allowedFields[name].alias || '';

            if (!columnName) {
                return;
            }

            switch (operator) {
                case 'contain':
                    must.push({
                        wildcard: {
                            [columnName]: `*${value.toString()}*`
                        }
                    });
                    break;
                case 'doesNotContain':
                    mustNot.push({
                        wildcard: {
                            [columnName]: `*${value.toString()}*`
                        }
                    });
                    break;
                case 'is':
                    must.push({
                        wildcard: {
                            [columnName]: value.toString()
                        }
                    });
                    break;
                case 'startWith':
                    must.push({
                        prefix: {
                            [columnName]: value.toString()
                        }
                    });
                    break;
                case 'matchesAny':
                    must.push({
                        terms: {
                            [columnName]: value
                        }
                    });
                    break;
                case 'on': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 23:59:59`,
                                gt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'before': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                lt: `${date} 00:00:00`
                            }
                        }
                    });
                    break;
                }
                case 'after': {
                    const date = moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD');

                    must.push({
                        range: {
                            [columnName]: {
                                gt: `${date} 23:59:59`
                            }
                        }
                    });
                    break;
                }
            }
        });

        return {must, mustNot, should};
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                filters
            }
        });
    }
};

module.exports = {
    index,
    getInfo,
    get,
    getWithId,
    update,
    delete: del,
    mapping,
    transform,
    transformWithId,
    bulk,
    validateFilters,
    validateRawFilters,
    bulkData
};
