// Libraries
const moment = require('moment');

// ElasticSearch
const CommonElasticSearch = require('@library/ElasticSearch/Common');

// Utils
const {handleError, removeSpecialCharacter, removeAccent} = require('@library/Utils');

const PATH = '@library/ElasticSearch/Test';
const objectName = 'test';

const index = params => {
    try {
        const {
            testId = null,
            testName = null,
            properties = null,
            status = null,
            ctime = null,
            utime = null,
        } = params;

        const data = {
            test_id: +testId,
            test_name: testName,
            test_name_raw: removeSpecialCharacter(removeAccent(testName.toString().toLocaleLowerCase())),
            properties: properties,
            status: +status,
            utime: utime,
            ctime: ctime,
        };

        return CommonElasticSearch.index(objectName, data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const get = async params => {
    try {
        const {
            arrDestChannelId = [],
            page = 1,
            limit = 10,
            status = 1
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        if (arrDestChannelId.length) {
            must.push({
                terms: {
                    dest_connector_id: arrDestChannelId
                }
            });
        }

        if (status && !isNaN(+status)) {
            must.push({
                term: {
                    status
                }
            });
        }

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', objectName);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transform(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};


module.exports = {
    objectName,
    index,
    get
};
