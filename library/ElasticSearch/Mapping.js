// Utils
const {handleError} = require('@library/Utils');

const PATH = '@library/ElasticSearch/Mapping.js';

const objectType = {
    test: {
        test_id: {
            type: 'long'
        },
        test_name: {
            type: 'string',
            fields: {
                raw: {
                    type: 'string',
                    analyzer: 'my_analyzer'
                }
            }
        },
        test_name_raw: {
            type: 'string',
            index: 'not_analyzed'
        },
        properties: {
            type: 'string',
            analyzer: 'my_analyzer',
            ignore_above: 10922
        },
        status: {
            type: 'integer'
        },
        ctime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        utime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        data_search: {
            type: 'string',
            index: 'not_analyzed'
        }
    }
};

const formVersion = {
    form: {
        form_id: {
            type: 'long'
        },
        network_id: {
            type: 'long'
        },
        properties: {
            type: 'string',
            index: 'not_analyzed'
        },
        list_changed: {
            type: 'string',
            index: 'not_analyzed'
        },
        status: {
            type: 'integer'
        },
        user_id: {
            type: 'long'
        },
        ctime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        data_search: {
            type: 'string',
            index: 'not_analyzed'
        }
    }
};

const formEntity = {
    form: {
        form_id: {
            type: 'long'
        },
        network_id: {
            type: 'long'
        },
        form_name: {
            type: 'string',
            index: 'not_analyzed'
        },
        properties: {
            type: 'string',
            index: 'not_analyzed'
        },
        form_name_raw: {
            type: 'string',
            index: 'not_analyzed'
        },
        form_options: {
            type: 'string',
            index: 'not_analyzed'
        },
        share_link: {
            type: 'string',
            index: 'not_analyzed'
        },
        version_id: {
            type: 'string',
            index: 'not_analyzed'
        },
        status: {
            type: 'integer'
        },
        user_id: {
            type: 'long'
        },
        ctime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        utime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        data_search: {
            type: 'string',
            index: "not_analyzed"
        }
    }
};

const fieldEntity = {
    field: {
        field_id: {
            type: 'long'
        },
        network_id: {
            type: 'long'
        },
        form_id: {
            type: 'long'
        },
        field_name: {
            type: 'string',
            index: 'not_analyzed'
        },
        field_label_raw: {
            type: 'string',
            index: 'not_analyzed'
        },
        field_label: {
            type: 'string',
            index: 'not_analyzed'
        },
        field_type_id: {
            type: 'long'
        },
        description: {
            type: 'string',
            index: 'not_analyzed'
        },
        is_required: {
            type: 'integer'
        },
        is_hidden: {
            type: 'integer'
        },
        properties: {
            type: 'string',
            index: 'not_analyzed'
        },
        is_default: {
            type: 'integer'
        },
        status: {
            type: 'integer'
        },
        user_id: {
            type: 'long'
        },
        ctime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        utime: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        data_search: {
            type: 'string',
            index: 'not_analyzed'
        }
    }
};

const PageView = {
    page_view: {
        form_id: {
            type: 'long'
        },
        network_id: {
            type: 'long'
        },
        date: {
            type: 'date',
            format: 'yyyy-MM-dd HH:mm:ss',
            index: 'not_analyzed'
        },
        page_view: {
            type: 'integer'
        },
        ip: {
            type: 'string',
            index: 'not_analyzed'
        },
        user_agent: {
            type: 'string',
            index: 'not_analyzed'
        },
        referer: {
            type: 'string',
            index: 'not_analyzed'
        },
        resolution: {
            type: 'string',
            index: 'not_analyzed'
        }
    }
};

const getObjectInfo = (objectName) => {
    try {
        let config = {
            index: '',
            function_name: '',
            private_key: []
        };

        if (objectName.includes("page_view")) {
            config = {
                ...config,
                index: PageView.page_view,
                function_name: '',
                private_key: ['_id']
            };
            return config;
        }

        switch (objectName) {
            case 'test':
                config = {
                    ...config,
                    index: objectType.test,
                    function_name: '',
                    private_key: ['test_id']
                };
                break;
            case 'form_version':
                config = {
                    ...config,
                    index: formVersion.form,
                    function_name: '',
                    private_key: ['_id']
                };
                break;
            case 'form': {
                config = {
                    ...config,
                    index: formEntity.form,
                    field_raw: ['form_name'],
                    function_name: '',
                    private_key: ['form_id'],
                    dao_catching: 'getFormsData',
                    data_search: ['form_id', 'form_name_raw']
                };
                break;
            }
            case 'field': {
                config = {
                    ...config,
                    index: fieldEntity.field,
                    field_raw: ['field_label'],
                    function_name: '',
                    private_key: ['field_id'],
                    dao_catching: 'getFieldsData',
                    data_search: ['field_id', 'field_label_raw']
                };
                break;
            }
            default:
                break;
        }
        return config;
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                objectName
            }
        });
    }
};

module.exports = {
    getObjectInfo
};
