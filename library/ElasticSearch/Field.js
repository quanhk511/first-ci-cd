// Libraries
const moment = require('moment');

// ElasticSearch
const CommonElasticSearch = require('@library/ElasticSearch/Common');

// Utils
const {handleError, removeSpecialCharacter, removeAccent} = require('@library/Utils');

const PATH = '@library/ElasticSearch/Field';
const field = 'field';

const getFieldsLabel = async params => {
    try {
        const {
            page = 1,
            limit = 10,
            fieldIds,
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        for (let fieldId of fieldIds) {
            must.push({
                should: {
                    field_id: fieldId
                }
            });
        }

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', field);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transform(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    getFieldsLabel
};
