// Libraries
const moment = require('moment');

// ElasticSearch
const CommonElasticSearch = require('@library/ElasticSearch/Common');

// Utils
const {handleError, removeSpecialCharacter, removeAccent} = require('@library/Utils');

const PATH = '@library/ElasticSearch/Form';
const formVersion = 'form_version';
const form = 'form';

const index = params => {
    try {
        const {
            formId = null,
            networkId = null,
            properties = null,
            listChanged = null,
            status = null,
            userId = null,
            ctime = null
        } = params;

        const data = {
            form_id: +formId,
            network_id: +networkId,
            list_changed: listChanged,
            properties: properties,
            status: +status,
            user_id: +userId,
            ctime: ctime
        };

        return CommonElasticSearch.index(formVersion, data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const updateStatusFormVersion = params => {
    try {
        const {
            _id = null,
            status = null
        } = params;

        if (status === null || _id === null) {
            return null;
        }

        const data = {
            _id: _id,
            status: +status
        };

        return CommonElasticSearch.update(formVersion, data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const update = params => {
    try {
        const {
            _id = null,
            formId = null,
            networkId = null,
            properties = null,
            listChanged = null,
            status = null,
            userId = null,
            ctime = null
        } = params;

        const data = {
            _id: _id,
            form_id: +formId,
            network_id: +networkId,
            list_changed: listChanged,
            properties: properties,
            status: +status,
            user_id: +userId,
            ctime: ctime
        };

        return CommonElasticSearch.update(formVersion, data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const get = async params => {
    try {
        const {
            page = 1,
            limit = 10,
            status = 1
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        if (arrDestChannelId.length) {
            must.push({
                terms: {
                    dest_connector_id: arrDestChannelId
                }
            });
        }

        if (status && !isNaN(+status)) {
            must.push({
                term: {
                    status
                }
            });
        }

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', formVersion);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transform(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getLastestFormVersion = async params => {
    try {
        const {
            form_id,
            page = 1,
            limit = 10,
            ignoredStatus = 1
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];
        let must_not = [];

        must.push({
            term: {
                form_id: form_id
            }
        });

        must_not.push({
            term: {
                status: ignoredStatus
            }
        })

        // if (status && !isNaN(+status)) {
        //     must.push({
        //         term: {
        //             status
        //         }
        //     });
        // }

        const query = {
            bool: {
                must,
                must_not
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', formVersion);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transformWithId(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getLastestPublishedFormVersion = async params => {
    try {
        const {
            form_id,
            page = 1,
            limit = 1,
            status
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        must.push({
            term: {
                form_id: form_id
            }
        });

        must.push({
            term: {
                status: status
            }
        });

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', formVersion);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transformWithId(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const getDraftsAfterLatestPublishedFormVersion = async params => {
    try {
        const {
            form_id,
            page = 1,
            limit = 100,
            ctime = null,
            status,
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        must.push({
            term: {
                form_id: form_id
            }
        });

        must.push({
            range: {
                ctime: {
                    gt: ctime
                }
            }
        })

        must.push({
            term: {
                status: status
            }
        })

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', formVersion);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query,
                sort: {
                    ctime: {
                        order: 'desc'
                    }
                }
            }
        };

        const data = await elastic.get.client.search(request);

        return CommonElasticSearch.transformWithId(data);
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const searchFormVersionById = async params => {
    try {
        const {
            _id,
            page = 1,
            limit = 1,
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        must.push({
            term: {
                _id: _id
            }
        });

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');
        const elastic = new ElasticSearch('info_master', formVersion);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query
            }
        };

        const data = await elastic.get.client.search(request);

        return {
            ...CommonElasticSearch.transformWithId(data).rows[0]
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

const searchForm = async params => {
    try {
        const {
            search = "",
            page = 1,
            limit = 500,
            networkId,
            userId
        } = params;

        const offset = (page - 1) * limit || 0;

        let must = [];

        if (search) {
            let searchStr = "*" + search.toLowerCase() + "*";

            must.push({
                wildcard: {
                    data_search: searchStr
                },
                // match_phrase: {
                //     data_search: search.replace(/[^a-zA-Z0-9 ]/g, "")
                // },
            });
        }

        must.push({
            term: {
                network_id: networkId
            }
        });

        must.push({
            term: {
                user_id: userId
            }
        })

        const query = {
            bool: {
                must
            }
        };

        const ElasticSearch = require('@library/Elastic');

        const elastic = new ElasticSearch('info_master', form);

        const request = {
            index: elastic.get.indexName,
            from: offset,
            size: limit,
            type: 'data',
            body: {
                query
            }
        };

        const data = await elastic.get.client.search(request);

        return {
            ...CommonElasticSearch.transformWithId(data)
        };
    } catch (error) {
        handleError(error, {
            path: PATH,
            action: new Error().stack,
            args: {
                params
            }
        });
    }
};

module.exports = {
    objectName: formVersion,
    index,
    get,
    update,
    updateStatusFormVersion,
    getLastestFormVersion,
    getLastestPublishedFormVersion,
    getDraftsAfterLatestPublishedFormVersion,
    searchFormVersionById,
    searchForm
};
