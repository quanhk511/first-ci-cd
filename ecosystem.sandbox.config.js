require('dotenv').config();

const APPLICATION_ENV = process.env.APPLICATION_ENV || 'sandbox';
const PORT = process.env.PORT || 8001;
const UV_THREADPOOL_SIZE = process.env.UV_THREADPOOL_SIZE || 20;
const LOG_LEVEL = process.env.LOG_LEVEL || "OFF";

const ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV,
    UV_THREADPOOL_SIZE: UV_THREADPOOL_SIZE,
    LOG_LEVEL
};

const WORKER_ENV_PARAMS = {
    NODE_ENV: APPLICATION_ENV,
    PORT: PORT,
    APPLICATION_ENV: APPLICATION_ENV,
    LOG_LEVEL
};

module.exports = {
    apps: [
        {
            name: `form_nodejs.${APPLICATION_ENV}`,
            script: 'public/index.js',
            env: ENV_PARAMS,
            watch: false,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '500M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `form_nodejs.worker-data-sync.${APPLICATION_ENV}`,
            script: 'worker/worker-data-sync.js',
            env: WORKER_ENV_PARAMS,
            watch: false,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        },
        {
            name: `form_nodejs.worker-scheduler.${APPLICATION_ENV}`,
            script: 'worker/worker-scheduler.js',
            env: WORKER_ENV_PARAMS,
            watch_options: {
                'followSymlinks': true
            },
            max_memory_restart: '200M',
            log_date_format: 'YYYY-MM-DD HH:mm:ss'
        }
    ]
};
